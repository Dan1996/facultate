digit_map = {}
for i in range(ord('z')-ord('a') + 1):
    digit_map[str(i + 1)] = chr(ord('a') + i)


def solver(sequence):
    counter = 1
    counter_n = 1
    if sequence == "":
        return 0

    rez = sequence[0]

    for el in sequence[1:]:
        rez += el
        if len(rez) == 3:
            rez = rez[1:]

        if el == '0':
            break

        if rez in digit_map:
            counter_n, counter = counter, counter_n + counter

    return counter


"""
it 1:
1

it 2
1 1
11

it 3
1 1 1
11 1
1 11

it 4
1 1 1 3
11 1 3
1 11 3
1 1 13
11 13

it 5
1 1 1 3 4
11 1 3 4
1 11 3 4

1 1 13 4
11 13 4

1 1 1 34
11 1 34
1 11 34
"""