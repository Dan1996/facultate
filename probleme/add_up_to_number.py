
def sovler(numbers, target):
    mem = {}
    return rec_solver(numbers, len(numbers) - 1, target, mem)


def rec_solver(numbers, i, target, mem):
    if (i, target) in mem:
        return mem[i, target]

    if target == 0:
        return 1
    if i < 0 or target < 0:
        return 0

    if target < numbers[i]:
        rez = rec_solver(numbers, i-1, target, mem)
    else:
        rez = rec_solver(numbers, i-1, target - numbers[i], mem) + rec_solver(numbers, i-1, target, mem)

    mem[(i, target)] = rez

    return rez

print(sovler([2,4,6,10], 16))

