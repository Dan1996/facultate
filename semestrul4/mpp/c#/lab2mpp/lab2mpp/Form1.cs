﻿using lab2mpp.Entities;
using lab2mpp.Repository;
using lab2mpp.Services;
using Lab2mpp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace lab2mpp
{
	public partial class Form1 : Form
	{
		Service service;
		public Form1(Service service)
		{
			this.service = service;
			InitializeComponent();
		}

		private void textBox2_TextChanged(object sender, EventArgs e)
		{

		}

		private void button1_Click(object sender, EventArgs e)
		{
			User user= new User(textBox1.Text, textBox2.Text);
			if (service.validate(user))
			{
				//deschide form 2
				Form2 form2 = new Form2(service);
				Hide();
				MessageBox.Show("Log in successfully!");
				form2.ShowDialog();
				Close();
			}
			else
				MessageBox.Show("Log in failed!");
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}
	}
}
