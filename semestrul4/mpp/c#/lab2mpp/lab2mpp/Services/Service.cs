﻿using lab2mpp.Entities;
using lab2mpp.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Services
{
	public class Service
	{
		IDictionary<String, string> props;
		
			
		ParticipantDBRepository repoP;
		UserDBRepository repoU;
		SwimTaskDBRepository repoS;
		ContestRepository repoC;
		//swimtask
		//contest
		public Service()
		{
			props = new SortedList<String, String>();
			props.Add("ConnectionString", GetConnectionStringByName("tasksDB"));
			repoP = new ParticipantDBRepository(props);
			repoU = new UserDBRepository(props);
			repoS = new SwimTaskDBRepository(props);
			repoC = new ContestRepository(props);
		}
		public DataTable findAllParticipants()
		{
			return repoP.findAll();
		}
		public DataTable findAllParticipantsbyName(string name)
		{
			return repoP.findByName(name);
		}
		public DataTable findAllSwimTasks()
		{
			return repoS.findAll();
		}
		public void addParticipant(string name, string country)
		{
			Participant participant = new Participant(name, country);
			repoP.save(participant); 
		}
		public void addContest(int idP, int idS)
		{
			Contest contest = new Contest(idP, idS);
			repoC.save(contest);
		}

		public static string GetConnectionStringByName(string name)
		{
			// Assume failure.
			string returnValue = null;

			// Look for the name in the connectionStrings section.
			ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];

			// If found, return the connection string.
			if (settings != null)
				returnValue = settings.ConnectionString;

			return returnValue;
		}
		public bool validate(User user)
		{
			return repoU.validate(user);
		}
	}
}
