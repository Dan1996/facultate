﻿using lab2mpp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Entities
{
	class Participant:HasId<int>
	{
		public int _Id;
		internal string _Name { get; set; }
		internal string _Country { get; set; }
		public static int _Index { get; set; } = 0;
		public Participant(string name, string country) {
			_Index++;
			this._Id = _Index;
			this._Name = name;
			this._Country = country;
		}
		public Participant(int id,string name, string country)
		{
			this._Id = id;
			this._Name = name;
			this._Country = country;
		}

		public override string ToString()
		{
			return this._Id + " " + this._Name + " " + this._Country;
		}

		public int getId()
		{
			return this._Id;
		}

		public void setId(int id)
		{
			this._Id = id;
		}
	}
}
