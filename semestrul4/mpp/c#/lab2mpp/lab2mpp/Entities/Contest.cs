﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Entities
{
	class Contest
	{
		public int _Pid { set; get; }
		public int _Stid { set; get; }

		public Contest(int pid, int stid) {
			this._Pid = pid;
			this._Stid = stid;
		}
		public override string ToString()
		{
			return "Contest:" + this._Pid + " " + this._Stid;
		}
	}
}
