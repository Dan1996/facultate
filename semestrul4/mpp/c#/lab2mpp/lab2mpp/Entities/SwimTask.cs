﻿using lab2mpp.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Entities
{
	class SwimTask:HasId<int>
	{
		public int _Id { get; set; }
		internal string _Name { get; set; }
		internal int _Distance { get; set; }
		public static int _Index { get; set; } = 0;
		public SwimTask(string name, int distance)
		{
			_Index++;
			this._Id = _Index;
			this._Name = name;
			this._Distance = distance;
		}
		public override string ToString()
		{
			return this._Id + " " + this._Name + " " + this._Distance;
		}

		public int getId()
		{
			return this._Id;
		}

		public void setId(int id)
		{
			this._Id = id;
		}
	}
}
