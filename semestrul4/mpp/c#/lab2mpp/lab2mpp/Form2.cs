﻿using lab2mpp.Services;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2mpp
{
	public partial class Form2 : Form
	{
		Service service;
		public Form2(Service service)
		{
			this.service = service;
			InitializeComponent();

			


		}

		private void Form2_Load(object sender, EventArgs e)
		{
			dataGridView2.DataSource = service.findAllParticipants();
			dataGridView1.DataSource = service.findAllSwimTasks();

		}


		private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			//participants
			int rowIndex = e.RowIndex;
			DataGridViewRow row = dataGridView2.Rows[rowIndex];
			String id = row.Cells[0].Value.ToString();//aici iau id-ul
			label6.Text = id;
		}
		
		private void label1_Click(object sender, EventArgs e)
		{

		}

		private void label3_Click(object sender, EventArgs e)
		{

		}

		private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			//swimtasks
			int rowIndex = e.RowIndex;
			DataGridViewRow row = dataGridView1.Rows[rowIndex];
			String id = row.Cells[0].Value.ToString();//aici iau id-ul
			label4.Text = id;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			
				int pid = int.Parse(label6.Text);
				int stid = int.Parse(label4.Text);
				service.addContest(pid, stid);
				dataGridView1.DataSource = service.findAllSwimTasks();
			
		}

		private void button2_Click(object sender, EventArgs e)
		{
			string name = textBox1.Text;
			dataGridView2.DataSource = service.findAllParticipantsbyName(name);
		}

		private void button3_Click(object sender, EventArgs e)
		{
			dataGridView2.DataSource = service.findAllParticipants();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			string name = textBox2.Text;
			string country = textBox3.Text;
			service.addParticipant(name, country);
			MessageBox.Show("Successfully!");
			dataGridView2.DataSource = service.findAllParticipants();
		}

		private void button5_Click(object sender, EventArgs e)
		{
			Form1 form1 = new Form1(service);
			Hide();
			MessageBox.Show("Log out successfully!");
			form1.ShowDialog();
			Close();
		}
	}
}
