﻿using lab2mpp.Entities;
using Lab2mpp;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Repository
{
	
	public class UserDBRepository
	{
		IDictionary<String, string> props;
		MySqlConnection connection;
		public UserDBRepository(IDictionary<String, string> props)
		{
			this.props = props;
			connection = new MySqlConnection(props["ConnectionString"]);
		}
		public bool validate(User user)
		{
			try
			{
				connection.Open();
				string query = "Select password from users where username=" + "'" + user.Username + "'";
				var cmd = new MySqlCommand(query, connection);
				var reader = cmd.ExecuteReader();
				reader.Read();

				string username = reader.GetString(0);
				return true;
			}
			catch (Exception e)
			{
				Console.Write(e.ToString());
			}
			finally { 
			connection.Close();
			}
			return false;
		}
		public static string CreateMD5(string input)
		{
			// Use input string to calculate MD5 hash
			using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
			{
				byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
				byte[] hashBytes = md5.ComputeHash(inputBytes);

				// Convert the byte array to hexadecimal string
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < hashBytes.Length; i++)
				{
					sb.Append(hashBytes[i].ToString("X2"));
				}
				return sb.ToString();
			}
		}
	}
}
