﻿using lab2mpp.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Repository
{
	class SwimTaskDBRepository : ISwimTasksRepository
	{
		IDictionary<String, string> props;

		int maxim = 0;
		MySqlConnection connection;
		public SwimTaskDBRepository(IDictionary<String, string> props)
		{
			this.props = props;
			connection = new MySqlConnection(props["ConnectionString"]);
			this.setIndex();

		}
		public void setIndex()
		{
			connection.Open();
			string query = "Select max(id) as maxim from swimtasks";
			var cmd = new MySqlCommand(query, connection);
			var reader = cmd.ExecuteReader();
			reader.Read();
			int id = reader.GetInt16(0);

			if (id > maxim)
			{
				maxim = id;
			}
			connection.Close();
		}
		public DataTable findAll()
		{
			DataTable table = new DataTable();
			MySqlDataAdapter dataAdapter;
			try

			{
				connection.Open();
				string query = "Select * from swimtasks";
				var cmd = new MySqlCommand(query, connection);
				dataAdapter = new MySqlDataAdapter(cmd);
				dataAdapter.Fill(table);
				Console.Write(table);
				connection.Close();
			}
			catch (MySqlException e)
			{
				Console.WriteLine(e);
			}
			finally
			{
				connection.Close();
			}
			return table;
		}

		public void save(SwimTask t)
		{
			connection.Open();
			MySqlCommand command = connection.CreateCommand();
			command.CommandText = "INSERT INTO swimtasks(id, name, distance) VALUES (?id,?name,?distance)";
			command.Parameters.Add("?id", t.getId());
			command.Parameters.Add("?name", t._Name);
			command.Parameters.Add("?country", t._Distance);
			command.ExecuteNonQuery();
			connection.Close();
		}
	}
}