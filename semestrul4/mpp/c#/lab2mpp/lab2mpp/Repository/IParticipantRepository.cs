﻿using lab2mpp.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Repository
{
	interface IParticipantRepository:IRepository<Participant,int>
	{
		//chestii specifice doar pe participant
	}
}
