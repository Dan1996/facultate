﻿using lab2mpp.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab2mpp.Repository
{
	class ContestRepository
	{
		IDictionary<String, string> props;

		int maxim = 0;
		MySqlConnection connection;
		public ContestRepository(IDictionary<String, string> props)
		{
			this.props = props;
			connection = new MySqlConnection(props["ConnectionString"]);
		}
		public void save(Contest t)
		{
			try
			{
				connection.Open();
				MySqlCommand command = connection.CreateCommand();
				command.CommandText = "INSERT INTO contests(pid, stid) VALUES (?pid,?stid)";
				command.Parameters.Add("?pid", t._Pid);
				command.Parameters.Add("?stid", t._Stid);
				command.ExecuteNonQuery();
				command.CommandText = "update swimtasks set nrofparticipants=nrofparticipants+1 where id=?id";
				command.Parameters.Add("?id", t._Stid);
				command.ExecuteNonQuery();
				connection.Close();
			}catch(MySqlException e)
			{
				MessageBox.Show("Already exists!");
			}
		}
	}
}
