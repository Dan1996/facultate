﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using lab2mpp.Entities;
using log4net;
using MySql.Data.MySqlClient;

namespace lab2mpp.Repository
{
	class ParticipantDBRepository : IParticipantRepository
	{

		IDictionary<String, string> props;

		int maxim=0;
		MySqlConnection connection;
		public ParticipantDBRepository(IDictionary<String, string> props)
		{
			this.props = props;
			connection = new MySqlConnection(props["ConnectionString"]);
			this.setIndex();
			
		}
		public void setIndex() {
			connection.Open();
			string query = "Select max(id) as maxim from participants";
			var cmd = new MySqlCommand(query, connection);
			var reader = cmd.ExecuteReader();
			reader.Read();
			int id = reader.GetInt16(0);
			
			if (id > maxim)
			{
				maxim = id;
			}
			Participant._Index = maxim;
			connection.Close();
		}
		public DataTable findAll()
		{
			DataTable table = new DataTable();
			MySqlDataAdapter dataAdapter;
			try

			{
				connection.Open();
				string query = "Select * from participants";
				var cmd = new MySqlCommand(query, connection);
				dataAdapter = new MySqlDataAdapter(cmd);
				dataAdapter.Fill(table);
				Console.Write(table);
				connection.Close();
			}
			catch (MySqlException e)
			{
				Console.WriteLine(e);
			}
			finally
			{
				connection.Close();
			}
			return table;
		}
		public DataTable findByName(string name)
		{
			DataTable table = new DataTable();
			MySqlDataAdapter dataAdapter;
			try

			{
				connection.Open();
				string query = "Select * from participants where name like "+"'%"+name+"%'";
				var cmd = new MySqlCommand(query, connection);
				dataAdapter = new MySqlDataAdapter(cmd);
				dataAdapter.Fill(table);
				connection.Close();
			}
			catch (MySqlException e)
			{
				Console.WriteLine(e);
			}
			finally
			{
				connection.Close();
			}
			return table;
		}

		public void save(Participant t)
		{
			connection.Open();
			MySqlCommand command = connection.CreateCommand();
			command.CommandText = "INSERT INTO participants(id, name, country) VALUES (?id,?name,?country)";
			command.Parameters.Add("?id", t.getId());
			command.Parameters.Add("?name", t._Name);
			command.Parameters.Add("?country", t._Country);
			command.ExecuteNonQuery();
			connection.Close();
		}
	}

}


