﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab2mpp.Utils
{
	interface HasId<Id>
	{
		Id getId();
		void setId(Id id);
	}
}
