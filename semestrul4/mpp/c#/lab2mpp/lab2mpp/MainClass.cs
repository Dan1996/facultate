﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using lab2mpp.Repository;
using log4net;
using log4net.Config;

using System.Configuration;
using System.Windows.Forms;
using lab2mpp.Entities;
using lab2mpp;
using lab2mpp.Services;

namespace Lab2mpp
{
	public static class MainClass
	{
		[STAThread]
		public static void Main()
		{
			Service service = new Service();
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1(service));
		}

		
		
	}


}

