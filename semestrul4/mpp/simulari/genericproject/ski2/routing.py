from django.urls import path

from ski2 import consumers

websocket_urlpatterns = [
    path('ws/', consumers.ChatConsumer),
]
