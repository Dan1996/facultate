from django.db import models

# ASPECT_CHOICES = (
#         ('lungime', 'lungime'),
#         ('stil', 'stil'),
#         ('aterizare', 'aterizare')
#     )
#
# ETAPA_CHOICES = (
#         (0, 'NO JUMPS'),
#         (1, 'First Jump'),
#         (2, 'Finished'),
#     )
#
#
# class Participants(models.Model):
#     nume = models.CharField(max_length=100)
#     etapa = models.IntegerField(choices=ETAPA_CHOICES, default=0)
#     scor = models.IntegerField(default=0)
#
#
# class Judges(models.Model):
#     aspect = models.CharField(max_length=100, choices=ASPECT_CHOICES)
#     etapa = models.IntegerField(choices=ETAPA_CHOICES)
#     participant = models.ForeignKey(Participants, on_delete=models.CASCADE)

from django.contrib.auth.models import AbstractUser
from django.db import models


class Proba(models.Model):
    SWIM_STYLES = (
        ('freestyle', 'freestyle'),
        ('backstroke', 'backstroke')
    )
    nume = models.CharField(max_length=100)
    swimstyle = models.CharField(max_length=100, choices=SWIM_STYLES)
    distanta = models.FloatField()

    def __str__(self):
        return f'{self.nume} {self.swimstyle} {self.distanta}'
