import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer

from genericproject import settings
from ski2.models import Proba


class ChatConsumer(WebsocketConsumer):
    def connect(self):
        self.room_group_name = 'chat_%s' % settings.GROUP_NAME

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    def receive(self, text_data):
        data = json.loads(text_data)
        notif = False
        for proba in data:
            del proba['newData']
            del proba['pk']
            Proba.objects.create(**proba)
            notif = True

        if notif:
            async_to_sync(self.channel_layer.group_send)(
                self.room_group_name,
                {
                    'type': 'notify_user',
                    'object': text_data,
                }
            )

    # Receive message from room group
    def notify_user(self, event):
        # Send message to WebSocket
        # if self.scope['user'].pk != event['user']:
        self.send(text_data=event['object'])
