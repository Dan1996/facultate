import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize
from django.http import JsonResponse, Http404, HttpResponse
from django.shortcuts import render, redirect
import channels.layers


# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from myauth.models import MyUser
from ski2.consumers import ChatConsumer
from ski2.forms import UserRegistrationForm
import gc

# @login_required
# def index(request):
#     pants.objects.all()
#     return render(request, 'ski2/index.html', {'participants': participants})
#
#
# def get_participanti(request):
#     if request.method == 'GET':
#         participanti = Participants.objects.all()
#         if request.GET:
#             args = {**request.GET}
#             args = {key: value[0] for key, value in args.items()}
#             participanti = participanti.filter(**args)
#         ser = json.loads(serialize('json', participanti))
#         ser2 = [el['fields'] for el in ser]
#         for el1, el2 in zip(ser, ser2):
#             el2["pk"] = el1["pk"]
#         return JsonResponse(ser2, safe=False)
#
#     raise Http404
from ski2.models import Proba


def create_account(request):
    if request.method == 'POST':
        form1 = UserRegistrationForm(request.POST)
        if form1.is_valid():
            user = form1.save(commit=False)
            user.set_password(form1.cleaned_data['password'])
            user.save()
            login(request, user)
            return redirect('donare:donator_analize')
    else:
        form1 = UserRegistrationForm()

    return render(request, 'donator_inregistrare.html', {'form1': form1})


@csrf_exempt
def login_view(request):
    try:
        body = json.loads(request.body.decode('utf8'))
        username = body.get("username")
        password = body.get("password")
    except:
        username = request.POST.get("username")
        password = request.POST.get("password")
    print(username, password)
    try:
        user = MyUser.objects.get(username=username)
    except:
        raise Http404

    if user.check_password(password):
        login(request, user)
        session_token = request.session.session_key
        return JsonResponse({'token': session_token})

    raise Http404


# @login_required
def get_probe(request):
    if request.method == 'GET':
        probe = Proba.objects.all()
        ser = json.loads(serialize('json', probe))
        ser2 = [el['fields'] for el in ser]
        for el1, el2 in zip(ser, ser2):
            el2["pk"] = el1["pk"]
        ser2 = {'pagina': 1, "probe": ser2}
        return JsonResponse(ser2)


@csrf_exempt
def creare_probe(request):
    if request.method == "POST":
        data = json.loads(request.body.decode('utf8'))
        notif = False
        for proba in data:
            del proba['newData']
            del proba['pk']
            Proba.objects.create(**proba)
            notif = True

        if notif:
            channel_layer = channels.layers.get_channel_layer()

            async_to_sync(channel_layer.group_send)(f'chat_{settings.GROUP_NAME}',
                {
                        'type': 'notify_user',
                    'object': request.body.decode('utf8'),
                }
            )

        return HttpResponse('ok')
