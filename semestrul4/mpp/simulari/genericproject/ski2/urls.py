from django.urls import path

from ski2 import views

app_name = 'ski2'
urlpatterns = [
    # path('', views.index, name='index'),
    # path('participanti/', views.get_participanti, name='get_participanti')
    # path('<int:pk>/voteaza/', views.update_participant, name='voteaza')
    path('probe', views.get_probe, name='probe'),
    path('proba/creare_probe', views.creare_probe, name="creare_probe"),
    path('login', views.login_view, name='login'),
    path('', views.create_account, name='create_acc'),
]
