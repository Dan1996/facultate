from django.contrib.auth.views import LoginView
from django.urls import path

from myauth import views

app_name = 'myauth'
urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('register/', views.register, name="register"),
    path('logout', views.logout_view, name='logout'),
]
