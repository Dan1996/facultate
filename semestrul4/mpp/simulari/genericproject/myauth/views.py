from django.contrib.auth import login, logout, authenticate
from django.shortcuts import render, redirect

from genericproject import settings
from myauth.forms import UserRegistrationForm


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])
            user.save()
            login(request, user)
            return redirect(f'{settings.current_app}:index')
    else:
        form = UserRegistrationForm()
    return render(request, 'myauth/register.html', {'form': form})


def login_view(request):
    error = None
    if request.POST:
        form = UserRegistrationForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect(f'{settings.current_app}:index')
        error = 'Autentificare esuata'
    else:
        form = UserRegistrationForm()
    return render(request, 'myauth/login.html', {'error': error, 'form': form})


def logout_view(request):
    logout(request)
    # INSERT APP NAME HERE
    return redirect(f'{settings.current_app}:index')


def test_view(request):
    pass