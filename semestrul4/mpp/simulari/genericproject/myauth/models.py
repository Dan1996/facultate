from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class MyUser(AbstractUser):
    def __str__(self):
        return f'{self.username} {self.email}'

    ASPECT_CHOICES = (
        ('lungime', 'lungime'),
        ('stil', 'stil'),
        ('aterizare', 'aterizare')
    )

    aspect = models.CharField(max_length=40, choices=ASPECT_CHOICES, default='lungime')
