import chat.network.objectprotocol.ChatServerObjectProxy;
import chat.network.utils.ChatObjectConcurrentServer;
import chat.network.utils.ServerException;
import chat.services.IChatServer;

public class ServerMain {
    public static void main(String[] args){
        ChatObjectConcurrentServer server = new ChatObjectConcurrentServer(8000,
                new ChatServerObjectProxy("localhost", 8001));
        try {
            server.start();
        } catch (ServerException e) {
            e.printStackTrace();
        }
    }
}
