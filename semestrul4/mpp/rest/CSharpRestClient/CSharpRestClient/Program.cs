﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace CSharpRestClient
{
	class MainClass
	{
		static HttpClient client = new HttpClient();

		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			RunAsync().Wait();
		}


		static async Task RunAsync()
		{
			client.BaseAddress = new Uri("http://localhost:8000/");
			client.DefaultRequestHeaders.Accept.Clear();

            CreateProba("http://localhost:8000/proba/creare", "Sandu", "backstroke", "2.3").Wait();
            CreateProba("http://localhost:8000/proba/creare", "Ion", "foo--Not existing", "2.3").Wait();

            MediaTypeWithQualityHeaderValue json = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(json);
            List<Proba> lista = await GetLista("http://localhost:8000/probe");
            int pk = lista[0].pk;
            client.DefaultRequestHeaders.Accept.Remove(json);

            UpdateProba(String.Format("http://localhost:8000/proba/{0}/update", pk), "Tutu", "", "10.0").Wait();

            client.DefaultRequestHeaders.Accept.Add(json);
            lista = await GetLista("http://localhost:8000/probe");
            client.DefaultRequestHeaders.Accept.Remove(json);

            var response = await client.DeleteAsync(String.Format("http://localhost:8000/proba/{0}/delete", pk));
            if (response.IsSuccessStatusCode)
            {
                Console.Write(String.Format("deleted {0}", pk));
            }
            else
            {
                Console.WriteLine("fail on deletion");
            }
            Console.WriteLine("");

            client.DefaultRequestHeaders.Accept.Add(json);
            lista = await GetLista("http://localhost:8000/probe");

            var result = await GetProba(String.Format("http://localhost:8000/proba/{0}", lista[0].pk));
            Console.WriteLine("Am primit {0}", result);

                    

            Console.ReadLine();
		}

        static async Task<List<Proba>> GetLista(string path)
        {
            List<Proba> lista = null;
            HttpResponseMessage response = await client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                lista = await response.Content.ReadAsAsync<List<Proba>>();
                foreach (Proba p in lista)
                {
                    Console.Write(p);
                }
            }
            return lista;
        }

		static async Task<Proba> GetProba(string path)
		{
			Proba product = null;
			HttpResponseMessage response = await client.GetAsync(path);
			if (response.IsSuccessStatusCode)
			{
                product = await response.Content.ReadAsAsync<Proba>();
			}
			return product;
		}


        static async Task CreateProba(string path, string nume, string swimstyle, string distanta)
        {
            var values = new Dictionary<string, string>
            {
               { "nume", nume },
               { "swimstyle", swimstyle},
                {"distanta", distanta },
            };
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync(path, content);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Creat cu succes");
            }
            else
            {
                Console.WriteLine("Not so fast cowboy");
            }
            Console.WriteLine("");

        }

        static async Task UpdateProba(string path, string nume, string swimstyle, string distanta)
        {
            var values = new Dictionary<string, string>
            {
               { "nume", nume },
               { "swimstyle", swimstyle},
                {"distanta", distanta },
            };
            var content = new FormUrlEncodedContent(values);
            var response = await client.PutAsync(path, content);
            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Update cu succes");
            }
            else
            {
                Console.WriteLine("Not so fast cowboy");
            }
            Console.WriteLine("");
        }

    }

	public class Proba
	{
		public int pk { get; set; }
		public string nume { get; set; }
		public string swimstyle { get; set; } 

        public float distanta { get; set; }

        public override string ToString()
        {
            return pk + ' ' + nume + ' ' +  swimstyle + ' ' + distanta + '\n';
        }
    }
	
}
