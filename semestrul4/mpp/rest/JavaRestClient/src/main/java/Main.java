import com.sun.org.apache.regexp.internal.RE;
import okhttp3.*;

public class Main {
    public static void main(String args[]) throws Exception {
        Requester.Test();
    }

    private static OkHttpClient client = new OkHttpClient();

    public static class Requester
    {
        private static String baseURL = "http://localhost:8000";
        private static String urlParameters = "";

        public static void Test() throws Exception
        {
            TestGetAll();
            TestGetOne();
            TestCreate();
            TestUpdate();
            TestDelete();
        }

        public static void TestGetAll() throws Exception
        {
            System.out.println("Testing get all");
            String URL = baseURL;
            Request request = new Request.Builder().url(URL + "/probe").build();

            // List data response.
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }

        public static void TestGetOne() throws Exception
        {
            System.out.println("Testing get one");
            String URL = baseURL + "/proba/1";
            Request request = new Request.Builder().url(URL).build();

            // List data response.
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }

        public static void TestCreate() throws Exception
        {
            System.out.println("Testing create");
            String URL = baseURL + "/proba/creare";

            RequestBody formBody = new FormBody.Builder()
                    .add("nume", "Vasile" )
                    .add("swimstyle", "backstroke")
                    .add("distanta", "2.13" )
                    .build();

            Request request = new Request.Builder().url(URL).post(formBody).build();

            // List data response.
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }

        public static void TestUpdate() throws Exception
        {
            System.out.println("Testing update");
            String URL = baseURL + "/proba/1/update";

            RequestBody formBody = new FormBody.Builder()
                    .add("nume", "Bodiu" )
                    .add("swimstyle", "freestyle")
                    .add("distanta", "5.12" )
                    .build();
            Request request = new Request.Builder().url(URL).put(formBody).build();

            // List data response.
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }

        public static void TestDelete() throws Exception
        {
            System.out.println("Testing delete");
            String URL = baseURL + "/proba/10/delete";
            Request request = new Request.Builder().url(URL).delete().build();

            // List data response.
            Response response = client.newCall(request).execute();
            System.out.println(response);
        }
    }
}