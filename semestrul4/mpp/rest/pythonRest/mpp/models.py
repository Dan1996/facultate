from django.contrib.auth.models import AbstractUser
from django.db import models


class Proba(models.Model):
    SWIM_STYLES = (
        ('freestyle', 'freestyle'),
        ('backstroke', 'backstroke')
    )
    nume = models.CharField(max_length=100)
    swimstyle = models.CharField(max_length=100, choices=SWIM_STYLES)
    distanta = models.FloatField()

    def __str__(self):
        return f'{self.nume} {self.swimstyle} {self.distanta}'
