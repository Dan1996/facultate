import json

from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.core.exceptions import ValidationError
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse, QueryDict, Http404
# Create your views here.
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from mpp.models import Proba
from mpp.forms import baseForm, UserRegistrationForm
from mpp.validator import Validator


validator = Validator()


@csrf_exempt
def creare_proba(request):
    if request.method == 'POST':
        args = ['nume', 'swimstyle', 'distanta']
        kwargs = {key: request.POST.get(key) for key in args}
        proba = Proba(**kwargs)
        try:
            proba.distanta = float(proba.distanta)
        except ValueError:
            return HttpResponse('Failed')
        # validator.validate(proba)
        proba.save()
        return HttpResponse('Succes')
        # return redirect(reverse('probe'))
    else:
        form = baseForm()
    return render(request, 'update.html', {'form': form})


@csrf_exempt
def creare_probe(request):

    if request.method == "POST":
        data = json.loads(request.body.decode('utf8'))

        for proba in data:
            del proba['newData']
            del proba['pk']
            Proba.objects.create(**proba)

        return HttpResponse('ok')


@csrf_exempt
def modifica_proba(request, pk):
    proba = get_object_or_404(Proba, pk=pk)
    if request.method == 'PUT' or request.POST.get('_method') == 'put':
        args = ['nume', 'swimstyle', 'distanta']
        put = QueryDict(request.body)
        for key in args:
            if put.get(key):
                setattr(proba, key, put.get(key))
        try:
            proba.distanta = float(proba.distanta)
            validator.validate(proba)
        except (ValueError, ValidationError):
            return HttpResponse('Failed')
        else:
            proba.save()
            return redirect(reverse('get_proba', kwargs={'pk': pk}))
    else:
        form = baseForm(initial={"nume": proba.nume, "distanta": proba.distanta, "swimstyle": proba.swimstyle})
    return render(request, 'update.html', {'form': form})


def get_proba(request, pk):
    if request.method == 'GET':
        proba = get_object_or_404(Proba, pk=pk)
        if request.META.get('HTTP_ACCEPT') == 'application/json':
            ser = json.loads(serialize('json', [proba]))[0]
            ser2 = ser['fields']
            ser2['pk'] = ser['pk']
            return JsonResponse(ser2)
        else:
            return render(request, 'afiseaza.html', {'proba': proba})


# @login_required
def get_probe(request):
    if request.method == 'GET':
        probe = Proba.objects.all()
        ser = json.loads(serialize('json', probe))
        ser2 = [el['fields'] for el in ser]
        for el1, el2 in zip(ser, ser2):
            el2["pk"] = el1["pk"]
        ser2 = {'pagina': 1, "probe": ser2}
        return JsonResponse(ser2)


def delete_proba(request, pk):
    proba = get_object_or_404(Proba, pk=pk)
    if request.method == 'DELETE':
        proba.delete()
        return HttpResponse('ss')
    elif request.method == 'POST':
        if request.POST.get('raspuns') == 'da':
            proba.delete()
        else:
            return redirect(reverse('get_proba', kwargs={'pk': proba.pk}))
        return redirect('probe')
    else:
        return render(request, 'choose.html')


def create_account(request):
    if request.method == 'POST':
        form1 = UserRegistrationForm(request.POST)
        if form1.is_valid():
            user = form1.save(commit=False)
            user.set_password(form1.cleaned_data['password'])
            user.save()
            login(request, user)
            return redirect('donare:donator_analize')
    else:
        form1 = UserRegistrationForm()

    return render(request, 'donator_inregistrare.html', {'form1': form1})


def login_view(request):

    try:
        user = User.objects.get(username=request.POST['username'])
    except:
        raise Http404

    if user.check_password(request.POST['password']):
        login(request, user)
        session_token = request.session.session_key
        return JsonResponse({'token': session_token})

    raise Http404