from django.core.exceptions import ValidationError


class Validator:
    def validate(self, proba):
        err = ''
        if not proba.nume:
            err += 'Numele nu poate fi vid'
        names = (name[1] for name in proba.SWIM_STYLES)
        if proba.swimstyle not in names:
            err += ''.join(('Swimstyle-ul trebuie sa fie una dintre', ' '.join(names)))
        if proba.distanta is None or proba.distanta <= 0.0:
            err += 'distanta nu poate fi vida'
        if err:
            raise ValidationError(err)