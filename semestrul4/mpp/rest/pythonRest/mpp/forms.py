from django import forms
from django.contrib.auth.models import User


class baseForm(forms.Form):
    SWIM_STYLES = (
        ('freestyle', 'freestyle'),
        ('backstroke', 'backstroke')
    )

    nume = forms.CharField(max_length=100)
    swimstyle = forms.MultipleChoiceField(choices=SWIM_STYLES)
    distanta = forms.FloatField()


class UserRegistrationForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'password', 'email']
        help_texts = {
            'username': None,
        }

        widgets = {'password': forms.PasswordInput,
                   'username': forms.TextInput(attrs={'autofocus': True}),
                  }
