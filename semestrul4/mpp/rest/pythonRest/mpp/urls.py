from django.urls import path

from mpp import views

urlpatterns = [
    path('probe', views.get_probe, name='probe'),
    path('proba/creare', views.creare_proba, name='creare_proba'),
    path('proba/creare_probe', views.creare_probe, name="creare_probe"),
    path('proba/<int:pk>', views.get_proba, name='get_proba'),
    path('proba/<int:pk>/update', views.modifica_proba, name='update_proba'),
    path('proba/<int:pk>/delete', views.delete_proba, name='delete_proba'),
    path('', views.create_account, name='create_acc'),
    path('login', views.login_view, name='login'),
]
