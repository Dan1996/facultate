
import FXMLFiles.Login;
import Repository.ContestDBRepository;
import Repository.ParticipantDBRepository;
import Repository.SwimTaskDBRepository;
import Service.Service;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;


public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Login.class.getResource("/LogIn.fxml"));
        AnchorPane editPane = loader.load();
        Stage stage = new Stage();
        stage.initModality(Modality.WINDOW_MODAL);
        stage.setScene(new Scene(editPane));
        Login ctrl = loader.getController();
        ctrl.setPreviousStage(stage);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}