package Service;

import Entities.Contest;
import Entities.Participant;
import Entities.SwimStyle;
import Entities.SwimTask;
import Repository.ContestDBRepository;
import Repository.ParticipantDBRepository;
import Repository.SwimTaskDBRepository;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;

public class Service {
    ContestDBRepository contestRepo;
    ParticipantDBRepository participantRepo;
    SwimTaskDBRepository swimTaskRepo;

    public Service(ContestDBRepository contestRepo, ParticipantDBRepository participantRepo, SwimTaskDBRepository swimTaskRepo) {
        this.contestRepo = contestRepo;
        this.participantRepo = participantRepo;
        this.swimTaskRepo = swimTaskRepo;
    }

    /*
        Participants
    * */
    public void saveParticipant(String name, String country){
        Participant participant=new Participant(name, country);
        participantRepo.save(participant);
    }
    public Participant deleteParticipant(Integer id){
        return participantRepo.delete(id);
    }

    public Participant findOneParticipant(Integer id){
        return participantRepo.findOne(id);
    }
    public void updateParticipant(Integer id, String name,String country){
        Participant participant=new Participant(name,country);
        participantRepo.update(id,participant);
    }
    public ArrayList findAllParticipants(){
        return participantRepo.findAll();
    }
    public Participant findParticipantByName(String name){return participantRepo.findByName(name);}
    public ArrayList findParticipantByTask(SwimTask task){
        return participantRepo.findAllByTask(task);
    }

    /*
    * SwimTasks
    * */
    public ArrayList findAllSwimTasks(String name){return swimTaskRepo.findAll2(name);}
    public ArrayList findSwimTasksByParticipant(Participant participant){return swimTaskRepo.findByParticipants(participant);}
    /*
    * Contest
    * */
    public void addContest(Contest contest){
        contestRepo.save(contest);
    }
    public static String getMD5Hash(String text){
        try {

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(text.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            return number.toString(16);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
