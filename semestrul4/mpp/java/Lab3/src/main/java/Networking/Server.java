package Networking;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args){
        ServerSocket server = null;
        try {
            server = new ServerSocket(8000);
            while (true){
                Socket client = server.accept();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if ( server != null){
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
