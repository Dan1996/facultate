package FXMLFiles;

import Entities.Contest;
import Entities.Participant;
import Entities.SwimStyle;
import Entities.SwimTask;
import Service.Service;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class View {
    private Service service;

    @FXML
    public TableView<SwimTask> swimTaskView;
    public TableColumn<SwimTask, String> styleColumn;
    public TableColumn<SwimTask, Float> distanceColumn;
    public TableColumn<SwimTask, Integer> nrParticipantsColumn;
    public TableColumn<SwimTask, Integer> idColumn;
    private ObservableList<SwimTask> model;
    private ObservableList<Participant> model2;
    @FXML
    private TextField findField;
    public TableView participantsView;
    public TableColumn nameColumn;
    public TableColumn countryColumn;
    public TableColumn idColumn2;
    @FXML
    private TextField findField1;
    private Stage editStage;
    @FXML
    private TextField idSwimText;
    @FXML
    private TextField idParticipantText;
    @FXML
    private TextField nameText;
    @FXML
    private TextField countryText;

    public void setService(Service service) {
        this.service = service;
        this.loadData();
    }

    @FXML
    public void initialize() {
        styleColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        distanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
        nrParticipantsColumn.setCellValueFactory(new PropertyValueFactory<>("nrOfParticipants"));
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
        findField.textProperty().addListener((observable, oldValue, newValue) -> {
            filterButtonHandler();
        });
        swimTaskView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {selectRowHandler(newValue);
                fillIdText(newValue);});

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        countryColumn.setCellValueFactory(new PropertyValueFactory<>("country"));
        findField1.textProperty().addListener((observable, oldValue, newValue) -> {
            filterButtonHandler1();
        });
        idColumn2.setCellValueFactory(new PropertyValueFactory<>("id"));
        participantsView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {selectRowHandler2((Participant) newValue);
                fillId2Text((Participant) newValue);});
    }
    @FXML
    private void fillIdText(SwimTask swimTask){
        idSwimText.setText(String.valueOf(swimTask.getId()));
    }
    @FXML
    private void fillId2Text(Participant participant){
        idParticipantText.setText(String.valueOf(participant.getId()));
    }
    @FXML
    private void addContestHandler(){
        try {
            Contest contest = new Contest(Integer.valueOf(idParticipantText.getText()), Integer.valueOf(idSwimText.getText()));
            service.addContest(contest);
            loadData1();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void filterButtonHandler() {
        List<SwimTask> result = null;
        String value = findField.getText();
        if (value.equals("")) {
            result = service.findAllSwimTasks("");
        } else
            result = service.findAllSwimTasks(value);
        model.setAll(result);
    }

    @FXML
    private void registerHandler(){
        service.saveParticipant(nameText.getText(),countryText.getText());
        loadData2();
    }

    @FXML
    public void selectRowHandler(SwimTask newValue) {
        if (!(newValue == null)) {
            ArrayList<Participant> list = service.findParticipantByTask(newValue);
            model2 = FXCollections.observableArrayList(list);
            participantsView.setItems(model2);
        }
    }

    @FXML
    public void selectRowHandler2(Participant newValue) {
        if (!(newValue == null)) {
            ArrayList<SwimTask> list = service.findSwimTasksByParticipant(newValue);
            model = FXCollections.observableArrayList(list);
            swimTaskView.setItems(model);
        }
    }

    @FXML
    public void filterButtonHandler1() {
        Participant result = null;
        String value = findField1.getText();
        if (!value.equals("")) {
            result = service.findParticipantByName(value);
            model2.setAll(result);
        } else
            model2.setAll(service.findAllParticipants());

    }

    @FXML
    private void loadData() {
        this.loadData1();
        this.loadData2();
    }
    private void showErrorMessage(String text) {
        Alert message = new Alert(Alert.AlertType.ERROR);
        message.setTitle("Error Message");
        message.initOwner(editStage);
        message.setContentText(text);
        message.showAndWait();
    }

    private void loadData1() {
        ArrayList<SwimTask> list = service.findAllSwimTasks("");
        model = FXCollections.observableArrayList(list);
        swimTaskView.setItems(model);
    }

    private void loadData2() {
        ArrayList<Participant> list = service.findAllParticipants();
        model2 = FXCollections.observableArrayList(list);
        participantsView.setItems(model2);
    }

    @FXML
    public void logOutHandler() {
        System.exit(0);
    }


}
