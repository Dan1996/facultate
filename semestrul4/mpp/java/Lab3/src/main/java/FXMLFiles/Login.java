package FXMLFiles;

import Repository.ContestDBRepository;
import Repository.ParticipantDBRepository;
import Repository.SwimTaskDBRepository;
import Service.Service;
import Utils.MySqlAccess;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class Login {
    Properties serverProps;
    MySqlAccess mySqlAccess;
    Connection connection;
    PreparedStatement preparedStatement;
    private Stage previousStage;

    @FXML
    TextField username;
    private Stage editStage;
    @FXML
    PasswordField password;

    public Login() {
    }

    public void setPreviousStage(Stage previousStage) {
        this.previousStage = previousStage;
    }

    @FXML
    public void initialize() {
        serverProps = new Properties();
        mySqlAccess = new MySqlAccess(serverProps);
        try {
            serverProps.load(new FileReader("bd.config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection = mySqlAccess.getConnection();
    }

    @FXML
    public void logInHandler() {
        try {
            String sql = "SELECT * FROM users WHERE username=? AND password=?";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, username.getText());
            preparedStatement.setString(2, Service.getMD5Hash(password.getText()));
            ResultSet rs = preparedStatement.executeQuery();
            if (!rs.next()) {
                showErrorMessage("Invalid password or id!");
                password.setText("");
            } else {
                loadFirstPage();

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

//    @FXML
//    public void signUpHandler() {
//        try {
//
//            String sql = "SELECT * FROM users WHERE username=? ";
//            preparedStatement = connection.prepareStatement(sql);
//            preparedStatement.setString(1, username.getText());
//         //   preparedStatement.setString(1, Service.getMD5Hash(password.getText()));
//            ResultSet rs = preparedStatement.executeQuery();
//            if (!rs.next()) {
//                showErrorMessage("Username already exists!");
//                password.setText("");
//            } else {
//                loadFirstPage();
//
//            }
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                preparedStatement.close();
//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
//        }
//    }

    private void loadFirstPage() {

        try {
            previousStage.close();
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/View.fxml"));
            AnchorPane editPane = loader.load();
            Stage stage = new Stage();
            stage.initModality(Modality.WINDOW_MODAL);
            stage.setScene(new Scene(editPane));
            View ctrl = loader.getController();
            ParticipantDBRepository participantRepo=new ParticipantDBRepository();
            SwimTaskDBRepository swimTaskRepo=new SwimTaskDBRepository();
            ContestDBRepository contestRepo=new ContestDBRepository();
            Service service=new Service(contestRepo,participantRepo,swimTaskRepo);
            ctrl.setService(service);
            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void showErrorMessage(String text) {
        Alert message = new Alert(Alert.AlertType.ERROR);
        message.setTitle("Error Message");
        message.initOwner(editStage);
        message.setContentText(text);
        message.showAndWait();
    }
}
