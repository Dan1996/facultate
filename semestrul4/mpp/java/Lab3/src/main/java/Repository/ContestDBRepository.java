package Repository;

import Entities.Contest;
import Utils.MySqlAccess;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class ContestDBRepository implements IContestRepository {
    Properties serverProps;
    MySqlAccess mySqlAccess;
    Connection connection;
    PreparedStatement preparedStatement;

    public ContestDBRepository() {
        this.setConnection();
    }

    private void setConnection() {
        serverProps = new Properties();
        mySqlAccess = new MySqlAccess(serverProps);
        try {
            serverProps.load(new FileReader("bd.config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection = mySqlAccess.getConnection();
    }

    @Override
    public ArrayList<Contest> findAll() {
        ArrayList<Contest> list = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement("Select * from contests");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int pid = resultSet.getInt("pid");
                int stid = resultSet.getInt("stid");
                Contest contest = new Contest(pid, stid);
                list.add(contest);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public void save(Contest contest) {
        try {
            preparedStatement = connection.prepareStatement("Insert into contests(pid,stid) values (?,?)");
            preparedStatement.setInt(1, contest.getPid());
            preparedStatement.setInt(2, contest.getStid());
            preparedStatement.executeUpdate();
            //update number of participants
            preparedStatement = connection.prepareStatement("update swimtasks set nrofparticipants=nrofparticipants+1 where id=?");
            preparedStatement.setInt(1, contest.getStid());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void delete(Integer pid, Integer stid) {
        try {
            if(stid==0){
            preparedStatement = connection.prepareStatement("Delete from contests where pid=?");
            preparedStatement.setInt(1, pid);
            preparedStatement.executeUpdate();
            }
            else if(pid==0){
                preparedStatement = connection.prepareStatement("Delete from contests where stid=?");
                preparedStatement.setInt(1, stid);
                preparedStatement.executeUpdate();
            }
            else{
                preparedStatement = connection.prepareStatement("Delete from contests where stid=? and pid=?");
                preparedStatement.setInt(1, stid);
                preparedStatement.setInt(2, pid);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void update(Integer pid, Integer stid, Integer newPid, Integer newStid) {
        try {
            if (newPid == 0) {
                //update just stid
                preparedStatement = connection.prepareStatement("Update contests set stid=? where pid=? and stid=? ");
                preparedStatement.setInt(1, newStid);
                preparedStatement.setInt(2, pid);
                preparedStatement.setInt(3, stid);
                preparedStatement.executeUpdate();
            } else {
                //update just pid
                preparedStatement = connection.prepareStatement("Update contests set pid=? where pid=? and stid=? ");
                preparedStatement.setInt(1, newPid);
                preparedStatement.setInt(2, pid);
                preparedStatement.setInt(3, stid);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
