package Repository;

import Entities.Participant;
import Entities.SwimStyle;
import Entities.SwimTask;
import Utils.MySqlAccess;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class SwimTaskDBRepository implements ISwimTaskRepository {
    Properties serverProps;
    MySqlAccess mySqlAccess;
    Connection connection;
    PreparedStatement preparedStatement;

    public SwimTaskDBRepository() {
        this.setConnection();
        this.setIndex();
    }

    private void setConnection() {
        serverProps = new Properties();
        mySqlAccess = new MySqlAccess(serverProps);
        try {
            serverProps.load(new FileReader("bd.config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection = mySqlAccess.getConnection();
    }

    private void setIndex() {
        int maximIndex = 0;
        try {
            preparedStatement = connection.prepareStatement("Select max(id) as maxim from swimtasks");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int id = resultSet.getInt("maxim");
            if (id > maximIndex)
                maximIndex = id;
            SwimTask.setIndex(maximIndex);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(SwimTask swimTask) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Insert into swimtasks(id,name,distance) values (?,?,?)");
            preparedStatement.setInt(1, swimTask.getId());
            preparedStatement.setString(2, String.valueOf(swimTask.getName()));
            preparedStatement.setFloat(3, swimTask.getDistance());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public SwimTask delete(Integer id) {
        SwimTask swimTask = null;
        try {
            preparedStatement = connection.prepareStatement("Select * from swimtasks where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int id2 = resultSet.getInt("id");
            SwimStyle name = SwimStyle.valueOf(resultSet.getString("name"));
            float distance = resultSet.getFloat("distance");
            swimTask = new SwimTask(id2, name, distance);
            preparedStatement = connection.prepareStatement("Delete from swimtasks where id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return swimTask;
    }

    @Override
    public SwimTask findOne(Integer id) {
        SwimTask swimTask = null;
        try {

            preparedStatement = connection.prepareStatement("Select * from swimtasks where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int id2 = resultSet.getInt("id");
            SwimStyle name = SwimStyle.valueOf(resultSet.getString("name"));
            float distance = resultSet.getFloat("distance");
            swimTask = new SwimTask(id2, name, distance);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return swimTask;
    }

    @Override
    public void update(Integer id, SwimTask swimTask) {
        try {
            preparedStatement = connection.prepareStatement("Update swimtasks set name = ?, distance = ? where id = ? ");
            preparedStatement.setString(1, String.valueOf(swimTask.getName()));
            preparedStatement.setFloat(2, swimTask.getDistance());
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ArrayList findAll() {
        return null;
    }

    @Override
    public ArrayList findAll2(String name) {
        ArrayList<SwimTask> list = new ArrayList<>();
        String sql;
        try {
            if (name.equals("")) {
                sql = "Select * from swimtasks";
                preparedStatement = connection.prepareStatement(sql);
            } else {
                sql = "Select * from swimtasks where name like ?";
                preparedStatement = connection.prepareStatement(sql);
                preparedStatement.setString(1, String.valueOf(name) + "%");
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                SwimStyle name2 = SwimStyle.valueOf(resultSet.getString("name"));
                float distance = resultSet.getFloat("distance");
                int nr = resultSet.getInt("nrofparticipants");
                SwimTask swimTask = new SwimTask(id, name2, distance, nr);
                list.add(swimTask);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public ArrayList findByParticipants(Participant participant) {
        ArrayList<SwimTask> list = new ArrayList<>();
        SwimTask swimTask;
        try {
            preparedStatement = connection.prepareStatement("select * from swimtasks where id in(Select stid FROM contests WHERE pid=?)");
            preparedStatement.setInt(1, participant.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                SwimStyle style = SwimStyle.valueOf(resultSet.getString("name"));
                Float distance = resultSet.getFloat("distance");
                Integer nrParticipants = resultSet.getInt("nrofparticipants");
                swimTask = new SwimTask(id, style, distance, nrParticipants);
                list.add(swimTask);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return list;
    }
}
