package Repository;

import Entities.Participant;
import Entities.SwimStyle;
import Entities.SwimTask;

import java.util.ArrayList;

public interface ISwimTaskRepository extends IRepository<SwimTask,Integer> {
    public ArrayList findAll2(String name);
    public ArrayList findByParticipants(Participant participant);
}
