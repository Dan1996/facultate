package Repository;

import Entities.Participant;
import Entities.SwimTask;

import java.util.ArrayList;

public interface IParticipantRepository extends IRepository<Participant, Integer> {
    public Participant findByName(String name);
    public ArrayList findAllByTask(SwimTask task);
}
