package Repository;

import Entities.Contest;

import java.util.ArrayList;

public interface IContestRepository {
    void save(Contest contest);
    void delete(Integer pid, Integer stid);
    void update(Integer pid, Integer stid, Integer newPid, Integer newStId);
    ArrayList<Contest> findAll();
}
