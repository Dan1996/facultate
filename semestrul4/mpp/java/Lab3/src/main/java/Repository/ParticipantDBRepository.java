package Repository;

import Entities.Participant;
import Entities.SwimTask;
import Utils.MySqlAccess;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;

public class ParticipantDBRepository implements IParticipantRepository {
    Properties serverProps;
    MySqlAccess mySqlAccess;
    Connection connection;
    PreparedStatement preparedStatement;

    public ParticipantDBRepository() {
        this.setConnection();
        this.setIndex();
    }

    private void setConnection() {
        serverProps = new Properties();
        mySqlAccess = new MySqlAccess(serverProps);
        try {
            serverProps.load(new FileReader("bd.config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        connection = mySqlAccess.getConnection();
    }

    private void setIndex() {
        int maximIndex = 0;
        try {
            preparedStatement = connection.prepareStatement("Select max(id) as maxim from participants");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int id = resultSet.getInt("maxim");
            if (id > maximIndex)
                maximIndex = id;
            Participant.setIndex(maximIndex);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(Participant participant) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Insert into participants(id,name,country) values (?,?,?)");
            preparedStatement.setInt(1, participant.getId());
            preparedStatement.setString(2, participant.getName());
            preparedStatement.setString(3, participant.getCountry());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Participant delete(Integer id) {
        Participant participant = null;
        try {
            preparedStatement = connection.prepareStatement("Select * from participants where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            int id2 = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String country = resultSet.getString("country");
            participant = new Participant(id2, name, country);
            preparedStatement = connection.prepareStatement("Delete from participants where id=?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return participant;
    }

    @Override
    public Participant findOne(Integer id) {
        Participant participant = null;
        try {

            preparedStatement = connection.prepareStatement("Select * from participants where id = ?");
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            int id2 = resultSet.getInt("id");
            String name = resultSet.getString("name");
            String country = resultSet.getString("country");
            participant = new Participant(id2, name, country);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return participant;
    }

    @Override
    public void update(Integer id, Participant participant) {
        try {
            preparedStatement = connection.prepareStatement("Update participants set name = ?, country = ? where id = ? ");
            preparedStatement.setString(1, participant.getName());
            preparedStatement.setString(2, participant.getCountry());
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ArrayList findAll() {
        ArrayList<Participant> list = new ArrayList<>();
        try {
            preparedStatement = connection.prepareStatement("Select * from participants");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                String country = resultSet.getString("country");
                Participant participant = new Participant(id, name, country);
                list.add(participant);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Participant findByName(String name) {
        Participant participant = null;
        try {
            preparedStatement = connection.prepareStatement("Select * from participants where name = ?");
            preparedStatement.setString(1, name);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
            int id = resultSet.getInt("id");
            String name2 = resultSet.getString("name");
            String country = resultSet.getString("country");
            participant = new Participant(id, name2, country);}


        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return participant;
    }

    @Override
    public ArrayList findAllByTask(SwimTask task) {
        ArrayList<Participant> list = new ArrayList<>();
        Participant participant;
        try {
            preparedStatement = connection.prepareStatement("select * from participants where id in(Select pid FROM contests WHERE stid=?)");
            preparedStatement.setInt(1, task.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String name2 = resultSet.getString("name");
                String country = resultSet.getString("country");
                participant = new Participant(id, name2, country);
                list.add(participant);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
        return list;
    }

}
