package Repository;

import java.util.ArrayList;

public interface IRepository<E, ID> {
    void save(E entity);

    E delete(ID id);

    E findOne(ID id);

    ArrayList<E> findAll();

    void update(ID id, E entity);

}
