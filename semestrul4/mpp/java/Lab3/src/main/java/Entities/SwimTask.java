package Entities;

import Utils.HasId;

public class SwimTask  implements HasId<Integer>{
    int id;
    private static int index = 0;
    SwimStyle name;
    float distance;
    int nrOfParticipants;

    public int getNrOfParticipants() {
        return nrOfParticipants;
    }
    public SwimTask(int id, SwimStyle name, float dintance) {
        this.id = id;
        this.name = name;
        this.distance = dintance;
    }

    public SwimTask(int id, SwimStyle name, float dintance,int nrOfParticipants) {
        this.id = id;
        this.name = name;
        this.distance = dintance;
        this.nrOfParticipants=nrOfParticipants;
    }

    public SwimTask(SwimStyle name, float distance,int nrOfParticipants) {
        index++;
        this.id = index;
        this.name = name;
        this.distance = distance;
        this.nrOfParticipants=nrOfParticipants;
    }
    @Override
    public Integer getId() {
        return this.id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public static int getIndex() {
        return index;
    }

    public static void setIndex(int index) {
        SwimTask.index = index;
    }

    public SwimStyle getName() {
        return name;
    }

    public void setName(SwimStyle name) {
        this.name = name;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "SwimTask{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dintance=" + distance +
                '}';
    }
}
