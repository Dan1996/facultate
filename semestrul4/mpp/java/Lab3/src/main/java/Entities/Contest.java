package Entities;

public class Contest {
    int pid;
    int stid;

    public Contest(int pid, int stid) {
        this.pid = pid;
        this.stid = stid;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getStid() {
        return stid;
    }

    public void setStid(int stid) {
        this.stid = stid;
    }

    @Override
    public String toString() {
        return "Contest{" +
                "pid=" + pid +
                ", stid=" + stid +
                '}';
    }
}
