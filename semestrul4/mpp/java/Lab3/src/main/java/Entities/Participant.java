package Entities;

import Utils.HasId;

public class Participant implements HasId<Integer> {
    int id;
    private static int index = 0;
    String name;
    String country;

    public Participant(int id, String name, String country) {
        this.id = id;
        this.name = name;
        this.country = country;
    }
    public Participant(String name, String country) {
        index++;
        this.id = index;
        this.name = name;
        this.country = country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public static int getIndex() {
        return index;
    }

    public static void setIndex(int index) {
        Participant.index = index;
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
