package UI;

import Entities.Contest;
import Entities.Participant;
import Entities.SwimStyle;
import Entities.SwimTask;
import Repository.IContestRepository;
import Repository.IRepository;

import java.util.ArrayList;
import java.util.Scanner;

public class Console {
    private IRepository<Participant, Integer> repoParticipant;
    private IRepository<SwimTask, Integer> repoSwimTask;
    private IContestRepository repoContest;
    private Scanner in = new Scanner(System.in);

    public Console(IRepository<Participant, Integer> repoParticipant, IRepository<SwimTask, Integer> repoSwimTask, IContestRepository repoContest) {
        this.repoParticipant = repoParticipant;
        this.repoSwimTask = repoSwimTask;
        this.repoContest = repoContest;
    }

    private void showMainMenu() {
        System.out.println("SWIMMERS COMPETITION");
        System.out.println("1.Add Participant.");
        System.out.println("2.Delete  Participant.");
        System.out.println("3.Update Participant.");
        System.out.println("4.See the list of Participants.");
        System.out.println("5.Add SwimTask.");
        System.out.println("6.Delete SwimTask.");
        System.out.println("7.Update SwimTask.");
        System.out.println("8.See the list of SwimTasks.");
        System.out.println("9.Add Contest.");
        System.out.println("10.Delete Contest.");
        System.out.println("11.Update Contest.");
        System.out.println("12.See the list of Contests.");
    }

    private void saveParticipant() {
        try {
            System.out.println("Give the name:");
            String name = in.nextLine();
            System.out.println("Give the country:");
            String country = in.nextLine();
            Participant participant = new Participant(name, country);
            repoParticipant.save(participant);
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteParticipant() {
        try {
            System.out.println("Give the id:");
            Integer id = in.nextInt();
            Participant participant = repoParticipant.delete(id);
            System.out.println("Success! Deleted:" + participant);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateParticipant() {
        try {
            System.out.println("Give the id of the swimmer you want to change:");
            Integer id = in.nextInt();
            System.out.println("Give the new name:");
            String name = in.nextLine();
            System.out.println("Give the new country:");
            String country = in.nextLine();
            Participant participant = new Participant(name, country);
            repoParticipant.update(id, participant);
            System.out.println("Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findAllParticipants() {
        ArrayList<Participant> list = repoParticipant.findAll();
        for (Participant participant : list) {
            System.out.println(participant);
        }
    }

    private void saveSwimTask() {
        try {
            System.out.println("Give the name:");
            String name = in.nextLine();
            System.out.println("Give the distance:");
            String distance = in.nextLine();
            SwimTask swimTask = new SwimTask(SwimStyle.valueOf(name), Float.valueOf(distance),0);
            repoSwimTask.save(swimTask);
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteSwimTask() {
        try {
            System.out.println("Give the id:");
            Integer id = in.nextInt();
            SwimTask swimTask = repoSwimTask.delete(id);
            System.out.println("Success! Deleted:" + swimTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateSwimTask() {
        try {
            System.out.println("Give the id of the swwimtask you want to change:");
            Integer id = in.nextInt();
            System.out.println("Give the new name:");
            String name = in.nextLine();
            System.out.println("Give the new distance:");
            float distance = in.nextFloat();
            SwimTask swimTask = new SwimTask(SwimStyle.valueOf(name), distance,0);///jnim
            repoSwimTask.update(id, swimTask);
            System.out.println("Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findAllSwimTasks() {
        ArrayList<SwimTask> list = repoSwimTask.findAll();
        for (SwimTask swimTask : list) {
            System.out.println(swimTask);
        }
    }

    private void saveContest() {
        try {
            System.out.println("Give the participant id:");
            Integer pid = in.nextInt();
            System.out.println("Give the swimtask id:");
            Integer stid = in.nextInt();
            Contest contest = new Contest(pid, stid);
            repoContest.save(contest);
            System.out.println("Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteContest() {
        try {
            System.out.println("Give 0 where you don't want to delete");
            System.out.println("Give the pid:");
            Integer pid = in.nextInt();
            System.out.println("Give the stid:");
            Integer stid = in.nextInt();
            repoContest.delete(pid, stid);
            System.out.println("Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateContest() {
        try {
            System.out.println("Give the pid:");
            Integer pid = in.nextInt();
            System.out.println("Give the stid:");
            Integer stid = in.nextInt();
            System.out.println("Give 0 where you don't want to update");
            System.out.println("Give the new pid:");
            Integer pid2 = in.nextInt();
            System.out.println("Give the new stid:");
            Integer stid2 = in.nextInt();
            repoContest.update(pid, stid, pid2, stid2);
            System.out.println("Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findAllContests() {
        ArrayList<Contest> list = repoContest.findAll();
        for (Contest contest : list) {
            System.out.println(contest);
        }
    }

    public void run() {
        String option;
        while (true) {
            showMainMenu();
            System.out.println("Your option:");
            option = in.nextLine();
            switch (option) {
                case "1": {
                    this.saveParticipant();
                    break;
                }
                case "2": {
                    this.deleteParticipant();
                    break;
                }
                case "3": {
                    this.updateParticipant();
                    break;
                }
                case "4": {
                    this.findAllParticipants();
                    break;
                }
                case "5": {
                    this.saveSwimTask();
                    break;
                }
                case "6": {
                    this.deleteSwimTask();
                    break;
                }
                case "7": {
                    this.updateSwimTask();
                    break;
                }
                case "8": {
                    this.findAllSwimTasks();
                    break;
                }
                case "9": {
                    this.saveContest();
                    break;
                }
                case "10": {
                    this.deleteContest();
                    break;
                }
                case "11": {
                    this.updateContest();
                    break;
                }
                case "12": {
                    this.findAllContests();
                    break;
                }
                case "0": {
                    System.out.println("Exit.");
                    return;
                }
                default: {
                    System.out.println("Invalid option.");
                    break;
                }
            }
        }
    }
}
