black_list = ["=", "(", ")", "'", ","]
with open("models.py") as f:
    with open("new.py", 'w') as r:
        for line in f:
            for i, char in enumerate(line):
                if char in black_list:
                    r.write(' ')
                else:
                    r.write(char)
