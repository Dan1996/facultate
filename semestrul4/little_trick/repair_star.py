import os

current_dir = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(current_dir, 'models.py'), 'w') as models:
    for el in os.listdir(os.path.join(current_dir, 'Model', 'models.py')):
        with open(os.path.join(current_dir, 'Model', 'models.py', el)) as f:
            start = False
            for command in f:
                if 'class' in command:
                    models.write(command)
                try:
                    if start and command:
                        command = command.strip()
                        command = command[5:-7]
                        parts = iter(command.split())
                        chars_to_cut = False
                        command = '{} = {}('.format(next(parts), next(parts))
                        if 'ForeignKey' in command:
                            command += next(parts) + ' ,'
                        for i, b in enumerate(parts):
                            chars_to_cut = True
                            if i % 2 == 0:
                                command += b + '='
                            else:
                                command += b + ', '
                        if chars_to_cut:
                            command = command[: -2]
                        command = '\t' + command + ')' + '\n'
                        models.write(command)

                except StopIteration:
                    continue
                if '__init__' in command:
                    start = True
            models.write('\n')

# with open(os.path.join(current_dir, 'Model/models.py/models.py'), 'w') as models:
#
#
