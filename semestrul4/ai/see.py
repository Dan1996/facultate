import random
a = [1,2,3,4,5,6,7]

random.shuffle(a)

sorted = False
while not sorted:
    sorted = True
    for i in range(len(a) - 1):
        if a[i] >= a[i + 1]:
            a[i], a[i + 1] = a[i + 1], a[i]
            sorted = False

print(a)
