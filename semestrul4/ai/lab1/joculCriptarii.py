import time
from collections import deque
from copy import deepcopy
import re


# shadowing real hex, too lazy to do it properly
def hex(integer):
    return format(integer, "x")


def bfs(words, signs, base, numbers, pool):
    solution_lenght = len(pool)
    carries = prepare_words(words, pool)

    x = deque()
    x.appendleft(({}, pool))

    while x:
        el, pool = x.pop()

        is_solution = solution(el, signs, base, solution_lenght, *words)
        if is_solution:
            return is_solution

        if solution_lenght != len(el):
            if len(words) == 3:
                modify_entities_on_session(words, pool, carries, numbers, base, el)
            current_letter = min(pool, key=lambda y: len(pool[y]))
            current_numbers = pool[current_letter]

            for i in current_numbers:
                el[current_letter] = i
                new_pool = deepcopy(pool)
                new_pool.__delitem__(current_letter)
                if i not in {el[key] for key in el if el[key] == i and key != current_letter}:
                    x.appendleft((deepcopy(el), new_pool))
                el.__delitem__(current_letter)


def solution(el, signs, base, solution_length, *words):
    if len(el) == solution_length:
        rez = []
        for word in words:
            rez.append("")
            for let in word:
                if let != '?':
                    rez[-1] += el[let]

        signs_it = iter(signs)
        rez_it = iter(rez)

        sign = next(signs_it)
        rezult = int(next(rez_it), base)

        while sign != "=":
            if sign == "+":
                rezult += int(next(rez_it), base)
            else:
                rezult -= int(next(rez_it), base)
            sign = next(signs_it)

        if rezult == int(next(rez_it), base):
            return rez, el


def get_base_numbers():
    base = 0
    print("Write the base in which you want to generate your result! (10 or 16)")
    while base != 10 and base != 16:
        try:
            base = int(input())
        except ValueError:
            print("Wrong! Type again\n")

    if base == 16:
        numbers = [hex(i) for i in range(0, 16)]
    else:
        numbers = [str(i) for i in range(10)]

    return base, numbers


def get_words_signs():
    while True:
        print("Write equation of the form 'arg1 operator1 arg2 opartor2 .... opartorN = argN' \n")
        # commands = input()
        # commands = "ABCD + EBCB = AFGAG"
        # commands = "POTATO + TOMATO = PUMPKIN"
        commands = "SEND + MORE = MONEY"
        # commands = "EAT + THAT = APPLE"
        # commands = "NEVER - DRIVE = RIDE"
        # commands = "TAKE + A + CAKE = KATE"
        try:
            return parse_commands(commands)
        except TypeError:
            print("The command is not valid, try again \n")


def choose_method():
    print('What method do you want to use? 1.Bfs 2.Greedy')
    while True:
        cmd = input()
        if cmd in {'1', '2'}:
            break
        print('Please try again the input must be 1 or 2')
    return cmd


def main():
    base, numbers = get_base_numbers()

    words, signs = get_words_signs()

    if len(words) == 3 and '-' in signs:
        words[0], words[2] = words[2], words[0]
        signs[0] = "+"

    pool = make_pool(words, numbers)

    method = choose_method()
    # some validation for words is required

    a = time.time()
    if method == '1':
        sol = bfs(words, signs, base, numbers, pool)
    else:
        try:
            sol = greedy(words, numbers, base, signs)
        except:
            sol = None
    if sol:
        print(sol[0])
        print(sol[1])
    else:
        print("No solution was found!")
    print("Time spent on bfs: ", time.time() - a)


def parse_commands(commands):
    commands = re.split(" +", commands)
    words = []
    signs = []
    for i, command in enumerate(commands):
        if i % 2 == 0:
            words.append(command)
        elif command in ["+", "=", "-"]:
            if "=" in signs:
                return None
            signs.append(command)
        elif command == '':
            pass
        else:
            return None

    if "=" not in signs:
        return None

    return words, signs


def make_pool(words, numbers):
    pool = {}
    for word in words:
        for let in word:
            if not pool.get(let):
                pool[let] = list(numbers)
    return pool


def prepare_words(words, pool):
    max_length_word = max(words, key=lambda x: len(x))
    unknown_carry = -1
    unknown_char = '?'

    # o means that the carry is not determined yet
    carrys = [unknown_carry] * (len(max_length_word) - 1)
    carrys.append(0)
    carryfirst = True

    for i, word in enumerate(words):
        # remove 0 from letter pool if it is the first letter
        if "0" in pool[word[0]]:
            pool[word[0]].remove("0")

        if len(max_length_word) == len(word) and word != max_length_word:
            carryfirst = False
        words[i] = (len(max_length_word) - len(word)) * unknown_char + word

    # if u some 2 numbers and the result is bigger, there is certainly a carry
    if carryfirst:
        carrys[0] = 1
        pool[words[-1][0]] = ["1"]
    else:
        carrys[0] = 0

    return carrys


def remove_combination_from_space(pool, char1, char2, base, letters, carry):
    rez = set()
    if char1 in pool:
        m1 = deepcopy(pool[char1])
    else:
        m1 = {letters[char1]}

    if char2 in pool:
        m2 = deepcopy(pool[char2])
    else:
        m2 = {letters[char2]}

    # if the carry is unknown consider it 1
    if carry == -1:
        carry = 1

    for val1 in m1:
        for val2 in m2:
            if int(val1, base) + int(val2, base) + carry >= base:
                rez.add(val1)
                rez.add(val2)

    if char1 in pool:
        for el in m1:
            if el not in rez:
                pool[char1].remove(el)
    if char2 in pool:
        for el in list(pool[char2]):
            if el not in rez:
                pool[char2].remove(el)


def modify_entities_on_session(words, pool, carries, numbers, base, letters):
    # EXPECT THIS FUNCTION TO BE HUGE, JUST CLEAN ENTITIES HERE
    if base == 16:
        transform = hex
    else:
        transform = str
    for i in range(len(words[1])):

        # nothing + A = B => B is 9
        # carries[i - 1] will generate no error because last carry is 0 everytime
        if i != 0 and not letters.get(words[1][i]) and\
                carries[i - 1] == 1 and words[0][i] == '?' and \
                letters.get(words[2][i - 1]):
            pool[words[1][i]] = [transform(base - 1)]

        if i != 0 and not letters.get(words[0][i]) and\
                carries[i - 1] == 1 and words[1][i] == '?' and\
                letters.get(words[2][i - 1]):
            pool[words[0][i]] = [transform(base - 1)]

        # compute digists is only 1 digit on the row
        if not letters.get(words[2][i]) and\
                i != 0 and \
                words[1][i] == '?' and\
                letters.get(words[0][i]) and \
                letters.get(words[2][i - 1]) and \
                words[0][i - 1] == '?' and \
                words[1][i - 1] == '?':
            value = transform((int(letters[words[0][i]], base)) % base)
            second_value = transform((int(value, base) + 1) % base)
            values = [value]
            if carries[i] == -1:
                values.append(second_value)
            elif carries[i] == 1:
                values = [second_value]
            for val in list(pool[words[2][i]]):
                if val not in values:
                    pool[words[2][i]].remove(val)

        if not letters.get(words[2][i]) and\
                i != 0 and \
                words[0][i] == '?' and\
                letters.get(words[1][i]) and \
                letters.get(words[2][i - 1]) and \
                words[0][i - 1] == '?' and \
                words[1][i - 1] == '?':
            value = transform((int(letters[words[1][i]], base)) % base)
            second_value = transform((int(value, base) + 1) % base)
            values = [value]
            if carries[i] == -1:
                values.append(second_value)
            elif carries[i] == 1:
                values = [second_value]
            for val in list(pool[words[2][i]]):
                if val not in values:
                    pool[words[2][i]].remove(val)

        if carries[i - 1] == 1 \
                and words[0][i] != '?' and words[1][i] != '?':
            # => the sum is >= 10 (remove all the elements that do not correspond
            remove_combination_from_space(pool, words[0][i], words[1][i], base, letters, carries[i])

        local_set = ["0", numbers[-1]]
        # B + A = B
        if words[0][i] == words[2][i]:
            pool[words[1][i]] = local_set
        # A + B = B
        if words[1][i] == words[2][i]:
            pool[words[0][i]] = local_set

        if carries[i] == 1:
            local_set.remove("0")
        elif carries[i] == 0:
            local_set.remove(numbers[-1])

        # A + 0 = C => carry
        if not letters.get(words[1][i]) and\
                not letters.get(words[2][i]) and\
                carries[i] == -1 and\
                letters.get(words[0][i]) == "0" and\
                words[1][i] != words[2][i]:
            carries[i] = 1
            remove_in_front(pool[words[1][i]], pool[words[2][i]], 1, base, transform, carries[i])

        if not letters.get(words[0][i]) and\
                not letters.get(words[2][i]) and\
                carries[i] == -1 and\
                letters.get(words[1][i]) == "0" and\
                words[0][i] != words[2][i]:
            carries[i] = 1
            remove_in_front(pool[words[0][i]], pool[words[2][i]], 1, base, transform, carries[i])

        # A + A = A  => if carry: A = numbers[-1] else A = {0, numbers[-1]}
        if words[0][i] == words[1][i] == words[2][i]:
            local_set = {"0", numbers[-1]}
            pool[words[0][i]] = local_set
            if carries[i] == 1:
                local_set.remove("0")
            elif carries[i] == 0:
                local_set.remove(numbers[-1])

        # if I have 2 digits sum them for the result
        compute_some_if_2_exist(words[0][i], words[1][i], words[2][i],
                                letters, base, carries[i], transform, pool)
        compute_some_if_2_exist(words[0][i], words[2][i], words[1][i],
                                letters, base, carries[i], transform, pool, sign=True)

        compute_some_if_2_exist(words[1][i], words[2][i], words[0][i],
                                letters, base, carries[i], transform, pool, sign=True)

        # if I have 3 digits the carry is determined
        if letters.get(words[0][i]) and letters.get(words[1][i]) and letters.get(words[2][i]):
            a, b = int(letters[words[0][i]], base), int(letters[words[1][i]], base)
            c = int(letters[words[2][i]], base)
            if i == len(carries) - 1 or (a + b) % base == c:
                carries[i] = 0
            else:
                carries[i] = 1

            if i != 0 and a + b + carries[i] >= base:
                carries[i - 1] = 1
            else:
                carries[i - 1] = 0


def compute_some_if_2_exist(char1, char2, char3, letters, base, carry, transform, pool, sign=None):
    # if I have 2 digits sum them for the result
    if char3 != '?' and \
            char2 != '?' and \
            char1 != '?' and \
            not letters.get(char3) and letters.get(char1) and letters.get(char2):
        if not sign:
            posible_value = (int(letters[char1], base) + int(letters[char2], base)) % base
        else:
            posible_value = (int(letters[char2], base) + base - int(letters[char1], base)) % base

        compute_values = {posible_value}
        if not sign:
            second_posible_value = (posible_value + 1) % base
        else:
            second_posible_value = (base + posible_value - 1) % base

        if carry == 1:
            compute_values = {second_posible_value}
        elif carry == -1:
            compute_values.add(second_posible_value)
        compute_values = {transform(el) for el in compute_values}

        for val in list(pool[char3]):
            if val not in compute_values:
                pool[char3].remove(val)


def remove_in_front(m1, m2, step, base, translate, carry):
    if carry == -1:
        carry = 1
    for el in list(m1):
        if translate((int(el, base) + step + carry) % base) not in m2 or\
                translate((int(el, base) + step) % base) not in m2:
            m1.remove(el)
    for el in list(m2):
        if translate((int(el, base) + base - step - carry) % base) not in m1 or\
                translate((int(el, base) + base - step) % base) not in m1:
            m2.remove(el)


def select_most_promising(pool, base):
    best_letter_to_choose_from = min(pool, key=lambda x: len(pool[x]))
    # selected_list = pool[best_letter_to_choose_from]
    # selected_ints = [int(el, base) for el in selected_list]
    #
    # average = sum(selected_ints) / len(selected_list)
    # max_item = base, base
    # for i, el in enumerate(selected_ints):
    #     if max_item[1] >= abs(average - el):
    #         max_item = i, abs(average - el)

    return best_letter_to_choose_from, pool[best_letter_to_choose_from][
        len(pool[best_letter_to_choose_from]) // 2
        # max_item[0]
    ]


def remove_candidate(candidate, pool):
    pool.__delitem__(candidate[0])
    for key, value in pool.items():
        if candidate[1] in value:
            value.remove(candidate[1])


def greedy(words, numbers, base, signs):
    letters = {}  # empty set as a candidate
    pool = make_pool(words, numbers)
    solution_length = len(pool)
    carries = prepare_words(words, pool)

    while pool != {}:
        modify_entities_on_session(words, pool, carries, numbers, base, letters)
        candidate = select_most_promising(pool, base)
        remove_candidate(candidate, pool)

        letters[candidate[0]] = candidate[1]

    is_solution = solution(letters, signs, base, solution_length, *words)
    if is_solution:
        return is_solution
    return None


if __name__ == '__main__':
    main()
