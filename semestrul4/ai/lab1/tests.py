from lab1.joculCriptarii import make_pool, bfs, greedy


def test():
    words = ["SEND", "MORE", "MONEY"]
    base = 10
    numbers = [str(i) for i in range(10)]
    signs = ["+", '=']

    pool = make_pool(words, numbers)
    rez = (['9567', '1085', '10652'],
           {'M': '1', 'S': '9', 'O': '0', 'E': '5',
           'N': '6', 'R': '8', 'D': '7', 'Y': '2'})
    assert bfs(words, signs, base, numbers, pool) == rez

    words = ["SEND", "MORE", "MONEY"]
    assert greedy(words, numbers, base, signs) == rez

    words = ["EAT", "THAT", "APPLE"]
    pool = make_pool(words, numbers)
    rez = (['819', '9219', '10038'],
           {'A': '1', 'T': '9', 'E': '8',
           'P': '0', 'L': '3', 'H': '2'})
    assert bfs(words, signs, base, numbers, pool) == rez

    words = ["EAT", "THAT", "APPLE"]
    assert greedy(words, numbers, base, signs) == rez

    words = ["RIDE", "DRIVE", "NEVER"]
    pool = make_pool(words, numbers)
    signs = ["+", "="]
    rez = (['6783', '86753', '93536'],
           {'R': '6', 'E': '3', 'D': '8',
            'V': '5', 'N': '9', 'I': '7'})
    assert bfs(words, signs, base, numbers, pool) == rez

    words = ["TAKE", "A", "CAKE", "KATE"]
    signs = ["+", "+", "="]
    pool = make_pool(words, numbers)
    assert bfs(words, signs, base, numbers, pool) == (['3961', '9', '2961', '6931'],
                                                      {'T': '3', 'A': '9', 'K': '6',
                                                       'C': '2', 'E': '1'})


test()
