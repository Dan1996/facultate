import csv


def make_new_csv(file):
    with open('csv') as csv_file:
        with open('new_csv', 'w') as dest:
            spamreader = csv.reader(csv_file)
            for line in spamreader:
                a = [line[1]] + line[3:5] + line[6:] + [line[5]]
                dest.write(','.join(a) + '\n')

make_new_csv('csv')