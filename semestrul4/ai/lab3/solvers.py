import random
from random import randint
import numpy as np
from numpy.linalg import inv
import abc


class AbstractSolver(abc.ABC):
    def __init__(self, file_name):
        self.input = file_name
        self.learn()

    @abc.abstractmethod
    def learn(self):
        self.X = np.genfromtxt(self.input, delimiter=',')
        self.Y = self.X[:, -1]
        self.X = self.X[:, :-1]

    def get_err_avg(self, beta_values):
        avg_err = 0
        for i in range(self.X.shape[0]):
            rez = self(self.X[i], beta_values)
            avg_err += np.absolute(rez - self.Y[i])
        avg_err /= self.X.shape[0]
        return avg_err

    def __call__(self, X, beta_values):
        return np.add.reduce(beta_values * X)


class LeastSquaresLineSolver(AbstractSolver):
    def learn(self):
        super().learn()
        XT = self.X.T
        self.BetaValues = (inv(XT @ self.X) @ XT) @ self.Y


class GradientDescentSolver(AbstractSolver):
    def learn(self):
        super().learn()
        lr = 0.00002
        self.Beta0 = 0
        self.BetaValues = np.array([0 for i in range(self.X.shape[1])])
        for k in range(1000):
            for i in range(self.X.shape[0]):
                computed = self(self.X[i], self.BetaValues)
                error = computed - self.Y[i]
                for j in range(self.BetaValues.shape[0]):
                    self.BetaValues[j] -= lr * error * self.X[i][j]
                self.Beta0 -= lr * error

    def __call__(self, X, beta_values):
        return super().__call__(X, beta_values) + self.Beta0


class GeneticSolver(AbstractSolver):
    def learn(self):
        self.population_size = 100
        self.generations = 500
        self.population = Population(self.population_size)
        self.interval = [-9, 9]
        super().learn()
        length = self.X.shape[1]
        self.population.initialize(self.interval, length)
        self.population.eval(self.X, self)
        aux_population = Population(self.population_size)

        for _ in range(self.generations):
            for __ in range(len(self.population.population) // 2):
                p1 = self.population.selection()
                p2 = self.population.selection()

                o1, o2 = p1.crossover(p2, self.interval)
                o1.mutate(self.interval)
                o2.mutate(self.interval)

                o1.eval(self.X, self)
                o2.eval(self.X, self)

                xx = [p1, p2, o1, o2]
                xx.sort()

                aux_population.add(xx[0])
                aux_population.add(xx[1])
            self.population = aux_population
            aux_population = Population(self.population_size)
        self.BetaValues = min(self.population.population).genes


class Individ:
    def __init__(self, genes):
        self.genes = genes

    def crossover(self, p2, interval):
        a = np.concatenate([self.genes[:self.genes.shape[0] // 2], p2.genes[p2.genes.shape[0] // 2:]])
        b = np.concatenate([p2.genes[:p2.genes.shape[0] // 2], self.genes[self.genes.shape[0] // 2:]])
        return Individ(a), \
               Individ(b)

    def mutate(self, interval):
        poz = randint(0, 2)
        val = [interval[0], 0, interval[1]][poz]
        self.genes[randint(0, self.genes.shape[0] - 1)] = val

    def eval(self, X, solver):
        self.fitness = solver.get_err_avg(self.genes)

    def __str__(self):
        return str(self.genes)+" "+str(self.fitness)

    def __lt__(self, other):
        return self.fitness < other.fitness


class Population:
    def __init__(self, size):
        self.population = []
        self.size = size
        self.size_turnir = size//5

    def initialize(self, interval, length):
        for i in range(self.size):
            self.population.append(Individ(np.array([random.uniform(*interval) for i in range(length)])))

    def eval(self, X, solver):
        for individ in self.population:
            individ.eval(X, solver)

    def selection(self):
        turnir = []
        for i in range(self.size_turnir):
            r = randint(0, self.size - 1)
            turnir.append(self.population[r])
        turnir.sort()
        return turnir[0]

    def add(self, o):
        self.population.append(o)


if __name__ == '__main__':
    file = 'new_csv'
    choices = 'age,test_time,Jitter(%),Jitter(Abs),' \
              'Jitter:RAP,Jitter:PPQ5,Jitter:DDP,' \
              'Shimmer,Shimmer(dB),Shimmer:APQ3,' \
              'Shimmer:APQ5,Shimmer:APQ11,Shimmer:DDA,' \
              'NHR,HNR,RPDE,DFA,PPE'
    while True:
        choose = input("Introduceti algoritmul dorit: patrate, gradient, genetic pentru a afla total_UPDRS")
        # input()
        # choose = 'patrate'
        # choose = 'gradient'
        # choose = 'genetic'
        print("wait")
        solver = {
            'patrate': LeastSquaresLineSolver,
            'gradient': GradientDescentSolver,
            'genetic': GeneticSolver,
        }.get(choose)
        if not solver:
            continue
        solver = solver(file)
        print(solver.get_err_avg(solver.BetaValues))
        cmd = input("Doriti sa iesiti? ")
        if cmd == 'da':
            break
        else:
            continue
