import abc
from random import randint, random
import random as r
import numpy as np


def map_values(x):
    if x == 'DH':
        return 0.0
    elif x == 'SL':
        return 0.33
    else:
        return 0.66


def get_mapped_solution(x):
    if x <= 0.33:
        return 0
    elif x <= 0.66:
        return 0.33
    else:
        return 0.66


class AbstractSolver(abc.ABC):
    def __init__(self, file_name, iterations):
        self.input = file_name
        self.iterations = iterations

    @abc.abstractmethod
    def learn(self):
        self.X = np.genfromtxt(self.input, delimiter=' ', dtype=str)
        self.Y = self.X[:, -1]
        self.X = self.X[:, :-1]
        self.X = np.vectorize(float)(self.X)
        self.Y = np.vectorize(map_values)(self.Y)
        self.X_R = self.X[:self.X.shape[0] // 3]
        self.X_T = self.X[self.X.shape[0] // 3:]

        self.Y_R = self.Y[:self.Y.shape[0] // 3]
        self.Y_T = self.Y[self.Y.shape[0] // 3:]

    def get_err_avg(self, beta_values):
        avg = 0
        for i in range(self.X_R.shape[0]):
            rez = self(self.X_R[i], beta_values)

            rez = get_mapped_solution(rez)
            if rez == self.Y_R[i]:
                avg += 1
        return avg, self.X_R.shape[0]

    def __call__(self, X, beta_values):
        return np.add.reduce(beta_values * X)

    @staticmethod
    def activate(x):
        return 1 / (1 + np.exp(-x))


class GradientClasifier(AbstractSolver):
    def learn(self):
        super().learn()
        lr = 0.00002
        self.Beta0 = 0
        self.BetaValues = np.array([random() for _ in range(self.X.shape[1])])
        computed_values = np.array([0 for _ in range(self.X_T.shape[0])])

        for k in range(self.iterations):

            for i in range(self.X_T.shape[0]):
                computed_values[i] = self(self.X_T[i], self.BetaValues)

            for i in range(self.BetaValues.shape[0]):
                gradient = 0.0
                for j in range(self.X_T.shape[0]):
                    error = computed_values[j] - self.Y_T[j]
                    gradient = gradient + self.X_T[j][i] * error

                self.BetaValues[i] -= lr * gradient
                self.Beta0 -= lr * gradient

    def __call__(self, X, beta_values):
        a = super().__call__(X, beta_values) + self.Beta0
        return self.activate(a)


class GeneticSolver(AbstractSolver):
    def learn(self):
        self.population_size = 100
        self.population = Population(self.population_size)
        self.interval = [0, 1]

        super().learn()
        length = self.X.shape[1]
        self.population.initialize(self.interval, length)

        self.population.eval(self.X, self)
        aux_population = Population(self.population_size)

        for _ in range(self.iterations):
            for __ in range(len(self.population.population) // 2):
                p1 = self.population.selection()
                p2 = self.population.selection()

                o1 = p1.crossover(p2)
                o1.mutate(self.interval)

                o1.eval(self.X, self)

                xx = [p1, p2, o1]
                xx.sort()

                aux_population.add(xx[0])
                aux_population.add(xx[1])
            self.population = aux_population
            aux_population = Population(self.population_size)
        self.BetaValues = min(self.population.population).genes


class Individ:
    def __init__(self, genes):
        self.genes = genes

    def crossover(self, p2):
        a = (p2.genes + self.genes) / 2
        return Individ(a)

    def mutate(self, interval):
        poz = randint(0, 2)
        val = [0.0, 0.33, 0.66][poz]
        self.genes[randint(0, self.genes.shape[0] - 1)] = val

    def eval(self, X, solver):
        self.fitness = solver.get_err_avg(self.genes)

    def __str__(self):
        return str(self.genes)+" "+str(self.fitness)

    def __lt__(self, other):
        return self.fitness < other.fitness


class Population:
    def __init__(self, size):
        self.population = []
        self.size = size
        self.size_turnir = size//5

    def initialize(self, interval, length):
        for i in range(self.size):
            self.population.append(Individ(np.array([r.uniform(*interval) for _ in range(length)])))

    def eval(self, X, solver):
        for individ in self.population:
            individ.eval(X, solver)

    def selection(self):
        turnir = []
        for i in range(self.size_turnir):
            r = randint(0, self.size - 1)
            turnir.append(self.population[r])
        turnir.sort()
        return turnir[0]

    def add(self, o):
        self.population.append(o)


file = 'column_3C.datShuffled'
while True:
    name = input("Da numele ")
    if name not in ["genetic", "gradient", "exit"]:
        print("valoare invalida")
        continue

    solver = {
        'genetic': GeneticSolver ,
        'gradient': GradientClasifier,
    }[name]
    try:
        iters = int(input("Introduceti numarul de iteratii "))
    except:
        print("valoare naspa")
        continue

    a = solver(file, iters)
    a.learn()
    print(a.get_err_avg(a.BetaValues))

