from random import randint


class GeneticSolver:
    def __init__(self, codo, **kwargs):
        self.population_size = kwargs.get('population_size', 100)
        self.generations = kwargs.get('generations', 500)
        self.codo = codo
        self.population = Population(self.population_size)

    def solve(self):
        self.population.initialize(self.codo)
        self.population.eval(self.codo)
        aux_population = Population(self.population_size)
        for _ in range(self.generations):
            for __ in range(len(self.population.population) // 2):
                p1 = self.population.selection()
                p2 = self.population.selection()
                o1, o2 = p1.crossover(p2, self.codo)
                o1.mutate()
                o2.mutate()

                o1.eval(self.codo)
                o2.eval(self.codo)
                xx = [p1, p2, o1, o2]
                xx.sort()

                aux_population.add(xx[0])
                aux_population.add(xx[1])
            self.population = aux_population
            aux_population = Population(self.population_size)
        return self.codo[int(min(self.population.population).genes, 2)]


class Individ:
    def __init__(self, genes):
        self.genes = genes
        self.fitness = 0

    def crossover(self, p2, codo):
        repr_size = len(format(len(codo), 'b'))
        a = self.genes[:len(self.genes) // 2] + p2.genes[len(p2.genes) // 2:]
        a = int(a, 2) % len(self.genes)
        a = format(a, 'b')
        b = p2.genes[:len(p2.genes) // 2] + self.genes[len(self.genes) // 2:]
        b = int(b, 2) % len(self.genes)
        b = format(b, 'b')
        return Individ('0' * (repr_size - len(a)) + a), \
               Individ('0' * (repr_size - len(b)) + b)

    def mutate(self):
        try:
            poz = randint(1, len(self.genes) - 2) + 1
            digit = str(randint(0, 1))
            self.genes = self.genes[:poz] + digit + self.genes[poz + 1:]
        except:
            pass

    def eval(self, codo):
        self.fitness = codo[int(self.genes, 2)]

    def __str__(self):
        return str(self.genes)+" "+str(self.fitness)

    def __lt__(self, other):
        return self.fitness < other.fitness


class Population:
    def __init__(self, size):
        self.population = []
        self.size = size
        self.size_turnir = size//5

    def initialize(self, codo):
        for i in range(self.size):
            genes = format(randint(0, len(codo) - 1), 'b')
            self.population.append(Individ(genes))

    def eval(self, codo):
        for individ in self.population:
            individ.eval(codo)

    def selection(self):
        turnir = []
        for i in range(self.size_turnir):
            r = randint(0, self.size - 1)
            turnir.append(self.population[r])
        turnir.sort()
        return turnir[0]

    def add(self, o):
        self.population.append(o)


def read_input():
    codo = []
    print("Hiello, please your array values. Enter 0 when you are finished")
    while True:
        try:
            raw = float(input())
            if raw == 0:
                return codo
        except ValueError:
            print("The vallue needs to be float")
        else:
            codo.append(raw)


def read_from_file(file):
    codo = []
    with open(file) as f:
        f.readline()
        f.readline()
        f.readline()
        line = f.readline()
        values = line.strip()
        for val in values.split(' '):
            codo.append(float(val))
    return codo


if __name__ == '__main__':
    # cmd = ''
    # while cmd not in  ('file', 'keys', 'exit'):
    #     cmd = input("Alege daca vrei sa citesti din fisier sau de la tastatura (key, file)")
    # while True:
    #     if cmd == 'file':
    #         try:
    #             file = input("Introduceti numele fisierului")
    #             codo = read_from_file(file)
    #         except FileNotFoundError:
    #             print("Fisierul nu exista")
    #             continue
    #     elif cmd == 'keys':
    #         codo = read_input()
    #         continue
    #     else:
    #         break

        codo = read_from_file('02_date4.txt')
        a = GeneticSolver(codo, generations=100)
        print(min(codo))
        print(a.solve())
