from lab2.punctMinim import GeneticSolver, read_from_file

codo = read_from_file('02_date1.txt')
solver = GeneticSolver(codo)
assert solver.solve() == min(codo)
print('test 1 passed')

codo = read_from_file('02_date2.txt')
solver = GeneticSolver(codo)
assert solver.solve() == min(codo)
print('test 2 passed')

codo = read_from_file('02_date3.txt')
solver = GeneticSolver(codo)
assert solver.solve() == min(codo)
print('test 3 passed hold on, take a bear, this one is gonna take a while')

codo = read_from_file('02_date4.txt')
solver = GeneticSolver(codo, population_size=500)
solution = solver.solve()
actual_solution = min(codo)
print(solution, actual_solution)
assert solution == actual_solution
print('SUCCESSS')
