function validateForm() {
    let nume = document.getElementById("nume");
    let data = document.getElementById("data");
    let varsta = document.getElementById("varsta");
    let email = document.getElementById("email");
    let punishStyle = "1px solid red";
    let godStyle = "1px solid grey";

    let ok = true;
    if (nume.value === ""){
        nume.style.border = punishStyle;
        ok = false
    }else{
        nume.style.border = godStyle;
    }

    let date = new Date(data.value);
    let year = date.getFullYear();
    if (year > 2000 || year < 1890 || date == "Invalid Date"){
        data.style.border = punishStyle;
        ok = false;
    }else{
        data.style.border = godStyle;
    }

    if (varsta.value < 18){
        varsta.style.border = punishStyle;
        ok = false;
    }else{
        varsta.style.border = godStyle;
    }

    let indexEmail = email.value.indexOf("@");
    console.log(indexEmail);
    console.log(email.value.length );
    if (indexEmail === -1 || indexEmail === email.value.length - 1){
        email.style.border = punishStyle;
        ok = false;
    }else{
        email.style.border = godStyle;
    }

    if (!ok){
        alert("Datele nu sunt valide");
        return false;
    }
}