function make_puzzle(table_id){
    let table = document.getElementById(table_id).tBodies[0];
    let rows = table.children;
    let size = rows.length * rows.length;
    let pool = [""];
    let blankrow = "";
    let blankcol = "";
    for (let i = 1; i < size; i++){
        pool.push(i)
    }
    let csize = size;
    for (let i = 0; i < rows.length; i++){
        let cols = rows[i].children;
        for(let j = 0; j < cols.length; j++){
            let poz = Math.floor(Math.random() * csize);
            csize -= 1;
            if(pool[poz] === ""){
                blankrow = i;
                blankcol = j;
            }
            cols[j].innerHTML = pool[poz];
            pool.splice(poz, 1);
        }
    }

    document.onkeyup = function (e) {
        let a = table.children[blankrow].children;
        let blank = table.children[blankrow].children[blankcol];
        if((e.which === 87 || e.which === 119) && table.children[blankrow + 1]) { //W w
            let b = table.children[blankrow + 1];
            b.insertBefore(blank, b.children[blankcol]);
            if(a[blankcol]){
                a[blankcol].parentNode.insertBefore(b.children[blankcol + 1], a[blankcol]);
            }else{
                a[blankcol - 1].parentNode.appendChild(b.children[blankcol + 1]);
            }
            blankrow += 1;
        } else if ((e.which === 97 || e.which === 65) && a[blankcol + 1] ){ // A
            a[blankcol + 1].parentNode.insertBefore(a[blankcol + 1], blank);
            blankcol += 1;
        } else if ((e.which === 83 || e.which === 115) && table.children[blankrow - 1]){ // S s
            let b = table.children[blankrow - 1];
            b.insertBefore(blank, b.children[blankcol]);
            if(a[blankcol]){
                a[blankcol].parentNode.insertBefore(b.children[blankcol + 1], a[blankcol]);
            }else{
                a[blankcol - 1].parentNode.appendChild(b.children[blankcol + 1]);
            }
            blankrow -= 1;
        } else if (e.which === 100 || e.which === 68 && a[blankcol - 1]){ // D d
            blank.parentNode.insertBefore(blank, a[blankcol - 1]);
            blankcol -= 1;
        }
    }
}


make_puzzle("table");