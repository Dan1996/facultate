let list = document.getElementById("photos");
let currentChild = list.children[0];
currentChild.style.display = "block";
let manual = true;
let next_btn = document.getElementById("next");
let prev_btn = document.getElementById("previous");
next_btn.addEventListener('click', function () {
    next();
});
prev_btn.addEventListener('click', function () {
    previous();
});

function auto_next() {
    setTimeout(function () {
        if(manual){
            next(true);
        }
        auto_next();
        manual = true;
    }, 2000);
}

function next(command=false) {
    manual = command;
    currentChild.style.display = "none";
    currentChild = currentChild.nextElementSibling;
    if (! currentChild){
        currentChild = list.children[0];
    }
    currentChild.style.display = "block";
}

function previous(command=false) {
    manual = command;
    currentChild.style.display = "none";
    currentChild = currentChild.previousElementSibling;
    if(! currentChild){
        currentChild = list.children[list.children.length - 1];
    }
    currentChild.style.display = "block";
}


auto_next();

