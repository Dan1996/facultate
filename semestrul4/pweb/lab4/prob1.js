let select1  = document.getElementById("select1");
let select2  = document.getElementById("select2");


function move_from_list(selectNumber) {
    let select_i_1;
    let select_i_2;
    if(selectNumber === 1){
        select_i_1 = select1;
        select_i_2 = select2;
    }
    else{
        select_i_1 = select2;
        select_i_2 = select1;
    }

    let el = select_i_1.options[select_i_1.selectedIndex];
    el.remove();
    select_i_2.appendChild(el);
}
