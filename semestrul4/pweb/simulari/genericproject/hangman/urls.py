from django.urls import path

from hangman import views

app_name = 'hangman'

urlpatterns = [
    path('', views.index, name='testapp'),
    path('get_val/', views.get_val, name='get_val'),
    path('get_fames/', views.get_fames, name='fames'),
    path('flush/', views.my_flush)
]
