import datetime
import json
import random

from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.core import serializers
from hangman.models import Cuvant, HallOfTime, HallOfMistakes


def index(request):
    all_cuvinte = Cuvant.objects.all()
    cuvant = None
    greseli = None
    if 'greseli' in request.session:
        greseli = request.session['greseli']
    if request.user.is_authenticated:
        pass
    else:
        if 'level' in request.session:
            level = request.session['level']
        else:
            level = 4
            request.session['level'] = level
            request.session['greseli'] = 0
            greseli = 0
            request.session['first_call'] = True
            request.session['time'] = str(datetime.datetime.now())

        if request.session.get('first_call'):
            request.session['first_call'] = False
            cuvinte_level = [cuvant for cuvant in all_cuvinte if len(cuvant.content) == level]
            if not cuvinte_level:
                return HttpResponse('Nu sunt cuvinte in baza de date pentru acest nivel')
            if len(cuvinte_level) > 1:
                cuvant = cuvinte_level[random.randrange(0, len(cuvinte_level) - 1)]
            else:
                cuvant = cuvinte_level[0]
            request.session['cuvant_id'] = cuvant.pk
            cuvant = getHiddenWord(cuvant.content)
            request.session['cuvant'] = cuvant
        else:
            cuvant = request.session['cuvant']

    return render(request, 'hangman/main.html', {'cuvant': cuvant, 'all_cuvinte': all_cuvinte, 'greseli': greseli})


def getHiddenWord(cuvant):
    inceput_final = (cuvant[0], cuvant[-1])
    rez = ''
    for char in cuvant:
        if char in inceput_final:
            rez += char
        else:
            rez += '*'
    return rez


def get_val(request):
    litera = request.GET.get('litera')
    cuvant = Cuvant.objects.get(pk=request.session['cuvant_id']).content
    greseli = True
    rez = ''
    for char1, char2 in zip(request.session['cuvant'], cuvant):
        if char1 == char2:
            rez += char2
        elif char2 == litera:
            rez += char2
            greseli = False
        else:
            rez += '*'
    request.session['cuvant'] = rez
    if greseli:
        greseli = request.session['greseli'] + 1
        request.session['greseli'] = greseli

    win = False
    restart = False
    in_time_hall = False
    in_greseli_hall = False
    if '*' not in rez:
        request.session['level'] += 1
        request.session['first_call'] = True
        restart = True

    if request.session['level'] == 7:
        win = True
        restart = False
        current_time = datetime.datetime.now()
        begin = datetime.datetime.strptime(request.session['time'], "%Y-%m-%d %H:%M:%S.%f")
        delta = (current_time - begin).total_seconds()
        request.session['delta'] = delta
        in_time_hall = verify_hall(HallOfTime, 'time_hall', delta, request.session)
        in_greseli_hall = verify_hall(HallOfMistakes, 'greseli_hall', request.session['greseli'], request.session)
        if not in_time_hall and not in_greseli_hall:
            request.session.flush()

    return HttpResponse(json.dumps({'cuvant': rez,
                                    'greseli': greseli,
                                    'restart': restart,
                                    'win': win,
                                    'in_hall': in_time_hall or in_greseli_hall,
                                    }), content_type='application/json')


def get_fames(request):
    time_hall = request.session.get('time_hall')
    greseli_hall = request.session.get('greseli_hall')
    if time_hall:
        HallOfTime.objects.create(nume=request.GET.get('name', 'Anonim'), criteriu=request.session['delta'])
    if greseli_hall:
        HallOfMistakes.objects.create(nume=request.GET.get('name', 'Anonim'), criteriu=request.session['greseli'])
    time = serializers.serialize('json', HallOfTime.objects.order_by('criteriu')[:3])
    greseli = serializers.serialize('json', HallOfMistakes.objects.order_by('criteriu')[:3])
    data = {'Hall_time': [el['fields'] for el in json.loads(time)],
            'Hall_mistakes': [el['fields'] for el in json.loads(greseli)],
            }
    request.session.flush()
    return HttpResponse(json.dumps(data), content_type='application/json')


def verify_hall(hall, hall_name, number,  session):
    in_hall = False
    time_hall = hall.objects.all().order_by('criteriu')[:3]
    if len(time_hall) < 3:
        in_hall = True
        session[hall_name] = True
    for i, hall in enumerate(time_hall):
        if number < hall.criteriu:
            in_hall = True
            session[hall_name] = True
    return in_hall


def my_flush(request):
    request.session.flush()
    return HttpResponse('flushed')
