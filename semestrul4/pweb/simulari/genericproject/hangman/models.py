from django.core.validators import MinLengthValidator, MaxLengthValidator
from django.db import models


class Cuvant(models.Model):
    content = models.CharField(max_length=6, validators=[MinLengthValidator(4), MaxLengthValidator(6)])

    def __str__(self):
        return self.content


class HallOfTime(models.Model):
    nume = models.CharField(max_length=100)
    criteriu = models.FloatField()


class HallOfMistakes(models.Model):
    nume = models.CharField(max_length=100)
    criteriu = models.IntegerField()
