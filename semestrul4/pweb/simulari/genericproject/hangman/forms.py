from django import forms

from hangman.models import Cuvant


class CuvantForm(forms.ModelForm):
    class Meta:
        model = Cuvant
        fields = ['content']
