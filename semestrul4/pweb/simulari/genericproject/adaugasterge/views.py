from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect


# Create your views here.
from adaugasterge.forms import form_factory
from genericproject import settings
from testapp.models import Eveniment

my_model = Eveniment


@login_required
def create_view(request):
    form_class = form_factory(my_model)
    if request.method == 'POST':
        form = form_class(request.POST, request.FILES)
        if form.is_valid():
            form.save(commit=False)
            form.instance.user = request.user
            form.save()
            return redirect(f'{settings.current_app}:testapp')
    else:
        form = form_class()
    return render(request, 'adaugasterge/creare.html', {'form': form})


@login_required
def delete_view(request, pk):
    err = None
    if request.method == 'POST':
        if request.POST.get('raspuns') == 'da':
            my_model.objects.get(pk=pk).delete()
            return redirect(f'{settings.current_app}:testapp')
        elif request.POST.get('raspuns') == 'nu':
            return redirect(f'{settings.current_app}:testapp')
        err = 'Trebuie sa alegi o optiune'
    return render(request, 'adaugasterge/delete.html', {'err': err})
