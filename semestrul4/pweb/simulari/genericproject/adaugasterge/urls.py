from django.urls import path

from adaugasterge import views
from testapp.views import get_evenimente

app_name = 'adaugasterge'

urlpatterns = [
    path('creare/', views.create_view, name='adauga'),
    path('delete/<int:pk>', views.delete_view, name='sterge'),
    path('get_evenimente/', get_evenimente, name='sda'),
]
