from django import forms
from django.forms import DateField

from genericproject.settings import DATE_INPUT_FORMATS


def form_factory(my_model):
    class GenericForm(forms.ModelForm):
        data = DateField(widget=forms.DateInput(attrs={'class': 'datepicker'} ),)
                         # input_formats=DATE_INPUT_FORMATS,)


        class Meta:
            model = my_model
            fields = ['data', 'ora', 'descriere']

            widgets = {
                'ora': forms.TimeInput(attrs={'class': 'timepicker'}),
            #     'password': forms.PasswordInput,
            #     'username': forms.TextInput(attrs={'autofocus': True}),
            }
    return GenericForm
