from django.contrib.auth.views import LoginView
from django.urls import path

from myauth import views

app_name = 'myauth'
urlpatterns = [
    path('login/', views.login_view, name='login'),
    path('change_password/', views.change_password, name='change_password'),
    path('upload_avatar/', views.upload_avatar, name='add_avatar'),
    path('register/', views.register, name="register"),
    path('logout', views.logout_view, name='logout'),
]
