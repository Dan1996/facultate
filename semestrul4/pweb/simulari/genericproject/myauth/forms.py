from django import forms

from myauth.models import MyUser


class UserRegistrationForm(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ['username', 'password']
        help_texts = {
            'username': None,
        }

        widgets = {'password': forms.PasswordInput,
                   'username': forms.TextInput(attrs={'autofocus': True}),
                  }


class UserImage(forms.ModelForm):
    class Meta:
        model = MyUser
        fields = ['avatar']
