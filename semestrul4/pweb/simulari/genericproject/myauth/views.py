from django.contrib.auth import login, logout, authenticate, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect, get_object_or_404

from genericproject import settings
from myauth.forms import UserRegistrationForm, UserImage
from myauth.models import MyUser


def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            # user.set_password(form.cleaned_data['password'])
            user.save()
            login(request, user)
            return redirect(f'{settings.current_app}:testapp')
    else:
        form = UserRegistrationForm()
    return render(request, 'myauth/register.html', {'form': form})


def my_authenticate(username, password):
    user = MyUser.objects.raw('Select id, username, password from myauth_myuser where username="' + username + '"and password="'+ password + '"')
    print('Select id, username, password from myauth_myuser where username="' + username +
                              '"and password="'+ password + '"')
    try:
        user = user[0]
    except Exception as ex:
        user = None
    return user


def login_view(request):
    error = None
    if request.POST:
        # form = UserRegistrationForm(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = my_authenticate(username, password)
        if user is not None:
            login(request, user)
            return redirect(f'{settings.current_app}:testapp')
        error = 'Autentificare esuata'
    else:
        pass
    return render(request, 'myauth/login.html', {'error': error})


def logout_view(request):
    logout(request)
    # INSERT APP NAME HERE
    return redirect(f'{settings.current_app}:testapp')


@login_required
def change_password(request):
    form = PasswordChangeForm(user=request.user)

    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect(f'{settings.current_app}:testapp')

    return render(request, 'myauth/password.html', {
        'form': form,
    })


def upload_avatar(request):
    form = UserImage()
    if request.method == 'POST':
        form = UserImage(request.POST, request.FILES, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect(f'{settings.current_app}:testapp')
    return render(request, 'myauth/avatar.html', {'form': form})
