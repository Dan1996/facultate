from django.contrib import admin

# Register your models here.
from myauth.models import MyUser

admin.site.register(MyUser)
