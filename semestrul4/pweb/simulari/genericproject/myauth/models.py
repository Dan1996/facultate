from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class MyUser(AbstractUser):
    def __str__(self):
        return f'{self.username} {self.email}'

    avatar = models.ImageField(null=True, upload_to='imagini/')

