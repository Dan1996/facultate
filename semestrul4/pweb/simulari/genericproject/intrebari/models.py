from django.db import models


# Create your models here.
from myauth.models import MyUser


class Intrebare(models.Model):
    data = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)


class Raspuns(models.Model):
    intrebare = models.ForeignKey(Intrebare, on_delete=models.CASCADE)
    data = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(MyUser, on_delete=models.CASCADE)
    text = models.TextField()
