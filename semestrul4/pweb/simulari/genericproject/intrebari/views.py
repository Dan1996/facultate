import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.core.serializers import serialize

# Create your views here.
from intrebari.models import Intrebare, Raspuns
from myauth.models import MyUser


def index(request):
    intrebari = Intrebare.objects.order_by('-data')
    return render(request, 'intrebari/testapp.html', {'intrebari': intrebari})


def get_raspunsuri(request):
    id = int(request.GET.get('id_intrebare')[2:])
    intrebare = get_object_or_404(Intrebare, pk=id)
    raspunsuri = Raspuns.objects.filter(intrebare=intrebare).order_by('-data')
    real_json = json.loads(serialize('json', raspunsuri))

    my_json = {'raspunsuri': [model['fields'] for model in real_json]}
    for i, el in enumerate(my_json['raspunsuri']):
        user = MyUser.objects.get(id=el['user'])
        el['nume'] = user.username
        if user.avatar:
            el['avatar'] = user.avatar.url
        else:
            el['avatar'] = '/files/imagini/anonim.png'
    my_json['authenticated'] = request.user.is_authenticated
    return HttpResponse(json.dumps(my_json), content_type='application/json')


@login_required
def sub_raspuns(request):
    raspuns = request.GET.get('raspuns')
    id_intrebare = int(request.GET.get('id_intrebare')[2:])
    if raspuns:
        Raspuns.objects.create(intrebare=Intrebare.objects.get(pk=id_intrebare), user=request.user, text=raspuns)
    return HttpResponse(json.dumps({}), content_type='application/json')