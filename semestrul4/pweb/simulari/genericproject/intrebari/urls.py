from django.urls import path

from intrebari import views

app_name = 'intrebari'

urlpatterns = [
    path('', views.index, name='testapp'),
    path('get_raspunsuri/', views.get_raspunsuri, name='get_raspunsuri'),
    path('sub_raspuns/', views.sub_raspuns, name='sub_raspuns'),
    # path('get_val/', views.get_val, name='get_val'),
    # path('get_fames/', views.get_fames, name='fames'),
    # path('flush/', views.my_flush)
]
