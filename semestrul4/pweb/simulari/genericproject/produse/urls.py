from django.urls import path

from produse import views

app_name = 'produse'

urlpatterns = [
    path('', views.index, name='testapp'),
    path('creare_produs/', views.create_view, name='creare_produs'),
    path('delete_produs/<int:pk>', views.delete_view, name='delete_produs'),
    path('filter/', views.filter, name='filter'),
    path('get_detail/', views.get_detail, name='detail'),
]
