from django import forms

from produse.models import Produse


class ProdusForm(forms.ModelForm):
    class Meta:
        model = Produse
        fields = ['nume', 'descriere', 'producator', 'pret', 'cantitate', 'poza']

        widgets = {
        #     'password': forms.PasswordInput,
        #     'username': forms.TextInput(attrs={'autofocus': True}),
        }
