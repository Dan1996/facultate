import datetime
import json

from django.contrib.auth.decorators import login_required
from django.core.serializers import serialize
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from produse.forms import ProdusForm
from produse.models import Produse, Popularitate


def index(request):
    produse = None
    if request.user.is_authenticated:
        produse = Produse.objects.all()
    return render(request, 'produse/testapp.html', {'produse': produse})


@login_required
def create_view(request):
    if request.method == 'POST':
        form = ProdusForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            Popularitate.objects.create(produs=form.instance)
            return redirect('produse:testapp')
    else:
        form = ProdusForm()
    return render(request, 'adaugasterge/creare.html', {'form': form})


@login_required
def delete_view(request, pk):
    err = None
    if request.method == 'POST':
        if request.POST.get('raspuns') == 'da':
            Produse.objects.get(pk=pk).delete()
            return redirect('produse:testapp')
        elif request.POST.get('raspuns') == 'nu':
            return redirect('produse:testapp')
        err = 'Trebuie sa alegi o optiune'
    return render(request, 'produse/delete.html', {'err': err})


def filter(request):
    filtru = request.GET['filtru']
    produse = (Produse.objects.values_list('nume', 'poza', 'pk').filter(nume__contains=filtru) |
               Produse.objects.values_list('nume', 'poza', 'pk').filter(descriere__contains=filtru)).distinct()

    produse = [{'nume': values[0], 'poza': values[1], 'pk': values[2]} for values in produse]
    return HttpResponse(json.dumps(produse), content_type='application/json')


def get_detail(request):
    id = int(request.GET.get('id')[2:])
    produs = Produse.objects.get(pk=id)
    Popularitate.objects.create(produs=produs)
    produs.save()
    popularitate = Popularitate.objects.filter(produs=produs).order_by('creare')
    data = {'produs': json.loads(serialize('json', [produs]))[0],
            'popularitate': [{'creare': model['fields']['creare']} for model in json.loads(serialize('json', popularitate))]
            }
    return HttpResponse(json.dumps(data),
                        content_type='application/json')
