from django.core.validators import MinValueValidator
from django.db import models


# Create your models here.
class Produse(models.Model):
    nume = models.CharField(max_length=100)
    descriere = models.CharField(max_length=100)
    producator = models.CharField(max_length=100)
    pret = models.IntegerField(validators=[MinValueValidator(0)])
    cantitate = models.IntegerField(validators=[MinValueValidator(0)])
    poza = models.ImageField(upload_to='imagini/')


class Popularitate(models.Model):
    produs = models.ForeignKey(Produse, on_delete=models.CASCADE)
    creare = models.TimeField(auto_now_add=True)
