import datetime

from django.http import HttpResponse, Http404
from django.shortcuts import render
from django.core import serializers
# Create your views here.
from testapp.models import Eveniment


def index(request):
    date = Eveniment.objects.values_list('data', flat=True).distinct().order_by('data')
    for data in date:
        if data >= datetime.datetime.now().date():
            break
    evenimente = Eveniment.objects.filter(data=data)
    return render(request, 'testapp/index.html', {'date': date, 'evenimente': evenimente})


def get_evenimente(request):
    data = request.GET.get('data')
    if data:
        try:
            data = datetime.datetime.strptime(data, "%Y-%m-%d").date()
        except Exception as ex:
            print(ex)
            raise Http404
        evenimente = Eveniment.objects.filter(data=data)
        evenimente = serializers.serialize('json', evenimente)
        return HttpResponse(evenimente, content_type='application-json')
    raise Http404