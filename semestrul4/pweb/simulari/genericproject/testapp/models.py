from django.db import models


# Create your models here.
class Eveniment(models.Model):
    data = models.DateField()
    ora = models.TimeField()
    descriere = models.TextField()
