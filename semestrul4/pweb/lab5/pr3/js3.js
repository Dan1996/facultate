//jquery3   
    var pair=[];
    var fixedValues=0;
    var values=[];    
    
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--; 
        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}

function validate(xIndex,yIndex)
{
    return $.isNumeric(xIndex) && $.isNumeric(yIndex) ; 
}

function initializeTable(xIndex,yIndex)
{     
    pair=[];
    fixedValues=0;
    for(let i=0;i<xIndex*yIndex;i+=2)
    {
            let val=Math.floor(Math.random()*xIndex*yIndex);
            values[i]=val;
            values[i+1]=val;
    }     
    shuffle(values);
    $(values).each(x=>console.log(values[x]));
    for(let i=0;i<xIndex;i++)
    {
       $("#table").append("<tr>");
       for(let j=0;j<yIndex;j++)
        {
            $("#table tr:last").append("<td>");
            $("#table tr:last td:last").on("click",function(){
                let row=$(this).closest("tr").index();
                let column=$(this).closest("td").index();
                if($(this).html()=="")
                {
                    xIndex<yIndex ? $(this).html(values[xIndex*column+row]) : $(this).html(values[row*yIndex+column]);
                    pair.push(row);
                    pair.push(column);
                    if($(pair).length==4)
                    {
                            let cell1=$("#table").children("tr").eq(pair[0]).children("td").eq(pair[1]);
                            let cell2=$("#table").children("tr").eq(pair[2]).children("td").eq(pair[3]);
                            
                            
                            console.log("valorile: "+$(cell1).html()+" "+$(cell2).html());
                            if($(cell1).html()!=$(cell2).html())
                            {
                                setTimeout(() => {
                                    $(cell1).html("");                                   
                                    $(cell2).html("");
                                },1500);
                            }
                            else
                            {
                                fixedValues+=2;
                                if(fixedValues==xIndex*yIndex)
                                    alert("gata boss");                                    
                            }
                        pair=[];
                    }
                }       
            });
        }        
    }         
}

$(document).ready(function()
{
    $("#button").on("click",function(){
        $("#table").html("");
        let xIndex=$("#xIndex").val(); 
        let yIndex=$("#yIndex").val();
        if(validate(xIndex,yIndex))        
        {
            initializeTable(xIndex,yIndex);        
        }
        else alert("Dimensiuni invalide!");
    });
});