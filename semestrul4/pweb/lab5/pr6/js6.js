//jquery
let freePosition=[];

function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--; 
        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }
    return array;
}


$(document).ready(
    function()
    {
        $("#button").on("click",function(){
            $("#table").html("");
            let value=$("#dimension").val();
            for(let i=0;i<value;i++)
            {
               $("#table").append("<tr>");
               for(let j=0;j<value;j++)
                {
                    $("#table tr:last").append("<td>"); 
                }
            }
            console.log($("#table").html());
            //generate values
    
            let values=[];
            for(let i=1;i<value*value;i++)
                values.push(i);
            values=shuffle(values);
             
            $(values).each(i=>console.log(values[i]));

            let freePositionIndex=Math.floor(Math.random()*value*value);
            console.log(freePositionIndex);
            freePosition[0]=Math.floor(freePositionIndex/value);
            freePosition[1]=Math.floor(freePositionIndex%value);
            console.log(freePosition[0]+"   "+freePosition[1]);

            //generate configuration
            $(values).each(i=>{
                if(i<freePositionIndex)
                    $("#table").find("td").eq(i).html(values[i]);  
                else
                    $("#table").find("td").eq(i+1).html(values[i]);       
                });     
        let freeCell=$("#table").find("td").eq(freePositionIndex);        
        setOnKeyDown(freeCell);
        });
        
    });


function setOnKeyDown(freeCell)
{
    $(document).on("keydown",function(e)
        {
            e = e || window.event;
            switch(e.keyCode)
            {
                    case 37:
                        let rightCell=$(freeCell).next("td");
                        if(rightCell.index()>-1)
                        {
                            $(freeCell).html($(rightCell).html());
                            $(rightCell).html("");
                            freeCell=rightCell;
                            verifySolution();
                        }
                    break;
                        //left
                    case 38:
                        
                        let lowerCell=$(freeCell).parent().next().children("td").eq($(freeCell).index());
                        if(lowerCell.index()>-1)
                        {
                            $(freeCell).html($(lowerCell).html());
                            $(lowerCell).html("");
                            freeCell=lowerCell;
                            verifySolution();
                        }
                        //up
                        break;
                    case 39:
                        
                        let leftCell=$(freeCell).prev("td");
                        if(leftCell.index()>-1)
                        {
                            $(freeCell).html(leftCell.html());
                            $(leftCell).html("");
                            freeCell=leftCell;
                            verifySolution();
                        }//right;
                        break;
                    case 40:
                        let upperCell=$(freeCell).parent().prev().children("td").eq($(freeCell).index());
                        if(upperCell.index()>-1)                   
                        {   $(freeCell).html($(upperCell).html());
                            $(upperCell).html("");
                            freeCell=upperCell;
                            verifySolution();
                        }//alert("down");
                        //moveDown();
                        break;
                    default:
                        break;    
            }        
        })
}

function unsetOnKeyDown()
{
    $(document).off("keydown");
}

function verifySolution()
{
    let table=$("#table");
    let dimension=$("#table").find("td").length;
    if($(table).find("td").eq(dimension-1).html()!="")
        return false;
    let contor=1; 
    for(let i=0;i<dimension-1;i++)
        if($(table).find("td").eq(i).html()!=contor++)
            return false;
        alert("game over!");
        unsetOnKeyDown();
        return true;
}
