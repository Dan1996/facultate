import threading

threads = []
conditions = []


def gogo(i):
    with condition_r:
        condition_r.notify()

    while True:
        # with conditions[i]:
        conditions[i].wait()
        if i == 0:
            print("par")
        else:
            print("impar")

        with condition_r:
            condition_r.notify()


condition_r = threading.Condition()
condition_r.acquire()

for i in range(2):
    conditions.append(threading.Condition())
    conditions[i].acquire()

    threads.append(threading.Thread(name=f'Thread-{i}', target=gogo, args=(i,)))
    threads[i].start()


while True:

    condition_r.wait()
    try:
        a = int(input("mama ta e o catea "))
        print(a)
    except ValueError:
        print("integer cowboy")
        continue
    i = a % 2

    with conditions[i]:
        conditions[i].notify()
