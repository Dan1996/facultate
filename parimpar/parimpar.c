#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <unistd.h>


int retval2;
int retval;

#define SIGNAL(x) \
  retval = pthread_cond_signal(&x); \
  if ( retval != 0 ) { \
    fprintf(stderr, "Runtime error: %s returned %d at %s:%d", #x, retval, __FILE__, __LINE__); \
    return 0; \
  };

#define WAIT(x, y) \
  retval2 = pthread_cond_wait(&x, &y); \
  if ( retval2 != 0 ) { \
    fprintf(stderr, "Runtime error: %s returned %d at %s:%d", #x, retval2, __FILE__, __LINE__); \
    return 0; \
  };



pthread_mutex_t mutex[2];
pthread_mutex_t mutex_rw;
pthread_mutex_t mutex_ready[2];
pthread_cond_t cond_ready[2];
pthread_cond_t cond[2];
pthread_cond_t cond_rw;
int iam = 0;

int n;

void* gogo( void* id )
{
  const int myid = (int)id;
  pthread_mutex_lock(&mutex_ready[myid]);
  pthread_mutex_lock(&mutex[myid]);
  SIGNAL(cond_ready[myid]);
  pthread_mutex_unlock(&mutex_ready[myid]);
    
  while (1){
      WAIT(cond[myid], mutex[myid]); 

      if (myid == 0)
        printf("%s\n", "par");
      else
        printf("%s\n", "impar");
      
      // sleep(5);
      pthread_mutex_lock(&mutex_rw);
      SIGNAL(cond_rw);
      pthread_mutex_unlock(&mutex_rw);      
  }

  return NULL; // NEver muhahha 
}

int main( int argc, char** argv )
{
  
  pthread_t threads[2];
  int i;
  pthread_mutex_init(&mutex_rw, NULL);
  pthread_cond_init(&cond_rw, NULL);
  pthread_mutex_lock(&mutex_rw);


  for( int t=0; t<2; t++ ){
    pthread_mutex_init(&mutex[t], NULL);
    pthread_cond_init(&cond[t], NULL);
    pthread_mutex_init(&mutex_ready[t], NULL);
    pthread_cond_init(&cond_ready[t], NULL);

    pthread_mutex_lock(&mutex_ready[t]);


    pthread_create( &threads[t], NULL, gogo, (void*)t );
  }

  //wait both to come 
  WAIT(cond_ready[0], mutex_ready[0]);
  WAIT(cond_ready[1], mutex_ready[1]);

  while (1){
    // sleep(2);

    printf("Introdu valoare \n");
    scanf("%d", &n);
    printf("%d\n", n);
    if (n == 10)
      return 0;

    i = n % 2;

    pthread_mutex_lock(&mutex[i]);
    SIGNAL(cond[i]);
    pthread_mutex_unlock(&mutex[i]);

    WAIT(cond_rw, mutex_rw);
  }

  
  return 0;
}