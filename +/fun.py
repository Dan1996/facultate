def rotate(matrix):
    k = 0
    while len(matrix) - k - 1 % 2:
        for i in range(k, len(matrix) - k - 1):
            matrix[k][i], matrix[i][len(matrix) - 1 - k], matrix[len(matrix) - 1 - k][len(matrix) - i - 1], matrix[len(matrix) - i - 1][k] = \
                matrix[len(matrix) - i - 1][k], matrix[k][i],  matrix[i][len(matrix) - 1 - k], matrix[len(matrix) - 1 - k][len(matrix) - i - 1 ]
        k += 1

matrix = [
    [5, 1, 9, 11],
    [2, 4, 8, 10],
    [13, 3, 6, 7],
    [15, 14, 12, 16]
]

rotate(matrix)
print('\n'.join(str(e) for e in matrix))
