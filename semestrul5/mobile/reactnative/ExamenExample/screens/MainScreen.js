import {createStackNavigator, createAppContainer} from "react-navigation";
import {Login} from "./LoginScreen";
import {DataStore} from "../dataList/DataStore";
import {DetailsScreen} from "./DetailScreen";

const AppNavigator = createStackNavigator({
    // Login: {
    //     screen: Login,
    //     navigationOptions: ({navigation}) => ({
    //         title: "Login",
    //     }),
    // },
    Home: {
        screen: DataStore,
        navigationOptions: {
            title: "Movies",
            headerLeft: null
        }
    },
    Details: {
        screen: DetailsScreen,
        navigationOptions: ({navigation}) => ({
            title: "Details",
        }),
    },
    // AddProba: {
    //     screen: AddProbaScreen,
    //     navigationOptions: ({navigation}) => ({
    //         title: "Add Proba",
    //     }),
    // }
}, {
    initialRouteName: 'Home',
});
const navigator = createAppContainer(AppNavigator);
export default navigator;