import React from "react";
import styles from "../core/styles";
import {Button, TextInput, View} from "react-native";

export class DetailsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: ""
        }
    }

    updateTask(){
        const params = this.props.navigation.state.params;
        params.callback(params.item, this.state.text, params.self);
    }

    render() {

        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TextInput
                    value = {this.state.text}
                    onChangeText={(text) => this.setState({text})}
                    placeholder={'text'}
                    style={styles.input}
                />
                <Button style={styles.input} title={"Schimba"} onPress={() => {this.updateTask();}}/>
            </View>
        );
    }
}