import React, {Component} from "react"
import styles from "../core/styles"
import {Consumer} from "../core/context"
import { Text, View, ActivityIndicator, FlatList, TouchableHighlight, TextInput, Button, Alert, AsyncStorage} from 'react-native';
import {headers, httpApiUrl} from "../core/api";
import {fetchWithTimeout} from "../core/utils";


export class DataList extends Component{
    constructor(props){
        super(props);
        this.state = {
            message: "",
        };
        this.navigate = this.props.navigate;
    }

    sendMessage(){
        const text = this.state.message;

        fetchWithTimeout(`${httpApiUrl}/message`, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(
                {text}
            )
        })
            .then(response => {
                if(response.status !== 200){
                    Alert.alert("Eroare", "Nu s-a trimis");
                }
            })
            .catch(error => Alert.alert("Eroare", "Nu s-a trimis"))
    }

    logout(){
        fetchWithTimeout(`${httpApiUrl}/logout`, {
            method: 'POST',
            headers: headers,
        })
            .then((response) => {
                AsyncStorage.removeItem("data");
                AsyncStorage.removeItem("token")
                    .then((e) => this.navigate.pop())
            })
    }



    render(){
        return (
            <Consumer>
                {({isLoading, issue, data}) => (
                    <View style={styles.content}>
                        <View style={styles.inline}>
                            <Button style={styles.input} title={"Logout"} onPress={() => {this.logout();}}/>
                            <Button
                                title={'Add'}
                                style={styles.input}
                                onPress={() => {
                                    this.sendMessage();
                                }}
                            />
                        </View>
                        <TextInput
                            onChangeText={(message) => this.setState({message})}
                            placeholder={'message'}
                            style={styles.input}
                        />

                        <ActivityIndicator animating={isLoading} style={styles.activityIndicator} size={"large"}/>
                        {issue && <Text>{issue.toString()}</Text>}

                        {data &&
                            <FlatList
                                data={data}
                                renderItem={({item}) =>
                                    (<TouchableHighlight onPress={() =>
                                        this.navigate.navigate("Details", {"item": item, "callback": this.props.callback, self: this.props.self})}>

                                        <View>
                                            <Text>PK: {item.id}</Text>
                                            <Text>Text: {item.text}</Text>
                                            <Text>Status: {item.status}</Text>
                                            <Text>Updated: {item.updated}</Text>
                                        </View>

                                    </TouchableHighlight>)}
                                keyExtractor={(item, index) => String(item) + String(index)}
                            />
                        }
                    </View>
                )}
            </Consumer>
        )
    }
}