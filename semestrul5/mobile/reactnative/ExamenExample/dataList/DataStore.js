import React, {Component} from 'react';
import {AsyncStorage, BackHandler, ToastAndroid} from "react-native";
import {headers, wsApiUrl, httpApiUrl} from "../core/api"
import {Provider} from "../core/context"
import {fetchWithTimeout} from "../core/utils";
import {DataList} from "./DataList";

export class DataStore extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            data: null,
            issue: null,
        };
        // this.props.navigation.state.params.mylist.push("BELEA");
    }

    componentDidMount() {
        // AsyncStorage.removeItem("data");
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.load();
        this.connectWs();
    }

    componentWillUnmount() {
        // log('componentWillUnmount');
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        this.ws.close();
    }

    handleBackButton() {
        ToastAndroid.show('You are already logged in', ToastAndroid.SHORT);
        return true;
    }

    updateElement(id, criteriu, value){
        let state_data = this.state.data;

        for(let i=0; i < state_data.length; i++){
            if(state_data[i].id === id){
                state_data[i][criteriu] = value;
                break;
            }
        }
        this.setState({
            data: state_data
        });

        return state_data;
    }

    connectWs = () => {
        const ws = new WebSocket(wsApiUrl);
        ws.onopen = () => {};
        ws.onmessage = e => {
            const data = JSON.parse(e.data);
            const event = data.event || "inserted";
            const task = data.task || data;

            let state_data;
            if(event === "inserted"){
                state_data = [task].concat(this.state.data || []);
                this.setState({
                    data: state_data
                })
            }
            else{
                state_data = this.updateElement(task.id, "status", "deleted");
            }
            AsyncStorage.setItem("data", JSON.stringify(state_data));
        };
        ws.onerror = e => {this.connectWs()};
        ws.onclose = e => {};
        this.ws = ws;
    };

    async load() {
        debugger;
        this.setState({isLoading: true, issue: null});
        try {
            let data = await AsyncStorage.getItem("data");

            let biggest_updated = 0;
            if (data !== null) {
                data = JSON.parse(data);
                biggest_updated = Math.max.apply(Math, data.map(function (e) {
                    return e.updated
                }));
                data.sort(function(a, b){
                    return b.updated - a.updated;
                });
            } else {
                data = [];
            }


            fetchWithTimeout(`${httpApiUrl}/task?lastUpdated=${biggest_updated}`, {
                method: 'GET',
                headers: headers,
            })
                .then(response => response.json())
                .then(json => {
                    const items = json.concat(data);
                    AsyncStorage.setItem("data", JSON.stringify(items));
                    this.setState({isLoading: false, data: items});
                })
                .catch(error => this.setState({isLoading: false, issue: error}))


        } catch (error) {
            this.setState({isLoading: false, issue: error});
            console.log("error retrieving data");
        }

    }

    send_to_server(item){
        fetchWithTimeout(`${httpApiUrl}/task/${item.id}`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(item)
        })
            .then((response) => {
                if(response.status === 400){
                    console.log("II bun");
                    setTimeout(() => this.send_to_server(item), 2000);
                }
            })
            .catch((error) => (console.log(error)))
    }

    update_callback(item, text, self){
        self.updateElement(item.id, "text", text);
        // AsyncStorage.setItem("data", JSON.stringify(data));
        self.send_to_server(item);
    }

    render() {
        return (
            <Provider value={this.state}>
                <DataList navigate={this.props.navigation} callback={this.update_callback} self={this}/>

            </Provider>
        )
    };
}