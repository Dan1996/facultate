export const apiUrl = '172.30.117.233:3000';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}/ws/`;
export const headers = {
    "Content-Type": "application/json",
    'Accept': 'application/json',
};
