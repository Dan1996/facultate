import React, {Component} from 'react';
import {AsyncStorage, BackHandler, ToastAndroid} from "react-native";
import {headers, wsApiUrl, httpApiUrl} from "../core/api"
import {Provider} from "../core/context"
import {fetchWithTimeout} from "../core/utils";
import {DataList} from "./DataList";

export class DataStore extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            data: null,
            issue: null,

        };
        // this.props.navigation.state.params.mylist.push("BELEA");
    }

    componentDidMount() {
        // AsyncStorage.removeItem("data");
        this.load();
        this.connectWs();
        this.tryToUpdate();
    }

    tryToUpdate(){
        setInterval(() => {
            fetchWithTimeout(`${httpApiUrl}/task`, {
                method: 'GET',
                headers: headers,
            })
                .then((resp) => {
                    if(resp.status === 200){
                        let state_data = [].concat(this.state.data);

                        for(let i=0; i < state_data.length; i++){
                            if(state_data[i].red){
                                delete state_data[i].red;
                                this.update_callback(state_data[i], state_data[i].text, this);
                            }
                        }
                        this.setState({
                            data: state_data
                        });
                    }
                    this.tryToUpdate();

                })
                .catch((error) => {
                    this.tryToUpdate();
                });
        }, 20000)
    }

    componentWillUnmount() {
        // log('componentWillUnmount');
        this.ws.close();
    }


    updateElement(id, criteriu, value, red=false){
        console.log(id, criteriu, value);
        let state_data = [].concat(this.state.data);

        for(let i=0; i < state_data.length; i++){
            if(state_data[i].id === id){
                state_data[i][criteriu] = value;
                if(red){
                    state_data[i].red = true;
                }
                break;
            }
        }

        this.setState({
            data: state_data
        });
        return state_data;
    }

    connectWs = () => {
        const ws = new WebSocket(wsApiUrl);
        ws.onopen = () => {};
        ws.onmessage = e => {
            const task = JSON.parse(e.data);
            let state_data = [].concat(this.state.data);
            for(let i=0; i < state_data.length; i++){
                if(state_data[i].id === task.id){
                    state_data[i] = task;
                    break;
                }
            }

            this.setState({
                data: state_data
            });


            AsyncStorage.setItem("data", JSON.stringify(state_data));
        };
        ws.onerror = e => {this.connectWs()};
        ws.onclose = e => {};
        this.ws = ws;
    };

    async load() {
        this.setState({isLoading: true, issue: null});
        try {
            let data = await AsyncStorage.getItem("data");

            if (data !== null) {
                data = JSON.parse(data);
            } else {
                data = [];
            }


            fetchWithTimeout(`${httpApiUrl}/task`, {
                method: 'GET',
                headers: headers,
            })
                .then(response => {
                    if(response.status === 200){
                        return response.json();
                    }
                    else{
                        throw Error("Eroare la incarcarea datelor")
                    }

                })
                .then(json => {
                    AsyncStorage.setItem("data", JSON.stringify(json));
                    console.log("am luat datele");
                    this.setState({isLoading: false, data: json});
                })
                .catch((error) => {
                    this.setState({isLoading: false, issue: error, data: data});
                })

        } catch (error) {
            this.setState({isLoading: false, issue: error});
            console.log("error retrieving data");
        }

    }

    async send_to_server(item){
        return await fetchWithTimeout(`${httpApiUrl}/task/${item.id}`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(item)
        })
            .then((response) => {
                console.log(response.status);
                if(response.status === 409){
                    return false;
                }else if (response.status === 200){
                    return true;
                }else{
                    throw Error("Nu se pface update");
                }
            })

    }

    async update_callback(item, text, self){

        item.text = text;
        return self.send_to_server(item);
    }

    render() {
        return (
            <Provider value={this.state}>
                <DataList navigate={this.props.navigation} callback={this.update_callback} self={this}/>

            </Provider>
        )
    };
}