export const apiUrl = '192.168.0.105:3000';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}/ws/`;
export const headers = {
    "Content-Type": "application/json",
    'Accept': 'application/json',
};
