import React from "react";
import styles from "../core/styles";
import {AsyncStorage, Button, TextInput, View} from "react-native";
import {fetchWithTimeout} from "../core/utils";
import {headers, httpApiUrl} from "../core/api";

export class DetailsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            item: this.props.navigation.state.params.item
        }
    }

    updateTask(){
        const params = this.props.navigation.state.params;
        console.log(this.state.text);
        params.callback(this.state.item, this.state.text, params.self)
            .then((update) => {
                if(update){
                    console.log("Updatez lista");
                    params.self.updateElement(this.state.item.id, "text", this.state.text);
                    params.self.load();
                } else{
                    this.retriveAndShowConflict();
                }
            })
            .catch((error) => {
                params.self.updateElement(this.state.item.id, "text", this.state.text, true);
            });

    }

    retriveAndShowConflict(){
        fetchWithTimeout(`${httpApiUrl}/task`, {
            method: 'GET',
            headers: headers,
        })
            .then(response => {
                if(response.status === 200){
                    return response.json();
                }
                else{
                    throw Error("Eroare la incarcarea datelor")
                }

            })
            .then(json => {
                console.log("face update");
                for (let i=0; i<json.length; i++){
                    if(json[i].id === this.state.item.id){
                        const text = json[i].text;
                        this.setState({text: text, item: json[i]});
                        break;
                    }
                }
                this.props.navigation.state.params.self.load();
            })
            .catch((error) => {
                console.log("eroare pe details");
            })
    }

    render() {

        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TextInput
                    value = {this.state.text}
                    onChangeText={(text) => this.setState({text})}
                    placeholder={'text'}
                    style={styles.input}
                />
                <Button style={styles.input} title={"Save"} onPress={() => {this.updateTask();}}/>
            </View>
        );
    }
}