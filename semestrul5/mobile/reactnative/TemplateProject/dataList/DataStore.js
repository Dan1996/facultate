import React, {Component} from 'react';
import {AsyncStorage, BackHandler, ToastAndroid} from "react-native";
import {headers, wsApiUrl, httpApiUrl} from "../core/api"
import {Provider} from "../core/context"
import {fetchWithTimeout} from "../core/utils";
import {DataList} from "./DataList";

export class DataStore extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            data: null,
            issue: null,
        };
        // this.props.navigation.state.params.mylist.push("BELEA");
    }

    componentDidMount() {
        // AsyncStorage.removeItem("data");
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.load();
        this.connectWs();
    }

    componentWillUnmount() {
        // log('componentWillUnmount');
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        this.ws.close();
    }

    handleBackButton() {
        ToastAndroid.show('You are already logged in', ToastAndroid.SHORT);
        return true;
    }

    connectWs = () => {
        const ws = new WebSocket(wsApiUrl);
        ws.onopen = () => {};
        ws.onmessage = e => this.setState({
            data: [JSON.parse(e.data)].concat(this.state.data || [])
        });
        ws.onerror = e => {this.connectWs()};
        ws.onclose = e => {};
        this.ws = ws;
    };

    async load() {
        debugger;
        this.setState({isLoading: true, issue: null});
        try {
            let data = await AsyncStorage.getItem("data");
            let biggest_created = 0;
            if (data !== null) {
                data = JSON.parse(data);
                biggest_created = Math.max.apply(Math, data.map(function (e) {
                    return e.created
                }))
            } else {
                data = [];
            }

            fetchWithTimeout(`${httpApiUrl}/message/?created=${biggest_created}`, {
                method: 'GET',
                headers: headers,
            })
                .then(response => response.json())
                .then(json => {
                    const items = json.concat(data);
                    items.sort(function(a, b){
                        return b.created - a.created;
                    });
                    AsyncStorage.setItem("data", JSON.stringify(items));
                    this.setState({isLoading: false, data: items});
                })
                .catch(error => this.setState({isLoading: false, issue: error}))


        } catch (error) {
            this.setState({isLoading: false, issue: error});
            console.log("error retrieving data");
        }


    }

    render() {
        return (
            <Provider value={this.state}>
                <DataList navigate={this.props.navigation}/>
            </Provider>
        )
    };
}