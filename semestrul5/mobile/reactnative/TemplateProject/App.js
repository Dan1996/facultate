import React from 'react';
import {Component} from "react";
import MainScreen from "./screens/MainScreen";

export default class App extends Component{
    constructor(props) {
        super(props);
    }
    render(){
        return (
            <MainScreen/>
        )
    }
}