import {createStackNavigator, createAppContainer} from "react-navigation";
import {Login} from "./LoginScreen";
import {DataStore} from "../dataList/DataStore";

const AppNavigator = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            title: "Login",
        }),
    },
    Home: {
        screen: DataStore,
        navigationOptions: {
            title: "Movies",
            headerLeft: null
        }
    },
    // Details: {
    //     screen: DetailsScreen,
    //     navigationOptions: ({navigation}) => ({
    //         title: "Details",
    //     }),
    // },
    // AddProba: {
    //     screen: AddProbaScreen,
    //     navigationOptions: ({navigation}) => ({
    //         title: "Add Proba",
    //     }),
    // }
}, {
    initialRouteName: 'Login',
});
const navigator = createAppContainer(AppNavigator);
export default navigator;