import React, {Component} from 'react';
import {Button, Text, TextInput, View, AsyncStorage} from 'react-native';
import {httpApiUrl, headers} from "../core/api";
import styles from "../core/styles"
import {fetchWithTimeout} from "../core/utils";

export class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            error: '',
        };

        AsyncStorage.getItem("token")
            .then((item) => {
                if(item !== null){
                    headers['token'] = item;
                    this.navigate('Home', {username: item})
                }
            });
        this.navigate = this.props.navigation.navigate;
    }


    onLogin() {
        const {username, password} = this.state;

        delete headers.token;
        console.log(this.state.mylist);

        fetchWithTimeout(httpApiUrl + '/login', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                "password": password,
                "username": username
            }),
        }).then((response) => {
            if (response.status === 201) {
                return response.json();
            } else {
                this.setState({error: 'Invalid credentials'});
            }
        })
            .then((jsonresp) => {
                headers['token'] = jsonresp["token"];
                AsyncStorage.setItem("token", jsonresp["token"]);
                console.log("LOGIN CU SUCCES");

                this.navigate('Home', {username: username, mylist: this.state.mylist} );

            })
            .catch((error) => {
                this.setState({error: 'Exista deja un user cu acest nume'});
            });

    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username) => this.setState({username})}
                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={(password) => this.setState({password})}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <Text>{this.state.error}</Text>

                <Button
                    title={'Login'}
                    style={styles.input}
                    onPress={() => {
                        this.onLogin();
                    }}
                />
            </View>
        );
    }
}