import React, {Component} from 'react';
import {Alert, Button, StyleSheet, Text, TextInput, View} from 'react-native';
import {createAppContainer, createStackNavigator} from "react-navigation";
import {headers, httpApiUrl} from "../core/api";
import {DetailsScreen, Index} from "./index";
import AddProbaScreen from "./AddProba";

class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
            error: '',
        };
        this.navigate = this.props.navigation.navigate;

    }

    fetchWithTimeout(url, options = undefined, timeout = 2000) {
        return Promise.race([
            fetch(url, options),
            new Promise((_, reject) =>
                setTimeout(() => reject(new Error('timeout')), timeout)
            )
        ]);
    }

    onLogin() {
        const {username, password} = this.state;
        console.log(username);
        console.log(password);
        console.log(headers);


        this.fetchWithTimeout(httpApiUrl + '/login', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                "password": password,
                "username": username
            }),
        }).then((response) => {
            if(response.status === 200){
                console.log("bine");
                return response.json();
            } else{
                this.setState({error: 'Invalid credentials'});

            }
        })
            .then((jsonresp) => {
                headers['cookie'] = "sessionid="+jsonresp["token"];
                this.navigate('Home', {username});
            })
            .catch((error) => {
                try{
                    this.setState({error: 'Nu este conexiune la server'});
                } catch (err) {
                    console.error(error);
                }
            });


    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username) => this.setState({username})}
                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={(password) => this.setState({password})}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <Text>{this.state.error}</Text>

                <Button
                    title={'Login'}
                    style={styles.input}
                    onPress={() => {
                        this.onLogin();
                    }}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },


});
const AppNavigator = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: ({navigation}) => ({
            title: "Login",
        }),
    },
    Home: {
        screen: Index,
        navigationOptions: ({navigation}) => ({
            title: "Movies",
        }),
    },
    Details: {
        screen: DetailsScreen,
        navigationOptions: ({navigation}) => ({
            title: "Details",
        }),
    },
    AddProba: {
        screen: AddProbaScreen,
        navigationOptions: ({navigation}) => ({
            title: "Add Proba",
        }),
    }
}, {
    initialRouteName: 'Login',
});
export default createAppContainer(AppNavigator);



