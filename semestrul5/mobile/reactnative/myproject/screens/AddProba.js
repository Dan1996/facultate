import {headers, httpApiUrl} from "../core/api";
import React from 'react';
import {AsyncStorage, Button, StyleSheet, TextInput, View} from 'react-native';
import {fetchWithTimeout} from "../core/utils";

export default class AddProbaScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pk: "",
            newData: "",
            nume: '',
            swimstyle: '',
            distanta: '',
        };
        this.navigate = this.props.navigation.navigate;
    }



    _storeData = async (proba) => {
        let values;
        try {
            values = await AsyncStorage.getItem('new_probe');
        } catch (error) {
            console.log("error retrieving data");
        }
        if(!values){
            values = [proba]
        } else{
            values = JSON.parse(values);
        }
        proba["newData"] = true;
        values.push(proba);

        try {
            await AsyncStorage.setItem('new_probe', JSON.stringify(values));
        } catch (error) {
            console.log("error saving data")
        }
    };

    addProba() {
        console.log("adding proba");

        const proba = {
            pk: this.state.pk,
            newData: this.state.newData,
            nume: this.state.nume,
            swimstyle: this.state.swimstyle,
            distanta: this.state.distanta
        };

        fetchWithTimeout(httpApiUrl + '/proba/creare_probe', {
            method: 'POST',
            headers: headers,
            body: JSON.stringify([proba]),
        }).then((response) => this.props.navigation.pop())
            .catch((error) => {
                try {
                    console.log(proba);
                    this._storeData(proba);
                    console.log("AM FACUT STORE");
                    this.props.navigation.pop();
                }catch (e) {
                    console.log(e);
                }

            });

    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    onChangeText={(nume) => this.setState({nume})}
                    placeholder={'nume'}
                    style={styles.input}
                />
                <TextInput

                    onChangeText={(swimstyle) => this.setState({swimstyle})}
                    placeholder={'swimstyle'}
                    style={styles.input}
                />
                <TextInput
                    onChangeText={(distanta) => this.setState({distanta})}
                    placeholder={'distanta'}
                    style={styles.input}
                />
                <Button
                    title={'Add'}
                    style={styles.input}
                    onPress={() => {
                        this.addProba();
                    }}
                />
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
    },
    input: {
        width: 200,
        height: 44,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },


});