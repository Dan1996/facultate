import React from "react";
import {ActivityIndicator, AsyncStorage, FlatList, Text, TouchableHighlight, View, Button} from "react-native";
import {headers, httpApiUrl, wsApiUrl} from "../core/api";
import {fetchWithTimeout} from "../core/utils";

export class Index extends React.Component {

    constructor(props) {
        super(props);
        this.state = {isLoading: true};
        this.navigate = this.props.navigation.navigate;
        this.ws = new WebSocket(wsApiUrl);
        this.ws.onmessage = (json) => {
            this.loadData();
        };
        this.callback = () => {
            console.log("EROARE close");
            setTimeout(() => {
                this.ws = new WebSocket(wsApiUrl);
                this.ws.onopen = () => {
                    this._retrieFailed();
                };
                this.ws.onmessage = (json) => {
                    this.loadData();
                };
                this.ws.onclose = this.callback;
            }, 7000);
        };

        this.ws.onclose = () => {this.callback()};

        this.ws.onopen = () => {
            this._retrieFailed();
        };
    };

    _storeData = async (probe) => {
        try {
            AsyncStorage.setItem('probe', JSON.stringify(probe));
        } catch (error) {
            console.log("error saving data")
        }
    };

    _retrieveData = async () => {
        try {
            let value = await AsyncStorage.getItem('probe');
            let new_value = await AsyncStorage.getItem("new_probe");
            if (new_value === null) {
                value = value.concat(new_value);
            }
            if (value !== null) {
                this.setState({
                    isLoading: false,
                    dataSource: JSON.parse(value.concat(new_value)),
                });
            }
        } catch (error) {
            console.log("error retrieving data");
        }
    };


    _retrieFailed = async () => {
        try {
            console.log("AM DESCHIS");
            const value = await AsyncStorage.getItem('new_probe');
            if (value !== null) {
                fetchWithTimeout(httpApiUrl + '/proba/creare_probe', {
                    method: 'POST',
                    headers: headers,
                    body: value,
                })
                    .then((response) => {
                        if (response.status === 200) {
                            console.log("REMOVE THEM ALL");
                            AsyncStorage.removeItem("new_probe");
                        }
                    })
                    .catch((error) => {
                        try {
                            console.log(error);
                        } catch (e) {
                            console.log(e);
                        }
                    })
            }
        } catch (e) {
            console.log("error");
        }
    };

    loadData() {
        return fetchWithTimeout(httpApiUrl + "/probe",
            {
                method: 'GET',
                headers: headers,
            }
        )
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson !== null) {
                    this._storeData(responseJson["probe"]);
                    this.setState({
                        isLoading: false,
                        dataSource: responseJson["probe"],
                    }, function () {

                    });

                } else {
                    this._retrieveData();
                }

            })
            .catch((error) => {
                try {
                    this._retrieveData();
                } catch (e) {
                    console.log(e);
                }

            });
    }

    componentDidMount() {
        return this.loadData();
    }


    render() {

        if (this.state.isLoading) {
            return (
                <View style={{flex: 1, padding: 20}}>
                    <ActivityIndicator/>
                </View>
            )
        }

        return (
            <View style={{flex: 1, paddingTop: 20}}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={({item}) =>
                        (<TouchableHighlight onPress={() => this.navigate("Details", {item})}>

                            <View>
                                <Text>PK: {item.pk}</Text>
                                <Text>Nume: {item.nume}</Text>
                            </View>

                        </TouchableHighlight>)}
                    keyExtractor={(item, index) => index.toString()}
                />
                <Button
                    onPress={() => this.navigate("AddProba")}
                    title="Add Proba"
                    color="#841584"
                    accessibilityLabel="Learn more about this purple button"
                />

            </View>
        );
    }
}

export class DetailsScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        this.item = this.props.navigation.state.params.item;
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Text>{this.item.swimstyle}, {this.item.distanta}</Text>
            </View>
        );
    }
}