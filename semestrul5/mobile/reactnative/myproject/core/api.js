export const apiUrl = '172.25.14.123:8000';
export const httpApiUrl = `http://${apiUrl}`;
export const wsApiUrl = `ws://${apiUrl}/ws/`;
export const headers = {
    "Content-Type": "application/json",
    'Accept': 'application/json',
};
