const Koa = require('koa');
const app = new Koa();
const server = require('http').createServer(app.callback());
const WebSocket = require('ws');
const wss = new WebSocket.Server({server});
const Router = require('koa-router');
const cors = require('koa-cors');
const bodyparser = require('koa-bodyparser');

app.use(bodyparser());
app.use(cors());

app.use(async function (ctx, next) {
  const start = new Date();
  await next();
  const ms = new Date() - start;
  console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);
});

const tasks = [];
for (let i = 0; i < 7; i++) {
  tasks.push({id: i, text: `Task ${i}`, status: 'active', updated: Date.now() + i, version: 1})
}

const router = new Router();
router.get('/task', ctx => {
  const clientLastUpdated = parseInt(ctx.request.query.lastUpdated) || 0;
  if (!clientLastUpdated) {
    console.log('lastUpdated not set');
  }
  ctx.response.body = tasks
    .slice()
    .filter(t => t.updated > clientLastUpdated)
    .sort((n1, n2) => (n1.updated - n2.updated));
  ctx.response.status = 200;
});

const broadcast = (data) =>
  wss.clients.forEach((client) => {
    if (client.readyState === WebSocket.OPEN) {
      client.send(JSON.stringify(data));
    }
  });

router.put('/task/:id', ctx => {
  const task = ctx.request.body;
  console.log(task);
  console.log(ctx.params);
  const id = parseInt(ctx.params.id);
  console.log(id);
  const index = tasks.findIndex(task => task.id === id);
  if (id !== task.id || index === -1) {
    console.log("AICI 1");
    ctx.response.body = {text: 'Task not found'};
    ctx.response.status = 400;
  } else if (tasks[index].status === 'deleted') {
      console.log("AICI 2");
    ctx.response.body = {text: 'Cannot modify deleted tasks'};
    ctx.response.status = 412;
  } else if (task.version < tasks[index].version) {
      console.log("AICI 3");
    ctx.response.body = {text: 'Version conflict'};
    ctx.response.status = 409;
  } else {
      console.log("BINEE");
    task.version++;
    tasks[index] = task;
    ctx.response.body = task;
    ctx.response.status = 200;
    broadcast(task);
  }
});

let deletedIndex = -1;
setInterval(() => {
  const insertedTask = {id: tasks.length, text: `Task ${tasks.length}`, status: 'active', updated: Date.now(), version: 1};
  tasks.push(insertedTask);
  console.log(`INSERTED ${insertedTask.text}`);
  broadcast({event: 'inserted', task: insertedTask});
  const deletedTask = tasks[++deletedIndex];
  deletedTask.status = 'deleted';
  deletedTask.updated = Date.now();
  console.log(`DELETED ${deletedTask.text}`);
  broadcast({event: 'deleted', task: deletedTask});
}, 5000);

app.use(router.routes());
app.use(router.allowedMethods());

server.listen(3000);
