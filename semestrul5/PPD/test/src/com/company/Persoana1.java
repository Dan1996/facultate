package com.company;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class Persoana1 implements Runnable{
    ListaBile listaBile;
    BufferedWriter bw;
    Random rand = new Random();

    public Persoana1(ListaBile listaBile, BufferedWriter bw) {
        this.listaBile = listaBile;
        this.bw = bw;
    }

    static void write(String message, BufferedWriter bw) {
        try {
            bw.write(message + " " + System.nanoTime() + "\n"); // write to file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (listaBile.size() > 0) {
            try {
                System.out.println("P1");
                listaBile.lock();
                if(listaBile.size() == 1){
                    write("P1 am extras o bila, era verde asa ca n-am mai pus-o la loc", bw);
                    break;
                }
                int index_bila = rand.nextInt(listaBile.size() - 1);
                String bila = listaBile.getElem(index_bila);
                if (bila.equals("v")) {
                    listaBile.remove(index_bila);
                    write("P1 am extras o bila, era verde asa ca n-am mai pus-o la loc", bw);
                    System.out.println("P1 am extras o bila, era verde asa ca n-am mai pus-o la loc");
                    System.out.println(listaBile.size());
                } else{
                    write("P1 am extras o bila, era rosie asa ca am mai pus-o la loc", bw);
                    System.out.println("P1 am extras o bila, era rosie asa ca am mai pus-o la loc");
                    System.out.println(listaBile.size());
                }
                try {
                    Thread.sleep(4);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                listaBile.unlock();
            } finally {
                listaBile.unlock();
            }
        }
    }
}
