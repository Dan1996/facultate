package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ListaBile {
    List<String> lista = new ArrayList<>();
    final ReentrantLock lock = new ReentrantLock();

    public ListaBile() {
    }

    public void lock() {
        if (!lock.isHeldByCurrentThread()) lock.lock();
    }

    public void unlock() {
        if (lock.isHeldByCurrentThread()) lock.unlock();
    }


    public void insert(String el){
        lista.add(el);
    }

    public void remove(int i){
        lista.remove(i);
    }

    public synchronized int size(){
        return lista.size();
    }

    public String getElem(int i){
        return lista.get(i);
    }
}
