package com.company;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Random;

public class Persoana2 implements Runnable{
    ListaBile listaBile;
    BufferedWriter bw;

    public Persoana2(ListaBile listaBile, BufferedWriter bw) {
        this.listaBile = listaBile;
        this.bw = bw;
    }

    Random rand = new Random();

    static void write(String message, BufferedWriter bw) {
        try {
            bw.write(message + " " + System.nanoTime() + "\n"); // write to file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (listaBile.size() > 0) {
            try {
                listaBile.lock();
                if(listaBile.size() < 2){
                    break;
                }
                int index_bila1 = rand.nextInt(listaBile.size() - 1);
                int index_bila2 = rand.nextInt(listaBile.size() - 1);


                if(listaBile.size() == 2){
                    index_bila1 = 0;
                    index_bila2 = 1;
                }else {
                    while (index_bila1 == index_bila2) {
                        index_bila2 = rand.nextInt(listaBile.size() - 1);
                    }
                }

                String bila1 = listaBile.getElem(index_bila1);
                String bila2 = listaBile.getElem(index_bila2);

                if (!bila1.equals(bila2)) {
                    if (bila1.equals("v")) {
                        listaBile.remove(index_bila1);
                    } else {
                        listaBile.remove(index_bila2);
                    }
                    write("P2 am extras doua bile, erau diferite asa ca am pus la loc bila rosie", bw);
                    System.out.println("P2 am extras doua bile, erau la fel asa ca le-am scos si am bagat un verde noua");
                    System.out.println(listaBile.size());
                } else {
                    listaBile.remove(index_bila1);
                    listaBile.remove(index_bila2);
                    listaBile.insert("v");
                    System.out.println("P2 am extras doua bile, erau la fel asa ca le-am scos si am bagat un verde noua");
                    write("P2 am extras doua bile, erau la fel asa ca le-am scos si am bagat un verde noua", bw);
                    System.out.println(listaBile.size());
                }
                try {
                    Thread.sleep(4);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                listaBile.unlock();
            } finally {
                listaBile.unlock();
            }
        }
    }
}