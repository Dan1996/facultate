package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {

    static BufferedWriter getWriter(String filename) throws IOException {
        File file = new File(filename);
        FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
        BufferedWriter bw = new BufferedWriter(fw);
        return bw;
    }

    static void readList(ListaBile listaBile){
        Scanner sc = new Scanner(System.in);
        String in = "";

        while (!in.equals("s")){
            char c = sc.next().charAt(0);
            in = Character.toString(c);
            listaBile.insert(in);
            System.out.println(in);
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedWriter bw = getWriter("List.log");
        ListaBile listaBile = new ListaBile();

        readList(listaBile);


        Thread[] threads = new Thread[2];
        threads[0] = new Thread(new Persoana1(listaBile, bw));
        threads[1] = new Thread(new Persoana2(listaBile, bw));

        threads[0].start();
        threads[1].start();

        try {
            threads[0].join();
            threads[1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        bw.close();
    }
}
