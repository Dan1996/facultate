// ConsoleApplication3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "mpi.h"
#include <fstream>
#include <iostream>
using namespace std;

int initializeVector(long long* v, string filename, bool coefs) {
	int n;
	int a = 3;
	
	ifstream f(filename, std::ios_base::in);
	
	f >> n;
	int e;
	
	if (coefs) {
		f >> a;
	}
	for (int i = 0; i < n; i++) {
		f >> e;
		v[i] = e;
	}

	return a;
}

long long calculateValue(long long* coefs, long long* data, int n) {
	long long rez = 0;
	for (int i = 0; i < n; i+=2) {
		rez += coefs[data[i+1]] * data[i];
	}
	return rez;
}

int main()
{

	int size, rank, n=10001;
	int k = 0;
	long long rez = 0;

	MPI_Init(NULL, NULL);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	long long* coefs = new long long[n];
	long long* nums = new long long[n];
	int data_size = n / size * 2;
	long long* segment = new long long[data_size];
	int liber;

	int i, localdata;	
	if (rank == 0) {
		liber = initializeVector(coefs, "coefs.txt", true);
		initializeVector(nums, "input.txt", false);
		//for (int i = 0; i < n; i++) {
		//	cout << coefs[i] << "\n";
		//	cout << nums[i] << "\n";
		//}
	}
	MPI_Bcast(coefs, n, MPI_LONG_LONG, 0, MPI_COMM_WORLD);
	
	if (rank == 0) {
		for (int i = 1; i < size; i++) {
			for (int j = 0; j < data_size; j+=2) {
				segment[j] = nums[k];
				segment[j + 1] = k;
				k++;
			}
			MPI_Send(segment, data_size, MPI_LONG_LONG, i, 10, MPI_COMM_WORLD);
		}
	}
	else {
		MPI_Status status;
		MPI_Recv(segment, data_size, MPI_LONG_LONG, 0, 10, MPI_COMM_WORLD, &status);
		long long rez = calculateValue(coefs, segment, data_size);
		MPI_Send(&rez, 1, MPI_LONG_LONG, 0, 10, MPI_COMM_WORLD);
	}
		
	
	if (rank == 0) {
		long long arrive;
		rez += liber;
		for (int i = k; i < n; i++) {
			rez += coefs[i] * nums[i];
		}

		for (int i = 1; i < size; i++) {
			MPI_Status status;
			MPI_Recv(&arrive, 1, MPI_LONG_LONG, i, 10, MPI_COMM_WORLD, &status);
			rez += arrive;
		}
		cout << rez;

		//rez = liber;
		//for (int i = 0; i < n; i++) {
		//	rez += nums[i] * coefs[i];
		//}
		//cout << rez;
	}

	//MPI_Scatter(coefs, 1, MPI_INT, &localdata, , MPI_INT, 0, MPI_COMM_WORLD);
	//
	//printf("2. Processor %d has data %d\n", rank, localdata);
	//
	//
	//MPI_Gather(&localdata, 1, MPI_INT, globaldata, 1, MPI_INT, 0, MPI_COMM_WORLD);
	//
	//if (rank == 0) {
	//	printf("4. Processor %d has data: ", rank);
	//	for (i = 0; i<size; i++)
	//		printf("%d ", globaldata[i]);
	//	printf("\n");
	//}


	MPI_Finalize();
	
    return 0;
}
