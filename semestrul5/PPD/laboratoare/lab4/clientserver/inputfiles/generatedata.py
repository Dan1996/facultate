import random
import string

def get_random_string(l):
    return ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(l))

my = set()
with open('produse.txt', 'w') as f:
    for i in range(1000):
        key = get_random_string(10)
        if key not in my:
            f.write(' '.join((get_random_string(4), key, str(random.uniform(0.1, 100.0)), get_random_string(4), str(random.randint(10,100)))) + '\n')
        my.add(key)