package com.company.server;

import com.company.Models.Produs;
import com.company.Repo;

import java.io.*;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;

public class MyHandler implements Runnable {
    Socket connection;
    Repo repo;


    public MyHandler(Socket connection, Repo repo) {
        this.connection = connection;
        this.repo = repo;
    }

    @Override
    public void run() {
        try {
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            PrintWriter out = new PrintWriter(connection.getOutputStream(), true);

            while (true) {
                String msg = in.readLine();
                if (msg.equals("get_list")){
                    try {
                        repo.lock();
                        Iterator it = repo.getAll().entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            Produs prod = (Produs)pair.getValue();
                            if(prod.getCantitate() > 0){
                                out.println(pair.getValue().toString());
                            }
                        }
                        repo.unlock();
                    } finally {
                        repo.unlock();
                        out.println("done");
                    }
                } else if(!msg.equals("exit")){
                    String name = msg, id_prod = in.readLine();
                    int cantitate = Integer.parseInt(in.readLine());


                    String resp = repo.remove(id_prod, cantitate, name);
                    out.println(resp);
                } else{
                    connection.close();
                    break;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
