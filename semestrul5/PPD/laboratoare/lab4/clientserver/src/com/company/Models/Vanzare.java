package com.company.Models;

import java.util.Date;
import java.util.Objects;

public class Vanzare {
    Date data;
    Produs produs;
    int cantitate;

    public Vanzare(Date data, Produs produs, int cantitate) {
        this.data = data;
        this.produs = produs;
        this.cantitate = cantitate;
    }

    public Date getData() {
        return data;
    }

    public Produs getProdus() {
        return produs;
    }

    public int getCantitate() {
        return cantitate;
    }

    @Override
    public String toString() {
        return "Vanzare{" +
                "data=" + data +
                ", produs=" + produs +
                ", cantitate=" + cantitate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vanzare vanzare = (Vanzare) o;
        return cantitate == vanzare.cantitate &&
                Objects.equals(data, vanzare.data) &&
                Objects.equals(produs, vanzare.produs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, produs, cantitate);
    }
}
