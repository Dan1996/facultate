package com.company.Models;

public class Factura {
    String nume;
    Vanzare vanzare;
    float suma;

    public String getNume() {
        return nume;
    }

    public Vanzare getVanzare() {
        return vanzare;
    }

    @Override
    public String toString() {
        return "Factura{" +
                "nume='" + nume + '\'' +
                ", vanzare=" + vanzare +
                ", suma=" + suma +
                '}';
    }

    public float getSuma() {
        return suma;
    }

    public Factura(String nume, Vanzare vanzare, float suma) {
        this.nume = nume;
        this.vanzare = vanzare;
        this.suma = suma;
    }
}
