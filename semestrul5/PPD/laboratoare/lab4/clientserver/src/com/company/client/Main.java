package com.company.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Main {

    private static BufferedReader in;
    private static PrintWriter out;
    private static String get_list = "get_list";

    private static void printMessages() throws IOException {
        String msg = in.readLine();
        while (!msg.equals("done")){
            System.out.println(msg);
            msg = in.readLine();
        }
    }


    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 6501);
        Scanner scanner  = new Scanner(System.in);
        String name;

        System.out.println("Introduceti-va numele: ");
        name = scanner.nextLine();
        in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        String cmd = "Go";

        while (!cmd.equals("exit")){
            System.out.println("1. Afiseaza lista");
            System.out.println("2. Cumpara produs");
            System.out.println("3. Exit");

            cmd = scanner.nextLine();
            if(cmd.equals("1")){
                out.println(get_list);
                printMessages();
            } else if (cmd.equals("2")){
                out.println(name);
                System.out.println("Introdu id-ul produsului");
                // id prod
                out.println(scanner.nextLine());
                System.out.println("Introdu cantitatea");
                // cantitate
                out.println(scanner.nextLine());
                String resp = in.readLine();
                if(resp.equals("ghinion")){
                    System.out.println("Naspa, produsu s-a cumparat deja sau nu exista");
                } else {
                    System.out.println("Produs cumparat cu succes");
                }
            } else{
                cmd = "exit";
                out.println(cmd);
            }
        }
        socket.close();
    }
}

