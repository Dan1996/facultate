package com.company.Models;

public class Produs {
    String nume;
    String cod_produs;
    float pret_unitar;
    String unit_masura;
    int cantitate;

    public Produs(String nume, String cod_produs, float pret_unitar, String unit_masura, int cantitate) {
        this.nume = nume;
        this.cod_produs = cod_produs;
        this.pret_unitar = pret_unitar;
        this.unit_masura = unit_masura;
        this.cantitate = cantitate;
    }

    @Override
    public String toString() {
        return nume + " " + cod_produs + " " + pret_unitar + " " + unit_masura + " " + cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }

    public int getCantitate() {
        return cantitate;
    }

    public String getNume() {
        return nume;
    }

    public String getCod_produs() {
        return cod_produs;
    }

    public float getPret_unitar() {
        return pret_unitar;
    }

    public String getUnit_masura() {
        return unit_masura;
    }
}
