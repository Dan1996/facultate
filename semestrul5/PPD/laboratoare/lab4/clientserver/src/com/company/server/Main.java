package com.company.server;

import com.company.Models.Produs;
import com.company.Repo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

    final static int cores = Runtime.getRuntime().availableProcessors();
    final static ExecutorService executor = Executors.newFixedThreadPool(cores);
    final static Repo repo = new Repo();


    public static void initRepo(){
        String csvFile = "inputfiles" +  File.separator + "produse.txt";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = " ";

        try {
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] produs_elements = line.split(cvsSplitBy);
                Produs produs = new Produs(produs_elements[0], produs_elements[1], Float.parseFloat(produs_elements[2]), produs_elements[3], Integer.parseInt(produs_elements[4]));
                repo.getAll().put(produs.getCod_produs(), produs);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException{
        ServerSocket socket = new ServerSocket(6501);
        long start = System.nanoTime();
        initRepo();

        while (true){
            final Socket connection = socket.accept();
            executor.submit(new MyHandler(connection, repo));
            long current = System.nanoTime();
            // verificare stoc si

            if(current - start > 4 * Math.pow(10, 9)){
                Future<Boolean> resp = executor.submit(new StocChecker(repo));
                try{
                    if(resp.get()){
                        System.out.println("Stocurile existente si soldul sunt in regula");
                        start = System.nanoTime();
                    } else{
                        System.out.println("Stocurile existente si soldul au o eroare... shutting this down");
                        executor.shutdown();
                        break;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    executor.shutdown();
                    break;
                } catch (ExecutionException e) {
                    e.printStackTrace();
                    executor.shutdown();
                    break;
                }
            }
        }

    }

}
