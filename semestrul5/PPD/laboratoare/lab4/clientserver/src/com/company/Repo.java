package com.company;

import com.company.Models.Factura;
import com.company.Models.Produs;
import com.company.Models.Vanzare;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class Repo {
    String filename = "outputfiles" + File.separator + "vanzari_facturi.txt";
    final ReentrantLock lock = new ReentrantLock();
    public HashMap<String, Produs> produse = new HashMap<String, Produs>();
    public List<Vanzare> vanzari = new ArrayList<>();
    public List<Factura> facturi = new ArrayList<>();
    public float sold;
    public int index = 0;

    public void setLast_verification(float last_verification) {
        this.last_verification = last_verification;
    }

    public float last_verification = 0;

    public void lock() {
        if (!lock.isHeldByCurrentThread()) lock.lock();
    }

    public void unlock() {
        if (lock.isHeldByCurrentThread()) lock.unlock();
    }

    public Repo() {
    }

    public HashMap<String, Produs> getAll(){
        return produse;
    }


    void write(String message) {
        try {
            File file = new File(this.filename);
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(message + "\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String remove(String id, int cantitate, String numeClient) {
        try {
            lock();
            if (produse.containsKey(id) && produse.get(id).getCantitate() >= cantitate) {
                Produs prod = produse.get(id);
                prod.setCantitate(prod.getCantitate() - cantitate);
                float suma_totala = cantitate * prod.getPret_unitar();

                sold += suma_totala;
                Vanzare vanzare = new Vanzare(new Date(), prod, cantitate);
                vanzari.add(vanzare);
                Factura factura = new Factura(numeClient, vanzare, suma_totala);
                facturi.add(factura);
                String ret_val = prod.toString();
                write(vanzare.toString());
                write(factura.toString());
                unlock();
                return ret_val;
            }
            unlock();
            return "ghinion";

        }  finally {
            unlock();
        }
    }
}
