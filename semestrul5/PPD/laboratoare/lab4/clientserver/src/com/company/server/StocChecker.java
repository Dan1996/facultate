package com.company.server;

import com.company.Models.Factura;
import com.company.Models.Vanzare;
import com.company.Repo;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.Callable;

public class StocChecker implements Callable<Boolean> {
    Repo rep;

    public StocChecker(Repo rep) {
        this.rep = rep;
    }
    String filename = "outputfiles" + File.separator + "vanzari_facturi.txt";

    void write(String message) {
        try {
            File file = new File(filename);
            FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(message + "\n");
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Boolean call() throws Exception {
        float sum = rep.last_verification;
        try{
            rep.lock();
            if (rep.vanzari.size() == rep.index)
                return true;
            for(int i = rep.index; i<rep.vanzari.size(); i++){
                for(int j = rep.index; j<rep.facturi.size(); j++){
                    Factura factura = rep.facturi.get(j);
                    if(factura.getVanzare().equals(rep.vanzari.get(i))){
                        sum += factura.getSuma();
                    }
                }
            }
            rep.setLast_verification(sum);
            rep.index = rep.vanzari.size();
            rep.unlock();
        } finally {
            rep.unlock();
        }

        boolean b = Math.abs(sum - rep.sold) < 0.0001;
        if(b){
            write("Soldul " + sum + " se potriveste cu facturile si vanzarile efectuate");
        } else{
            write("Soldul " + sum + " NU e bun");
        }
        return b;
    }
}
