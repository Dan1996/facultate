package com.company.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.*;

public class FakeClient {
    private static BufferedReader in;
    private static PrintWriter out;
    private static String get_list = "get_list";

    static class MakeAnotherClient extends TimerTask {
        public void run() {
            Socket socket = null;
            try {
                socket = new Socket("localhost", 6501);
                String name = Thread.currentThread().getName();

                in = new BufferedReader(
                        new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                out.println(get_list);

                String msg = in.readLine();
                String prev = "begin";
                while (!msg.equals("done")){
                    prev = msg;
                    msg = in.readLine();
                }
                if(prev.equals("begin")){
                    System.out.println("Nu sunt produse in magazin");
                    return;
                }
                String id = prev.split(" ")[1];
                System.out.println(id);

                out.println(name);
                out.println(id);
                out.println("1");

                String resp = in.readLine();
                if(resp.equals("ghinion")){
                    System.out.println("Naspa, produsu s-a cumparat deja sau nu exista");
                } else {
                    System.out.println("Produs cumparat cu succes");
                }

                out.println("exit");
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static void main(String[] args){
        Timer timer = new Timer();
        timer.schedule(new MakeAnotherClient(), 0, 5000);

    }
}