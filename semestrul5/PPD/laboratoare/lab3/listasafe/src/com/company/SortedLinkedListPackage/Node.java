package com.company.SortedLinkedListPackage;

import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

public class Node {
    public Node(float e) {
        this.e = e;
        isNull = false;
        this.next = new Node();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return Float.compare(node.e, e) == 0 &&
                Objects.equals(next, node.next);
    }

    public void lock() {
        if (!lock.isHeldByCurrentThread()) lock.lock();
    }

    public void unlock() {
        if (lock.isHeldByCurrentThread())
            lock.unlock();
    }


    @Override
    public int hashCode() {
        return 0;
    }

    public Node next;
    public float e;
    ReentrantLock lock = new ReentrantLock();
    public boolean isNull;

    public void setNext(Node next) {
        this.next = next;
    }

    public void setE(float e) {
        this.e = e;
    }

    public Node getNext() {
        return next;
    }

    public float getE() {
        return e;
    }

    public Node(){
        this.e = -10000;
        isNull = true;
    }
}
