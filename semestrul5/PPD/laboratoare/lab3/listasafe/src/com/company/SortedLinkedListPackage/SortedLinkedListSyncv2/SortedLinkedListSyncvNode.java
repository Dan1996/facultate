package com.company.SortedLinkedListPackage.SortedLinkedListSyncv2;

import com.company.SortedLinkedListPackage.Node;
import com.company.SortedLinkedListPackage.SortedLinkedList;

public class SortedLinkedListSyncvNode extends SortedLinkedList {
    public void insert(float a) {
        lock();
        Node toAdd = new Node(a);
        Node prev = new Node();
        Node cur = null;
        try{
            cap.lock();
            unlock();
            if(cap.isNull){
                Node oldCap = cap;
                toAdd.next = cap;
                cap = toAdd;

                oldCap.unlock();
            } else{
                cur = cap;
                prev = new Node();

                while (! cur.isNull && a > cur.e){
                    cur.next.lock();
                    prev.unlock();

                    prev = cur;
                    cur = cur.next;
                }

                toAdd.next = cur;

                prev.next = toAdd;

            }

            n += 1;
        } finally {
            prev.unlock();
            if (cur != null)
                cur.unlock();
        }

        }

    public void delete(float a){
        cap.lock();
        Node cur = cap;
        Node prev = new Node();
        try{
            while (! cur.isNull && a != cur.e){
                cur.lock();
                cur.next.lock();
                prev.unlock();

                prev = cur;
                cur = cur.next;
            }

            if (cur.isNull){
                if (!prev.isNull){
                    prev.unlock();
                }
                throw new NullPointerException("mami tau n-a fost cuminte");
            }

            if (cap.equals(cur)){
                cap = cap.next;
            } else{
                prev.next = cur.next;
                prev.unlock();
            }

            cur.unlock();

            n -= 1;
        } finally {
            cur.unlock();
            prev.unlock();
        }

    }

}
