package com.company.SortedLinkedListPackage.SortedLinkedListSyncv1;

import com.company.SortedLinkedListPackage.SortedLinkedList;
import com.company.SortedLinkedListPackage.SortedListIterator;

import java.util.Iterator;

public class SortedLinkedListSync extends SortedLinkedList {

    public synchronized void insert(float a){
        super.insert(a);
    }

    public synchronized void delete(float a){
        super.delete(a);
    }

    public Iterator<Float> getIterator(){
        return new SortedListIterator(this);
    }

}
