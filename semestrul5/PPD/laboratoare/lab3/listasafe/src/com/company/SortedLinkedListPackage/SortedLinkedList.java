package com.company.SortedLinkedListPackage;

import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

public class SortedLinkedList {
    public Node cap = new Node();
    public int n;
    final ReentrantLock lock = new ReentrantLock();

    public void lock() {
        if (!lock.isHeldByCurrentThread()) lock.lock();
    }

    public void unlock() {
        if (lock.isHeldByCurrentThread()) lock.unlock();
    }


    public SortedLinkedList() {
        n = 0;
    }

    public void insert(float a){
        Node toAdd = new Node(a);
        Node cur = cap;
        Node prev = new Node();

        while (! cur.isNull && a > cur.e){
            prev = cur;
            cur = cur.next;
        }

        toAdd.next = cur;
        if(cap.isNull || cap.equals(cur)){
            cap = toAdd;
        } else {
            prev.next = toAdd;
        }

        n += 1;
    }

    public void delete(float a){
        Node cur = cap;
        Node prev = new Node();

        while (! cur.isNull && a != cur.e){
            prev = cur;
            cur = cur.next;
        }

        if (cur.isNull){
            throw new NullPointerException("Elementul "+ a + " nu exista in lista");
        }

        if (cap.equals(cur)){
            cap = cap.next;
        } else{
            prev.next = cur.next;
        }

        n -= 1;
    }

    public Iterator<Float> getIterator(){
        return new SortedListIterator(this);
    }

}
