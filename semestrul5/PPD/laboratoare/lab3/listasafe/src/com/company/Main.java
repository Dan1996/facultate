package com.company;

import com.company.SortedLinkedListPackage.SortedLinkedList;
import com.company.SortedLinkedListPackage.SortedLinkedListSyncv1.SortedLinkedListSync;
import com.company.SortedLinkedListPackage.SortedLinkedListSyncv2.SortedLinkedListSyncvNode;
import com.company.SortedLinkedListPackage.SortedListIterator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.concurrent.locks.ReentrantLock;

public class Main {
    static ReentrantLock lock = new ReentrantLock();

    public static void lock() {
        if (!lock.isHeldByCurrentThread()) lock.lock();
    }

    public static void unlock() {
        if (lock.isHeldByCurrentThread()) lock.unlock();
    }

    public static void test1(SortedLinkedList list) throws IOException {
        BufferedWriter bw = getWriter("List1.log");
        Thread t1 = new Thread(() -> {
            for(int i=0; i<10; i++){
                list.insert(i);
                write("T1 insert " + i, bw);
            }
        });
        Thread t2 = new Thread(() -> {
            for(int i=10; i<15; i++){
                list.insert(i);
                write("T2 insert " + i, bw);
            }
        });
        Thread t3 = new Thread(() -> {
            for(int i=0; i<7; i++){
                try{
                    list.delete(i);
                    write("T3 delete " + i, bw);
                } catch (NullPointerException ex){
                    write("T3 " + ex + "", bw);
                }

            }
        });

        Thread t4 = new Thread(() -> {
            while (t1.isAlive() || t2.isAlive() || t3.isAlive()) {
                write("T4 iterate", bw);
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
                    float e = it.next();
                }
            }
            write("T4 iterate", bw);
            for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
                float e = it.next();
            }
        });

        runTest(new Thread[] {t1, t2, t3, t4});
        write(stringFromList(list), bw);
        bw.close();
    }

    public static void test2(SortedLinkedList list) throws IOException {
        BufferedWriter bw = getWriter("List2.log");
        Thread t1 = new Thread(() -> {
            for(int i=0; i<100; i++){
                list.insert(i);
                write("T1 insert " + i, bw);
            }
        });
        Thread t2 = new Thread(() -> {
            for(int i=100; i<150; i++){
                list.insert(i);
                write("T2 insert " + i, bw);
            }
        });
        Thread t3 = new Thread(() -> {
            for(int i=0; i<50; i++){
                try{
                    list.delete(i);
                    write("T3 delete " + i, bw);
                } catch (NullPointerException ex){
                    write("T3 " + ex + "", bw);
                }

            }
        });

        Thread t4 = new Thread(() -> {
            while (t1.isAlive() || t2.isAlive() || t3.isAlive()) {
                write("T4 iterate", bw);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
                    float e = it.next();
                }
            }
            write("T4 iterate", bw);
            for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
                float e = it.next();
            }
        });

        runTest(new Thread[] {t1, t2, t3, t4});
        write(stringFromList(list), bw);
        bw.close();
    }

    static String stringFromList(SortedLinkedList list){
        String rez = "";
        for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
            float e = it.next();
            rez += e + " ";
        }
        return rez;
    }

    static void runTest(Thread[] threads) {
        for (Thread t : threads) t.start();
        try {
            for (Thread t : threads) t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static BufferedWriter getWriter(String filename) throws IOException {
        File file = new File(filename);
        FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
        BufferedWriter bw = new BufferedWriter(fw);
        write("NEW RUN", bw);
        return bw;
    }

    static void write(String message, BufferedWriter bw) {
        try {
            bw.write(message + " " + System.nanoTime() + "\n"); // write to file
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        SortedLinkedList list = new SortedLinkedListSyncvNode();

        list.insert(3);
        list.insert(5);
        list.insert(1);
        list.insert(2);
        list.insert(4);
        list.insert(10);
        list.delete(10);



//        for (Iterator<Float> it = list.getIterator(); it.hasNext(); ) {
//            float e = it.next();
//            System.out.println(e);
//        }
        test1(new SortedLinkedListSync());
        test1(new SortedLinkedListSyncvNode());
        test2(new SortedLinkedListSync());
        test2(new SortedLinkedListSyncvNode());


    }
}
