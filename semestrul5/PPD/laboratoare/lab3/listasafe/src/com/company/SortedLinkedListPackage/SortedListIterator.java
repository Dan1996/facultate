package com.company.SortedLinkedListPackage;

import java.util.Iterator;

public class SortedListIterator implements Iterator<Float> {
    SortedLinkedList sortedLinkedList;
    Node current;

    public SortedListIterator(SortedLinkedList sortedLinkedList){
        this.sortedLinkedList = sortedLinkedList;
        current = sortedLinkedList.cap;
        sortedLinkedList.lock();
    }

    public SortedListIterator(){
    }

    @Override
    public boolean hasNext()
    {
        boolean thruth = !current.isNull;
        if(! thruth){
            sortedLinkedList.unlock();
        }
        return thruth;
    }

    @Override
    public Float next() {
        Node prev = current;
        current = current.next;
        return prev.e;
    }
}
