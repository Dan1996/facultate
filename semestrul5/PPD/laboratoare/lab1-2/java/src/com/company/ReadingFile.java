package com.company;

public interface ReadingFile<T> {
    public T apply(String filename);
}
