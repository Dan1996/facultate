package com.company;

public class MatrixAdder<T> {
    public MatrixAdder(CustomAdd<T> addOp, int p) {
        this.addOp = addOp;
        this.p = p;
    }

    public MatrixAdder(CustomAdd<T> addOp) {
        this.addOp = addOp;
    }

    public Matrix<T> addMatrixes(Matrix<T> matrix1, Matrix<T> matrix2){
        Matrix<T> localMatrix   = new Matrix<>(matrix1.getN(), matrix2.getM());

        Thread[] threads = new Thread[this.p];
        long start;
        start=System.nanoTime();

        for (int i = 0; i < this.p; i++) {
            threads[i] = new Thread(new AddRun<T>(i, p, matrix1, matrix2, localMatrix, addOp));
            threads[i].start();
        }

        for(int i=0;i<this.p;i++){
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print(" "+(System.nanoTime() - start));

        return localMatrix;
    }

    private CustomAdd<T> addOp;
    private int p = 1;

}
