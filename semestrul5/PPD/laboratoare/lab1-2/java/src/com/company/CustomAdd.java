package com.company;

public interface CustomAdd<T> {
    public T process(T e, T b);
}
