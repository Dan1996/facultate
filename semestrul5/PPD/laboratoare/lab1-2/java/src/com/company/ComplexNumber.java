package com.company;

public class ComplexNumber {
    private int i, j;

    public ComplexNumber(int i, int j) {
        this.i = i;
        this.j = j;
    }

    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }
}
