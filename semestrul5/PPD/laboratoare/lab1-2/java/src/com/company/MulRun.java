package com.company;

public class MulRun implements Runnable{
    int i,n,m,p,x,y;
    int[][] matrix1, matrix2, rezMatrix;

    MulRun(int i, int p,int n, int m,int x, int y, int[][] matrix1, int[][] matrix2, int[][] rezMatrix){
        this.i = i;
        this.n = n;
        this.m = m;
        this.p = p;
        this.x = x;
        this.y = y;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.rezMatrix = rezMatrix;
    }

    @Override
    public void run() {
        int l,c,s;
        for(int j=i; j<n*y; j+=p){
            l=j/y;
            c=j%y;
            s = 0;
            for(int k=0; k<m; k++){
                s += matrix1[l][k] * matrix2[k][c];
            }
            rezMatrix[l][c] = s;
        }
    }
}
