package com.company;

public class AddRun<T> implements Runnable{
    int i, p;
    Matrix<T> matrix1, matrix2, rezMatrix;
    CustomAdd<T> addOp;

    public AddRun(int i, int p, Matrix<T> matrix1, Matrix<T> matrix2, Matrix<T> rezMatrix, CustomAdd<T> addOp) {
        this.i = i;
        this.p = p;
        this.matrix1 = matrix1;
        this.matrix2 = matrix2;
        this.rezMatrix = rezMatrix;
        this.addOp = addOp;
    }

    @Override
    public void run() {
        int l,c;
        for(int j=i;j<matrix1.getN()*matrix2.getM();j+=p){
            l=j/matrix1.getM();
            c=j%matrix2.getM();
            rezMatrix.setElem(l, c, addOp.process(matrix1.getElem(l,c), matrix2.getElem(l,c)));
        }

    }
}