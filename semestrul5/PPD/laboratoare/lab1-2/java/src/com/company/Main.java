package com.company;
//1 b 2 a 3 c tema LFTC
import java.io.*;
import java.util.Scanner;

public class Main {
    private  static  void readForMatrixInteger(Matrix<Integer> matrix, Scanner scanner){
        for ( int i = 0; i < matrix.getN(); i++){
            for (int j=0; j<matrix.getM(); j++){
                matrix.setElem(i, j, scanner.nextInt());
            }
        }
    }

    private static void initiliazeMatrixInteger(Matrix<Integer> m1, Matrix<Integer> m2, int ord){
        File fr = null;
        int n=0,m;
        try {
            fr = new File("src" + File.separator + "com" + File.separator + "company" + File.separator + "iofiles" + File.separator + Integer.class.getName() + "file" + ord + ".txt");
            Scanner scanner = new Scanner(fr);
            n=scanner.nextInt();
            m=scanner.nextInt();

            m1.setN(n);
            m1.setM(m);
            m2.setN(n);
            m2.setM(m);

            m1.setMatrix(new Integer[n][m]);
            m2.setMatrix(new Integer[n][m]);

            readForMatrixInteger(m1, scanner);
            readForMatrixInteger(m2, scanner);

            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private  static  void readForMatrixFloat(Matrix<Float> matrix, Scanner scanner){
        for ( int i = 0; i < matrix.getN(); i++){
            for (int j=0; j<matrix.getM(); j++){
                matrix.setElem(i, j, scanner.nextFloat() * 100);
            }
        }
    }

    private static void initiliazeMatrixFloat(Matrix<Float> m1, Matrix<Float> m2, int ord){
        File fr = null;
        int n=0,m;
        try {
            fr = new File("src" + File.separator + "com" + File.separator + "company" + File.separator + "iofiles" + File.separator + Float.class.getName() + "file" + ord + ".txt");
            Scanner scanner = new Scanner(fr);
            n=scanner.nextInt();
            m=scanner.nextInt();

            m1.setN(n);
            m1.setM(m);
            m2.setN(n);
            m2.setM(m);

            m1.setMatrix(new Float[n][m]);
            m2.setMatrix(new Float[n][m]);

            readForMatrixFloat(m1, scanner);
            readForMatrixFloat(m2, scanner);

            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private  static  void readForMatrixComplex(Matrix<ComplexNumber> matrix, Scanner scanner){
        for ( int i = 0; i < matrix.getN(); i++){
            for (int j=0; j<matrix.getM(); j++){
                matrix.setElem(i, j, new ComplexNumber(scanner.nextInt(), scanner.nextInt()));
            }
        }
    }

    private static void initiliazeMatrixComplex(Matrix<ComplexNumber> m1, Matrix<ComplexNumber> m2, int ord){
        File fr = null;
        int n=0,m;
        try {
            fr = new File("src" + File.separator + "com" + File.separator + "company" + File.separator + "iofiles" + File.separator + ComplexNumber.class.getName() + "file" + ord + ".txt");
            Scanner scanner = new Scanner(fr);
            n=scanner.nextInt();
            m=scanner.nextInt();

            m1.setN(n);
            m1.setM(m);
            m2.setN(n);
            m2.setM(m);

            m1.setMatrix(new ComplexNumber[n][m]);
            m2.setMatrix(new ComplexNumber[n][m]);

            readForMatrixComplex(m1, scanner);
            readForMatrixComplex(m2, scanner);

            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static<T> void printMatrix(Matrix<T> matrix, int ord){
        String rez;
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("src" + File.separator + "com" + File.separator + "company" + File.separator + "rezfiles" + File.separator + matrix.getElem(0, 0).getClass().getName() +  "file" + ord + ".txt");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println("" + matrix.getN() + " " + matrix.getM());

            for (int k = 0; k < matrix.getN(); k++) {
                rez = "";
                for (int j = 0; j < matrix.getM() - 1; j++) {
                    rez += matrix.getElem(k, j) + "  ";
                }
                rez += matrix.getElem(k, matrix.getM() - 1);
                printWriter.println(rez);
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void mulMatrix (int[][] matrix1, int[][] matrix2, int[][] rezMatrix, int n, int m, int x, int y, int p){
        Thread[] threads = new Thread[p];
        long start;

        start=System.nanoTime();
        for (int i = 0; i < p; i++) {
            threads[i] = new Thread(new MulRun(i, p, n, m, x, y, matrix1, matrix2, rezMatrix));
            threads[i].start();
        }

        for(int i=0;i<p;i++){
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print(" "+(System.nanoTime() - start));
    }


    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        int p;

        int choice=1;
        String type = "ma-ta";
//        while(choice != 1 && choice != 2) {

        switch (choice) {
            case 1:
                int choice2;
                System.out.printf("1.Int, 2.Float inmultire, 3.Complex inmultire 4. Float supra 5 Complex supra");
                choice2 = reader.nextInt();

                if (choice2 == 1){
                    Matrix<Integer> m1 = new Matrix<Integer>(), m2 = new Matrix<Integer>();

                    System.out.print("Alegeti numarul de threaduri\n");
                    p = reader.nextInt();
                    CustomAdd<Integer> customAdd = (a, b)->(a + b);
                    MatrixAdder<Integer> adder = new MatrixAdder<Integer>(customAdd);
                    initiliazeMatrixInteger(m1, m2, 10);
                    System.out.print("\n");
                    Matrix<Integer> rezMatrx = adder.addMatrixes(m1, m2);
                    printMatrix(rezMatrx, 10);
                } else if (choice2 == 2){
                    Matrix<Float> m1 = new Matrix<Float>(), m2 = new Matrix<Float>();

                    System.out.print("Alegeti numarul de threaduri\n");
                    p = reader.nextInt();
                    CustomAdd<Float> customAdd = (a, b)->(a * b);
                    MatrixAdder<Float> adder = new MatrixAdder<Float>(customAdd);
                    initiliazeMatrixFloat(m1, m2, 10);
                    System.out.print("\n");
                    Matrix<Float> rezMatrx = adder.addMatrixes(m1, m2);
                    printMatrix(rezMatrx, 10);
                } else if (choice2 == 3){
                    Matrix<ComplexNumber> m1 = new Matrix<ComplexNumber>(), m2 = new Matrix<ComplexNumber>();

                    System.out.print("Alegeti numarul de threaduri\n");
                    p = reader.nextInt();
                    CustomAdd<ComplexNumber> customAdd = (a, b)->(new ComplexNumber(a.getI() * b.getI() - a.getJ() * b.getJ(), a.getI() * b.getJ() + b.getI()* a.getJ()));
                    MatrixAdder<ComplexNumber> adder = new MatrixAdder<ComplexNumber>(customAdd);
                    initiliazeMatrixComplex(m1, m2, 10);
                    System.out.print("\n");
                    Matrix<ComplexNumber> rezMatrx = adder.addMatrixes(m1, m2);
                    printMatrix(rezMatrx, 10);
                } else if (choice2 == 4){
                    Matrix<Float> m1 = new Matrix<Float>(), m2 = new Matrix<Float>();

                    System.out.print("Alegeti numarul de threaduri\n");
                    p = reader.nextInt();
                    CustomAdd<Float> customAdd = (a, b)->(1/a + 1/b);
                    MatrixAdder<Float> adder = new MatrixAdder<Float>(customAdd);
                    initiliazeMatrixFloat(m1, m2, 10);
                    System.out.print("\n");
                    Matrix<Float> rezMatrx = adder.addMatrixes(m1, m2);
                    printMatrix(rezMatrx, 10);
                } else if (choice2 == 5){
                    Matrix<ComplexNumber> m1 = new Matrix<ComplexNumber>(), m2 = new Matrix<ComplexNumber>();

                    System.out.print("Alegeti numarul de threaduri\n");
                    p = reader.nextInt();
                    CustomAdd<ComplexNumber> customAdd = (a, b)->(new ComplexNumber((a.getI() + b.getI()) / 2, (a.getJ() + b.getJ()) / 2));
                    MatrixAdder<ComplexNumber> adder = new MatrixAdder<ComplexNumber>(customAdd);
                    initiliazeMatrixComplex(m1, m2, 10);
                    System.out.print("\n");
                    Matrix<ComplexNumber> rezMatrx = adder.addMatrixes(m1, m2);
                    printMatrix(rezMatrx, 10);

                }

                break;
            case 2:
//                    for(int h=1;h<=10;h++) {
//                        initiliazeMatrixInteger(m1, m2, h);
//                        System.out.printf("\n");
//                        mulMatrix(matrixes.getMatrix1(), matrixes.getMatrix2(), matrixes.getRezMatrix(),matrixes.getN(),matrixes.getM(), matrixes.getN(),matrixes.getM(), p);
//                        printMatrix(matrixes.getRezMatrix(), matrixes.getN(), matrixes.getN(), h);
////                    }
                break;
            default:
                System.out.printf("No bueno, try again(romgleza)\n");
                break;
        }
//        }

        reader.close();
    }
}
