package com.company;

public class Matrix<T> {
    private T[][] matrix;
    private int n, m;

    public Matrix() {
    }

    public T[][] getMatrix() {
        return matrix;
    }

    public Matrix(int n, int m) {
        this.n = n;
        this.m = m;
        this.matrix = (T[][]) new Object[this.n][this.m];
    }

    public int getN() {
        return n;
    }

    public int getM() {
        return m;
    }

    public void setMatrix(T[][] matrix) {
        this.matrix = matrix;
    }

    public void setN(int n) {
        this.n = n;
    }

    public void setM(int m) {
        this.m = m;
    }

    public T getElem(int i, int j){
        return this.matrix[i][j];
    }

    public void setElem(int i, int j, T el){
        this.matrix[i][j] = el;
    }

}
