#pragma once
#include "Matrix.h"
#include <thread>

template <class T>
class MatrixAdder {
	int p;
	std::thread* threads;

	std::function<T(T, T)> lambda;
public:
	MatrixAdder(int p, std::function<T(T, T)> lambda) :p{ p }, lambda{ lambda } {
		threads = new std::thread[p];
	};

	Matrix<T> addMatrixes(Matrix<T>& m1, Matrix<T>& m2) {
		Matrix<T> rezMatrix(m1.getN(), m2.getM());
	
		auto start = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < p; i++) {
			threads[i] = std::thread([&](std::function<T(T, T)> lambda, int i, int p) {
				int l, c;
				for (int j = i; j<m1.getN()*m2.getM(); j += p) {
					l = j / m1.getM();
					c = j % m2.getM();
					rezMatrix.setElem(l, c, lambda(m1.getElem(l, c), m2.getElem(l, c)));
				}
			}, lambda, i, p);
		}

		for (int i = 0; i < p; i++) {
			threads[i].join();
		}
		cout << (std::chrono::high_resolution_clock::now() - start).count();
		return rezMatrix;
	}

};