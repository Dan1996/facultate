// ConsoleApplication1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include "Matrix.h"
#include "MatrixAdder.h"
#include "Complex.h"
#include "ConsoleApplication1.h"

using namespace std;

template<class T>
void initializeMatrix(Matrix<T>& m1, Matrix<T>& m2, string filename) {
	int n, m;
	ifstream f(filename, std::ios_base::in);
	
	f >> n >> m;
	T e;

	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			f >> e;
			m1.setElem(i, j, e);
		}
	
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++) {
			f >> e;
			m2.setElem(i, j, e);
		}
}

int main()
{
	int p;
	cout << "1. float inmultire , 2. complex inmultire, 3. float impartire, 4. complex impartire";
	int opt;
	cin >> opt;
	string fileName;
	
	cout << "Alegeti numarul de threaduri";
	cin >> p;

	
	switch (opt)
	{
	case 1:
	{
		
		fileName = "..\\..\\..\\java\\ProgramareParalela\\src\\com\\company\\iofiles\\java.lang.Floatfile10.txt";
		Matrix<float> m1(1000, 1000), m2(1000, 1000);
		MatrixAdder<float> adder(p, [](float a, float b) {return a * b; });
		initializeMatrix<float>(m1, m2, fileName);
		Matrix<float> rezMatrix = adder.addMatrixes(m1, m2);

	}
		break;
	case 2:
	{
		fileName = "..\\..\\..\\java\\ProgramareParalela\\src\\com\\company\\iofiles\\com.company.ComplexNumberfile10.txt";
		Matrix<Complex> m1(1000, 1000), m2(1000, 1000);
		
		MatrixAdder<Complex> adder(p, [](Complex a, Complex b) {return Complex(a.getI() * b.getI() - a.getJ() * b.getJ(), a.getI() * b.getJ() + b.getI()* a.getJ()); });
		
		initializeMatrix<Complex>(m1, m2, fileName);
		Matrix<Complex> rezMatrix = adder.addMatrixes(m1, m2);
		cout << endl;

		break;
	}
	case 3:
	{
		fileName = "..\\..\\..\\java\\ProgramareParalela\\src\\com\\company\\iofiles\\java.lang.Floatfile10.txt";
		Matrix<float> m1(1000, 1000), m2(1000, 1000);
		MatrixAdder<float> adder(p, [](float a, float b) {return 1/a + 1/b; });
		initializeMatrix<float>(m1, m2, fileName);
		Matrix<float> rezMatrix = adder.addMatrixes(m1, m2);
		break;
	}
	case 4:
	{
		fileName = "..\\..\\..\\java\\ProgramareParalela\\src\\com\\company\\iofiles\\com.company.ComplexNumberfile10.txt";
		Matrix<Complex> m1(1000, 1000), m2(1000, 1000);

		MatrixAdder<Complex> adder(p, [](Complex a, Complex b) {return Complex((a.getI() + b.getI()) / 2, (a.getJ() + b.getJ()) / 2); });

		initializeMatrix<Complex>(m1, m2, fileName);
		Matrix<Complex> rezMatrix = adder.addMatrixes(m1, m2);
		break;
	}
	default:
		break;
	}
	
	
	int a;
	cin >> a;
    return 0;
}

