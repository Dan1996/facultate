#pragma once
using namespace std;
class Complex {
	int i, j;
public:
	Complex(){}
	Complex(int i, int j) :i{ i }, j{ j } {};

	int getI() {
		return i;
	}

	int getJ() {
		return j;
	}

	void setI(int i) {
		this->i = i;
	}

	void setJ(int j) {
		this->j = j;
	}

	friend ostream& operator<<(ostream& os, Complex& dt);
	friend ifstream& operator>>(ifstream& is, Complex& dt);
};

ostream& operator<<(ostream& os, Complex& dt)
{
	os << dt.getI()<< "+" << dt.getJ()<<"i";
	return os;
}

ifstream& operator>>(ifstream& is, Complex& dt) {
	int i, j;
	is >> i >> j;
	dt.setI(i);
	dt.setJ(j);
	return is;
}