#pragma once
#include <vector>

template <class T>
class Matrix {
	int n, m;
	T** matrix;
public:
	Matrix(int n, int m) :n{ n }, m{ m } {
		matrix = new T*[n];

		for (int i = 0; i < n; i++) {
			matrix[i] = new T[m];
		}
	};

	void setElem(int i, int j, T val) {
		matrix[i][j] = val;
	}

	T& getElem(int i, int j) {
		return matrix[i][j];
	}

	int getN() {
		return n;
	}

	int getM() {
		return m;
	}
};