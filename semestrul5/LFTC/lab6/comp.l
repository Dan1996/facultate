%{
	#include <string.h> /* for strcpy */

#include "attrib.h"
#include "y.tab.h"

extern int lineno;  /* defined in micro.y */
%}

%%
[ \t\r]        /*   ignore white spaces */
[\n]                    lineno++;

"let"                   return VAR;
"begin"                 return BEGINPAR;
"end"                   return ENDPAR;
"read"                  return READ;
"print"                 return PRINT;
"="                     return ASSIGN;
";"                     return SEMICOLON;
"+"                     return PLUS;
"-"                     return MINUS;

[A-Za-z]+      {
                         strcpy(yylval.varname,yytext);
                         return NAME;
                         }

[0-9]+                {strcpy(yylval.varname,yytext);
                        return NUMBER;
                        }

.                       {
                        printf( "My: Illegal character \n %s \n on line #%d\n",
                                yytext,lineno);

                        }

%%


int
yywrap() {
  return 1; /* tell scanner no further files need to be processed */
}