%{
#include <stdio.h> /* for (f)printf() */
#include <stdlib.h> /* for exit() */
#include <string.h>
#include "attrib.h"
#include "codeASM.h"


int  lineno = 1; /* number of current source line */
extern int yylex(); 
extern char *yytext;
char vars[1000][100];
char tempbuffer[250];
int numbars = 0;

void yyerror(char *s) {
  printf("\n \n \nMy error: \n");
  printf( "Syntax error on line #%d: %s\n", lineno, s);
  printf( "Last token was \"%s\"\n", yytext);
  exit(1);
}

int tempnr = 1;
void newTempName(char* s){
  sprintf(s,"[temp%d]",tempnr);
  tempnr++;
}

void addVar(char* s){
  strcpy(vars[numbars], s);
  numbars++;
}

char datasegmentbuffer[500];
char tempbuffer[250];

void addTemps2String(char* str){
 int i;
 for(i=1;i<tempnr;i++){
    sprintf(tempbuffer,"temp%d dw ?\n",i);
    strcat(str,tempbuffer);
 }
}

int tempboolnr = 1;
void newTempBoolName(char* s){
  sprintf(s,"tempbool%d",tempnr);
  tempboolnr++;
}

char tempBoolBuffer[250];
void buildTempBoolBuffers(){
 int i;
 if (tempboolnr>1){
  strcpy(tempBoolBuffer,"\n");
  for(i=1;i<tempboolnr;i++){
    sprintf(tempBoolBuffer,"%stemp%d db ?\n",tempBoolBuffer,i);
    }
 }
}


%}


%union {
 char varname[10];
 attributes attrib;
 char strCode[250];
}


%token VAR
%token BEGINPAR
%token ENDPAR
%token INTEGER
%token ASSIGN
%token SEMICOLON
%token PLUS
%token MINUS
%token DECLARE
%token READ
%token PRINT

%token <varname> NAME
%token <varname> NUMBER
%type <varname> variable
%type <attrib> term
%type <attrib> expression



%%
program         :    BEGINPAR
                     statement_list
                     ENDPAR {
                          printf(
                            "\
                            mov eax, 1 \n\
                            int 0x80\n\
                            section .data\n"
                            );
                            for (int i=0; i< numbars; i++){
                              printf("%s dd 0 \n", vars[i]);
                            }
                            for (int i=1; i< tempnr; i++){
                              printf("temp%d dd 0 \n", i);
                            }
                          printf(
                            "\
                            buf  dd 0, 0, 0, 0, 0 \n\
                            buf1 dd 0 \n");
                     }
                     ; 


statement_list  :  statement_list SEMICOLON statement 
                | statement
                ;

statement       : declaration 
                |  read     
                |  print
                |  assignment

                ;

read : READ variable
       {
          printf(READ_ASM_FORMAT, $2);
       }
       ;

declaration : VAR NAME 
            {
              addVar($2);
            }
            ;

print : PRINT expression 
          {
            printf("%s\n",$2.code);
            printf("mov eax, %s\n",$2.varn);
            printf(PRINT_ASM_FORMAT);
          }
          ;

assignment      : variable ASSIGN expression
                  {
                    printf("%s\n",$3.code);
                    printf("mov eax, %s\n",$3.varn);
                    printf("mov %s,eax\n",$1);
                   };
                ;

expression      : term {
                      strcpy($$.code,$1.code);
                      strcpy($$.varn,$1.varn);
                       }
                | expression PLUS term {
                     newTempName($$.varn);
                     sprintf($$.code,"%s\n%s\n",$1.code,$3.code);
                     sprintf(tempbuffer,ADD_ASM_FORMAT,$1.varn,$3.varn,$$.varn);
                     strcat($$.code,tempbuffer);
                     }
                | expression MINUS term {
                     newTempName($$.varn);
                     sprintf($$.code, "%s\n%s\n", $1.code, $3.code);
                     sprintf(tempbuffer,SUB_ASM_FORMAT,$1.varn,$3.varn,$$.varn);
                     strcat($$.code,tempbuffer);
                }
                ;

term            : NUMBER {
                      strcpy($$.code,"");
                      strcpy($$.varn,$1);
                      }
                | variable {
                        strcpy($$.code,"");
                        strcpy($$.varn,$1);
                      }
                ;

variable        : NAME    {
                      int a = 0;
                      for(int i = 0; i < numbars; i++){
                        if(!strcmp(vars[i], $1)){
                          a = 1;
                        }
                      }
                      if (!a){
                         yyerror("nu exista variabila");
                      }
                      sprintf($$, "[%s]", $1);
                  }
                ;
%%

int
main(int argc,char *argv[]) {
   printf(
   "\
section .text \n\
global _start \n\
_start: \n\
"
  );
  return yyparse();
}
