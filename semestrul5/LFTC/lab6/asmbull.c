section .text

global _start

_start:
    
    ; read a byte from stdin
    mov eax, 3       ; 3 is recognized by the system as meaning "read"
    mov ebx, 0       ; read from standard input
    mov ecx, buf        ; address to pass to
    mov edx, 3       ; input length (one byte)
    int 0x80                 ; call the kernel


    ;convert string to int
    mov edx, buf ; our string
    atoi:
    xor eax, eax ; zero a "result so far"
    .top:
    movzx ecx, byte [edx] ; get a character
    inc edx ; ready for next one
    cmp ecx, '0' ; valid?
    jb .done
    cmp ecx, '9'
    ja .done
    sub ecx, '0' ; "convert" character to number
    imul eax, 10 ; multiply "result so far" by ten
    add eax, ecx ; add in current digit
    jmp .top ; until done
    .done:

    
    
    ; CONVERT TO STRING
    ; mov eax, all bytes
    mov ecx,10
    xor ebx, ebx ;count of the pushes
    divide:
    xor edx, edx
    div ecx
    push edx ; DL is a digit in range [0..9]
    inc ebx ; Count it
    test eax, eax
    jnz divide ;
    mov ecx, ebx
    
    get_digit:
    pop eax
    add eax, '0' ; Make it ASCII
    mov [buf1] , eax
    inc esi
    
    push esi
    push ecx
        mov eax, 4           ; the system interprets 4 as "write"
        mov ebx, 1           ; standard output (print to terminal)
        mov ecx, buf1     ; pointer to the value being passed
        mov edx, 1           ; length of output (in bytes)
        int 0x80             ; call the kernel
    pop ecx
    pop esi
    loop get_digit

    
    mov     eax, 1
    int     0x80

section .data
    x db '5'
    y db '3'
    msg db  "sum of x and y is "
    len equ $ - msg
    buf  db 0, 0, 0, 0, 0
    buf1 db 0
    buf2 db 0


segment .bss

    sum resb 1
