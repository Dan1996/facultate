%{
void yyerror (char *s);
int yylex();
#include <stdio.h>     /* C declarations used in actions */
#include <stdlib.h>
#include <ctype.h>
#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <functional>
using namespace std;

struct Elem{
	int atomCode;
	int tsPosition;
};
vector<Elem> Fip;
map<string, int> Ts;
%}

%union {int num; char id[150]; float num2; }         /* Yacc definitions */
%start line
%token print
%token exit_command
%token <num> number
%token <id> identifier
%type <num> line exp term
%type <id> assignment
%%

/* descriptions of expected inputs     corresponding actions (in C) */

line    : assignment ';'		{;}
		| exit_command ';'		{exit(EXIT_SUCCESS);}
		| print exp ';'			{printf("Printing %d\n", $2);}
		| line assignment ';'	{;}
		| line print exp ';'	{printf("Printing %d\n", $3);}
		| line exit_command ';'	{exit(EXIT_SUCCESS);}
        ;

assignment : identifier '=' exp  { Ts[string($1)] = $3; }
			;
exp    	: term                  {$$ = $1;}
       	| exp '+' term          {$$ = $1 + $3;}
       	| exp '-' term          {$$ = $1 - $3;}
       	;
term   	: number                {$$ = $1;}
		| identifier			{$$ = Ts[string($1)];} 
        ;

%%                     /* C code */

int main (void) {
	/* init symbol table */

	return yyparse ();
}

void yyerror (char *s) {fprintf (stderr, "%s\n", s);} 

