class Stare:
    def __init__(self, name, is_terminal):
        self.name = name
        if is_terminal == "1":
            self.is_terminal = True
        else:
            self.is_terminal = False
        self.tranzitions = {}

    def __repr__(self):
        return self.name


class Automat:
    def __init__(self, stare_start):
        self.stare_start = stare_start

    def check_sequence(self, sequence):
        stack = [(0, self.stare_start)]
        max_sequence = ''

        while stack:
            index_start, current_stare = stack.pop()

            if current_stare.is_terminal and index_start == len(sequence):
                return sequence

            for i in range(index_start, len(sequence)):
                if sequence[i] not in current_stare.tranzitions:
                    if current_stare.is_terminal:
                        max_sequence = max(max_sequence, sequence[:i], key=lambda x: len(x))
                    break
                else:
                    if current_stare.is_terminal:
                        return_if_not_break = sequence[:i]
                    current_stare, *for_later = current_stare.tranzitions[sequence[i]]
                    for el in for_later:
                        stack.append((i + 1, el))
            else:
                if current_stare.is_terminal:
                    return sequence
                else:
                    return return_if_not_break

        return max_sequence


class FileReader:
    @staticmethod
    def search_elems(stari, elems):
        first_finished = False
        for el in stari:
            if el.name == elems[0]:
                first = el
                if first_finished:
                    break
                first_finished = True
            if el.name == elems[2]:
                second = el
                if first_finished:
                    break
                first_finished = True

        return first, second

    def init_automat(self, file_name):
        alphabet = set()
        tranzactii = []
        with open(file_name) as f:
            elems = f.readline().split()
            stari = [Stare(name, is_terminal) for name, is_terminal in zip(elems[::2], elems[1::2])]
            automat = Automat(stari[0])
            for line in f:
                tranzactii.append(line.strip())
                elems = line.split()
                el1, el2 = self.search_elems(stari, elems)
                if elems[1] in el1.tranzitions:
                    el1.tranzitions[elems[1]].append(el2)
                else:
                    el1.tranzitions[elems[1]] = [el2]

                alphabet.add(elems[1])

        return automat, stari, alphabet, tranzactii


class Starter:
    def __init__(self, file_name):
        self.file_reader = FileReader()
        self.automat, self.stari, self.alphabet, self.tranzactii = self.file_reader.init_automat(file_name)

    def start(self):
        while True:
            cmds = {"1", "2", "3", "4"}
            cmd = input("1. Afiseaza elementele automatului \
            2. Verifica secventa in automat\
            3. Cel mai lung prefix\
            4. Iesire")

            if cmd not in cmds:
                print("Comanda inexistenta")
                continue

            if cmd == "1":
                self.print_details()

            if cmd == "2":
                sequence = input()
                rez = self.automat.check_sequence(sequence)
                if rez == sequence:
                    print("Secventa corecta")
                else:
                    print("Secventa gresita")

            if cmd == "3":
                sequence = input()
                print(self.automat.check_sequence(sequence))

            if cmd == "4":
                break

    def print_details(self):
        print("Stari:", self.stari)
        print("Alphabet:", self.alphabet)
        print("Tranzitii:")
        for el in self.tranzactii:
            print(el)

        print("Stari finale:", [el for el in self.stari if el.is_terminal])


if __name__ == '__main__':
    starter = Starter("..\\lab1\\automat_variabila")
    starter.start()
