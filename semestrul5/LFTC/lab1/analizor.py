import sys
from keyword import iskeyword

from lab2.automat import FileReader


class LexicalError(Exception):
    pass


class Parser:
    operators = sorted([
        ";",
        ":",
        '=',
        "!=",
        "(",
        ")",
        "+",
        "<",
        "*",
        "%",
    ]
        , key=lambda x: len(x), reverse=True)

    def __init__(self):
        self.atom_code = self.initialise_table()
        self.current_id = 1
        self.fip = []
        self.ts = {}
        self.line = 1
        filereader = FileReader()
        self.automat_numeric = filereader.init_automat("automat_numere")[0]
        self.automat_variabile = filereader.init_automat("automat_variabila")[0]
        self.atom_combinations = self.get_atom_combinations()
        self.seps = [" ", "\n"]

    @staticmethod
    def initialise_table():

        atomi = [
            'id',
            'const',
            "input",
            "while",
            "print",
            *Parser.operators,
        ]
        table = {}

        for i, e in enumerate(atomi):
            table[e] = i + 1

        return table

    def is_valid_variable_name(self, name):
        return self.automat_variabile.check_sequence(name) == name and not iskeyword(name)

    def check_atom_ended(self, atom):
        comb = True
        if len(atom) in self.atom_combinations:
            comb = atom not in self.atom_combinations[len(atom)]

        return not self.is_valid_variable_name(atom) \
               and comb \
               and not self.is_valid_constant(atom)

    def add_fip_or_ts(self, atom, i):
        if atom not in self.ts:
            self.ts[atom] = hash(atom)
            self.current_id += 1

        self.fip.append((i, self.ts[atom]))

    def save_atom(self, atom, split_line):
        if atom in self.operators or atom in split_line:
            if atom in self.atom_code:
                self.fip.append((self.atom_code[atom], None))

            elif self.is_valid_variable_name(atom):
                self.add_fip_or_ts(atom, 1)
            elif self.is_valid_strig(atom):
                self.add_fip_or_ts(atom, 2)
            elif self.is_valid_constant(atom, incomplete=False):
                self.add_fip_or_ts(atom, 2)
            else:
                raise LexicalError("{} nu este un atom valid la linia {}".format(atom, self.line))
        else:
            raise LexicalError("{} nu este un atom valid la linia {}".format(atom, self.line))

    def split_line(self, line):
        elems = line.split()

        for sep in self.operators:
            final = []
            for el in elems:
                final += el.split(sep)
            elems = final
        return elems

    def parse(self, file):
        with open(file) as f:
            atom = ''
            for line in f:
                splited_line = self.split_line(line)
                for c in line:
                    # c = f.read(1)

                    atom += c

                    if self.check_atom_ended(atom):
                        if atom[0] in self.seps:
                            if atom[0] == '\n':
                                self.line += 1
                            atom = atom[1:]
                            continue
                        self.save_atom(atom[:-1], splited_line)
                        atom = c

            if atom != '\n':
                self.save_atom(atom, splited_line)

    def get_atom_combinations(self):
        longest_atom = max(self.atom_code, key=lambda x: len(x))
        combinations = {}
        for i in range(1, len(longest_atom) + 1):
            current_set = set()
            for e in self.atom_code:
                if len(e) >= i:
                    current_set.add(e[:i])
            combinations[i] = current_set

        return combinations

    def can_be_string(self, atom):
        if atom[-2] == '"' and len(atom) > 2:
            return False
        if atom[0] == '"':
            return True

    def is_valid_float(self, atom):
        is_float = True
        try:
            float(atom)
        except ValueError:
            is_float = False
        if ' ' in atom or '\n' in atom:
            is_float = False

        return is_float

    def is_valid_num(self, atom):
        # is_num = True
        # for tr in [e.isdigit() for e in atom]:
        #     if not tr:
        #         is_num = False
        #         break
        # return self.is_valid_float(atom) or is_num

        return self.automat_numeric.check_sequence(atom) == atom

    def is_valid_constant(self, atom, incomplete=True):

        is_num = self.is_valid_num(atom)

        if incomplete:
            is_num = is_num or self.can_be_string(atom)
        return is_num

    def is_valid_strig(self, atom):
        return atom[0] == '"' and atom[-1] == '"' and len(atom) > 1


class Starter:
    def __init__(self, parser):
        self.parser = parser

    def start(self):
        for file in sys.argv[1:]:
            self.parser.parse(file)
        print("Atom code: \n{}".format(self.parser.atom_code))
        print("Fip: \n{}".format(self.parser.fip))
        print("Ts: \n{}".format(self.parser.ts))


p = Parser()
s = Starter(p)
s.start()
