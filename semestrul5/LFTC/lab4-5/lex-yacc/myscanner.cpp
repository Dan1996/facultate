#include <iostream>
#include "myheader.h"
#include <map>
#include <vector>
#include <string>
#include <functional>

using namespace std;

hash<string> my_hash;
extern int yylex();
extern int yylineno;
extern char* yytext;

struct Elem{
	int atomCode;
	int tsPosition;
};

void addFipOrTs(vector<Elem>& Fip, string atomName, int atomCode, map<string, int>& Ts){
	Elem el;
	el.atomCode = atomCode;
	el.tsPosition = my_hash(atomName);

	if(Ts.find(atomName) == Ts.end()){
		Ts[atomName] = my_hash(atomName);
	}

	Fip.push_back(el);
}


int main(){
	int ntoken;
	string textToken;
	vector<Elem> Fip;
	map<string, int> Ts;


	ntoken = yylex();
	while(ntoken){
		if(ntoken == 0){
			printf("Eroare la linia %d \n", yylineno);
			return 1;
		}
		printf("%d %s \n", ntoken, yytext);
		textToken = string(yytext);
		if(ntoken == CONST || ntoken == ID){
			addFipOrTs(Fip, textToken, ntoken, Ts);
		} else{
			Elem el;
			el.atomCode = ntoken;
			el.tsPosition = 0;
			Fip.push_back(el);
		}
		ntoken = yylex();
	}
	printf("Fip: \n");
	for(auto el: Fip){
		printf("(%d, %d) ", el.atomCode, el.tsPosition);
	}
	cout << endl;
	printf("Ts: \n");
	for(auto el: Ts){
		cout<<el.first;
		printf(" %d, ", el.second);
	}
	printf("\n");
	return 0;
}

