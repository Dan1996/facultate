%{
	#include "y.tab.h"
%}

%%

"!="                  return RELATIONOP;
"print"               return PRINT;
"while"               return WHILE;
"input()"               return INPUT;
"\r\n"                return STUPIDENDLINE;
[\n*<+)(=:;%]            return yytext[0];
[a-zA-Z]+[0-9]*       return ID;
[0-9]                 return CONST;
[1-9][0-9]*	          return CONST;
[0-9]\.[0-9]*	      return CONST;
\".*\"                return CONST;
[ \t\n\r\n]				  ;
.		      return 0;

%%
int yywrap(void){
	return 1;
}

