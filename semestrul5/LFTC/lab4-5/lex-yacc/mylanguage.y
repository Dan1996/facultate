%{
void yyerror (char *s);
int yylex();
#include <stdio.h>     /* C declarations used in actions */
#include <stdlib.h>
#include <ctype.h>
extern FILE *yyin;
%}

%token PRINT
%token ID
%token CONST
%token WHILE
%token RELATIONOP
%token STUPIDENDLINE
%token INPUT
%%

/* descriptions of expected inputs     corresponding actions (in C) */
blockstatement: blockstatement statement
			  | blockstatement '\n'
			  | blockstatement STUPIDENDLINE
			  | statement

statement: assignment ';'|
		   outputstatement ';' |
		   whilestatement

assignment : ID '=' expresion


expresion    	: operand       
       			| expresion '+' operand          
       			| expresion '-' operand    
       			| expresion '*' operand
       			| expresion '/' operand
       			| expresion '%' operand

       	
operand   	: ID			
			| CONST
			| INPUT
        

outputstatement : PRINT '(' expresion ')'
whilestatement: WHILE condition ':' linestatement
condition: expresion '<' expresion
		 | expresion RELATIONOP expresion

linestatement: statement
			 | linestatement statement


%%

int main (void) {
	return yyparse ();
}

void yyerror (char *s) {fprintf (stderr, "%s\n", s);} 

