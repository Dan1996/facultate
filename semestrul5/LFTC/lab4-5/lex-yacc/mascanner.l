%{
	#include "myheader.h"
%}

%%

"%"                   return MOD;
"*"                   return MUL;
"<"                   return LESS;
"+"                   return PLUS;
")"                   return CLOSEPAR;
"("                   return OPENPAR;
"!="                  return NOTEQUAL;
"="                   return EQUAL;
":"     		      return COLON;
";"                   return SEMICOLON;
"print"               return PRINT;
"while"               return WHILE;
"input"               return INPUT;
[a-zA-Z]+[0-9]*       return ID;
[0-9]                 return CONST;
[1-9][0-9]*	          return CONST;
[0-9]\.[0-9]*	      return CONST;
\".*\"                return CONST;
[ \t\n\r]				  ;
.		      return 0;

%%
int yywrap(void){
	return 1;
}

