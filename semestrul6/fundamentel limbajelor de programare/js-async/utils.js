const isObject = (obj) =>
    typeof obj === 'object' && obj !== null && !Array.isArray(obj);

const isFunction = (func) => typeof func === 'function';

const isGeneratorLike = (obj) =>
    isObject(obj) && isFunction(obj.next) && isFunction(obj.throw);

export { isObject, isFunction, isGeneratorLike };