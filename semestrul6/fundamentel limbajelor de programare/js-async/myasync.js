// import {isGeneratorLike} from "./utils";

function myasync(generatorFactory) {
    const generator = generatorFactory();

    // if(!isGeneratorLike(generator)){
    //     return Promise.reject(
    //         new Error("Functia trebuie sa returneze un generator")
    //     )
    // }

    return (function resolve(result) {
        // generatorul returneaza un obiect de tip {value:Promise, done:bool}
        if(result.done){
            return Promise.resolve(result.value);
        }

        return Promise.resolve(result.value)
            .then((ensuredValue) => resolve(generator.next(ensuredValue)))
            .catch((error) => resolve(generator.throw(error)));
        })(generator.next());
}

const f = myasync (function* () {
    const a = yield Promise.resolve('a');
    console.log(a);

    const b = yield Promise.resolve('b');
    console.log(b);

    return 3;
});

console.log(f.then((rez) => console.log(rez)));