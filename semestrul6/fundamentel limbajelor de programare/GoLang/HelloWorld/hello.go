package main

import (
	"fmt"
	"errors"
	"math"
)

func main(){
	fmt.Println("Hello World!")
	// daca nu initializezi e default 0 
	var x int 
	var y int = 7
	// scriere scurta, compilatorul deduce tipul automat
	sum := x + y
	fmt.Println(sum)

	// fara paranteze la if
	// expresia evaluata trebuie sa fie neaparat boolean
	// if x % 2 { ... } - invalid
	if x % 2 == 0 {
		fmt.Println("Par")
	} else if x % 2 != 0{
		fmt.Println("Nu-i par")
	}

	var v [5]int
	// 0 peste tot pentru ca nu am dat valoare initiala
	fmt.Println(v)

	// declarare scurta, nu pot folosi tot b pentru ca e declarat deja, nu permite redeclarare
	a := [5]int{5,4,3,2,1}
	fmt.Println(a)

	//ce am definit pana acum sunt array-uri de lungime fixa
	// pentru a defini o lista trebuie sa omitem numarul de elemente
	l := []int{5,4,3,2,1}
	// se cheama de fapt slice-uri nu chiar liste
	// append returneaza un nou slice, nu-l modifica pe cel vechi
	l = append(l, 23123123)
	fmt.Println(l)

	dict := make(map[string]int)
	dict["start"] = 0
	dict["finish"] = 9000

	fmt.Println(dict)

	for i := 0; i < 5; i++{
		fmt.Println(i)
	}

	// asta e un while pentru ca baietii se cred foarte destepti acolo la google
	i := 0
	for i < 5{
		fmt.Println(i)
		i++
	}

	for index, value := range l{
		fmt.Println(index, value)
	}

	for key, value := range dict{
		fmt.Println(key, value)
	}

	result := suma(2, 3)
	fmt.Println(result)

	result2 , err := sqrt(-16)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(result2)
	}
	// go nu are exceptii de aia se folosesc erorile (asa o hotarat aia de la google ca nu le tre exceptii...)
	p := person{name: "Dan", age: 22}
	fmt.Println(p)

	//smecherie cu fitil, poti sa folosesti referinte in limbaju asta
	int_smecher := 10
	fmt.Println(int_smecher)
	fmt.Println(&int_smecher)

	inc(int_smecher) // nu face nimic 
	inc_smecher(&int_smecher)
	fmt.Println(int_smecher)
}

type person struct{
	name string
	age int
}

func suma(x int, y int) int {
	return x + y
}

func inc(x int){
	x++
}

func inc_smecher(x *int){
	*x++
}

// built in type error
func sqrt(x float64) (float64, error){
	if x < 0 {
		return 0, errors.New("N-ai voie cu negative la radical baiete")
	}
	return math.Sqrt(x), nil
}