package main

import "fmt"

// Cum se declara o interfata
type IPlanet interface {
	GetSizeTimesRadius() int
	SetSize(size int)
	
}

type PlanetCuInterfata struct {
	IPlanet
}

type Planet struct{
	Size int
	Radius int
	// campurile cu litere mici vor fi private implicit, iar cele cu litere mari sunt publice
	
}

//Composition
type World struct {
	Parent Planet
	Name string
}

// sau se mai poate declara asa
type WorldNew struct {
	Planet // si se acceseaza obj.Planet.Size ... 
	Name string
}

func (p Planet) GetSizeTimesRadius() int {
	return p.Size * p.Radius
}

func (p PlanetCuInterfata) GetSizeTimesRadius() int {
	return 37
}

func (p Planet) SetSize(size int) {
	p.Size = size
}

func (p* Planet) SetSizePointer(size int) {
	p.Size = size
}

func main() {
	world:=Planet{1,2}
	earth:=world //se copiaza continutul, dar se creeaza obiect nou
	earth.Size=100 //cand modificam asta nu va afecta obiectul world
	fmt.Println(world.Size); // afiseaza 1
	fmt.Println(earth.Size); //afiseaza 100
	
	// Problema de mai sus se rezolva cu pointeri
	var worldP *Planet = &Planet{1,2};
	// nu trebuie dereferentiat pointerul, compilatorul face asta automat
	// worldP.Radius 
	// Nu se pot assigna asa worldP e pointer!!
	// var mars Planet = worldP;
	var mars *Planet = worldP; 
	// sau asa var mars = worldP sau mars:=worldP <- asa creeaza o noua varibila
	mars.Size=100
	fmt.Println(worldP.Size); // afiseaza 100
	fmt.Println(mars.Size); //afiseaza 100
	
	
	var p Planet = Planet{1,2}
	p.SetSize(5)
	fmt.Println(p.GetSizeTimesRadius()) // Va afisa 2, chiar  daca am apelat functia de SetSize
	// Nu merge pentru ca functia GetSize primeste p Planet ca si copie, ar trebui trimis ca si pointer
	
	p.SetSizePointer(5)
	fmt.Println(p.GetSizeTimesRadius()) // Va afisa 10
	
	var worldW World = World{Parent: Planet{1,2}, Name: "earth"}
	// Composition
	worldW.Parent.SetSizePointer(5)
	fmt.Println(p.GetSizeTimesRadius()) // Va afisa 10
	
	DoSomethingOnPlanet(Planet{1,2}) // nu se poate folosi asa ceva 
	// Nu se satisface conditia de a fi IPlanet, daca lasam fara pointer la GetSizePointer atunci merge 
	// Cand lasam functia GetSizePointer fara pointer se va afisa valoarea 2
	// Duck typing, nu spunem ca Planet mosteneste interfata, dar daca satisface metodele si structura interfetei atunci e bine
	
	// Interfete pot fi implementate fara a implementa tot!
	DoSomethingOnPlanet(PlanetCuInterfata{}) // Se va afisa 37, metoda GetSizeTimesRadius implementeaza metoda declarata in interfata
	
	
}

func DoSomethingOnPlanet(planet IPlanet) {
	fmt.Println(planet.GetSizeTimesRadius())
	
}
