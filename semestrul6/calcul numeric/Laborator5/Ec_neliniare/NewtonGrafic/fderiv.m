function [rez]=fderiv(x)

rez=cos(x)-3*(x/2).^2;