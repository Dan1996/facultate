open Ast
open Program

exception MainFunctionMissing of string 
exception NameError of string
exception AdressError of string
exception ProgramError of string 
exception TypeError of string 
exception CallError of string 

type typVal = typ * value
type varEnv = (string * typVal) list
type heap = (int * typVal) list
type output = value list

type state = {f:program;
	 h:heap;
	 v:varEnv;
	 o:output;
	 e:exp}


let error (i) = raise (TypeError ("Eroare de tip" ^ i)) 


let rec print_values value_list = 
	match value_list with
	| [] -> ()
	| h :: t -> 
		match h with
		| Vaddr a -> let _ =  print_endline (string_of_int a) in print_values t
		| Vint a ->  let _ =  print_endline (string_of_int a) in print_values t
		| Vbool a -> let _ =  print_endline (string_of_bool a) in print_values t
		| Vnull -> let _ =  print_endline "null" in print_values t
		| Vvoid	-> let _ =  print_endline "void" in print_values t

let rec update venv varn myvalue = 	
	match venv with
	| [] -> raise(NameError "Variable not found")
	| (name, (vtype, v)) ::t -> 
	 	 if name = varn then
	 	 	(name, (vtype, myvalue)) :: t
	 	 else
	 	 	(name, (vtype, v)) :: update t varn myvalue

let eq_prim t1 t2 = 
	match (t1,t2) with
		|(Tint, Tint) -> true
		| (Tbool, Tbool) -> true
		| (Tvoid, Tvoid) -> true
		| (_,_) -> false

let rec eq_type t1 t2 = 
	match (t1,t2) with
		|(Tprim ta, Tprim tb) ->  eq_prim ta tb
		| (Tbot, Tbot) -> true
		| (Tbot, Taddr _) -> true
		| (Taddr _, Tbot) -> true
		| (Taddr ta,Taddr tb) -> eq_type ta tb
		| (_,_) -> false

let rec getVarType (venv_heap) (varn)  = 
	match venv_heap with
	| [] -> raise (AdressError "wrong adress")
	| (name_addr, (vtype, v)) ::t -> 
	 	 if name_addr = varn then
	 	 	vtype
	 	 else
	 	 	getVarType t varn

let rec getValType (value) (s) = 
	match value with
		| Vint _  -> Tprim(Tint)
		| Vbool _ -> Tprim(Tbool) 
		| Vvoid -> Tprim(Tvoid)
		| Vaddr addr  -> 
			let _  = print_endline (string_of_int addr) in 
			let addrtype = getVarType s.h addr in 
			Taddr(addrtype)
		| Vnull -> Tbot


let rec findfunc (p: program) (name: string): funDecl = 
	match p with
	 | [] -> raise (MainFunctionMissing (name ^ " function must be implemented"))
	 | myfunc ::t -> 
	 	 let (_, namef, _, _) = myfunc in 
	 	 if namef = name then
	 	 	myfunc
	 	 else
	 	 	findfunc t name

let rec getVal_content (varn_addr) (venv): value = 
	match venv with
	 | [] -> raise( NameError "Variable was not defined")
	 | (name_addr, (vtype, v)) ::t -> 
	 	 if name_addr = varn_addr then
	 	 	v
	 	 else
	 	 	getVal_content varn_addr t

let rec init mytype = 
	match mytype with
	| Tprim tprim -> 
		(match tprim with
		 |Tint -> Vint(0)
		 | Tbool -> Vbool(true)
		 | Tvoid -> Vvoid
		)
	| Taddr t -> Vnull 
	| Tbot -> Vnull

let aloc_space h = 
	let rec inner h p = 
		match h with
		| (loc, _) :: t -> 
			if loc > p then
				inner t loc
			else
				inner t p 
		| [] -> p + 1
	in
	inner h 0

let get_value eval = 
	match eval with
	| Eval v -> v
	| _ -> raise (TypeError "This is not a value")

let get_bool sbool = 
	match sbool with
	| Vbool b -> b
	| _ -> raise (TypeError "This is not bool")

let get_int sint = 
	match sint with
	| Vint b -> b
	| _ -> raise (TypeError "This is not int")

let get_bool sbool = 
	match sbool with
	| Vbool b -> b
	| _ ->  raise (TypeError "This is not bool") 

let get_int_from_value sv = 
	let v = get_value sv in
	get_int v

let get_bool_from_value sb = 
	let v = get_value sb in
	get_bool v

let rec deleteH loc h =
	match h with
	 | [] -> raise( NameError "Variable was not defined")
	 | (addr, (vtype, v)) ::t -> 
	 	 if loc = addr then
	 	 	t
	 	 else
	 	 	(addr, (vtype, v)) :: deleteH loc t

let findvarr_content (s: state) (varr) (f) = 
	getVal_content varr (f s)


let rec type_of_args args (s: state) = 
	match args with
	| v :: t -> 
		let mytype = getVarType s.v v in 
		mytype :: type_of_args t s
	| [] -> []

let rec check_func_types type_list params = 
	match (type_list, params) with
	| (type_v :: t1, (type_arg, name_arg ):: t2) ->
		if eq_type type_v type_arg then
			check_func_types t1 t2
		else
			false

	| ([], []) -> true
	| (_, _) -> false

let rec gen_vars args param_list s  = 
	(* (string , typval ) *)
	match (args, param_list) with
	| (varn :: t1, (type_par, name_par):: t2) ->
			 (name_par, (type_par, findvarr_content s varn (fun (s) -> s.v ))) :: gen_vars t1 t2 s
	| ([], []) -> []
	| (_, _) -> raise(CallError "Wrong call")


let rec getVarType2 (v) (g) = 
	match g with
	| (mytype, venv) :: t -> 
		if venv = v then
			mytype
		else
			getVarType2 v t
	| [] -> error("vartype")


let rec type_of_args2 args (g) = 
	match args with
	| v :: t -> 
		let mytype = getVarType2 v g in 
		mytype :: type_of_args2 t g
	| [] -> []

let rec typecheck_exp (f:program) (g: (typ*string) list) (e:exp) :typ =
	match e with
	| Eval Vnull -> Tbot
	| Eval Vvoid -> Tprim(Tvoid)
	| Eval Vint kint -> 
		let _ = print_endline "Eval vint" in
		Tprim(Tint)
	| Eval Vbool kbool -> Tprim(Tbool)
	| Eval Vaddr loc -> Taddr(Tprim(Tint))

	| Evar var -> 
		let _ = print_endline "Evar" in 
		getVarType2 var g 

	| Ederef var -> 
	    let typ=getVarType2 var g in
		(match typ with
		|Taddr t -> t
		| _ -> error("Ederef")
		)

	| Eassignvar (var,e) ->
		let _ = print_endline "assignvar" in 
		let varr = Evar(var) in
		let t1 = typecheck_exp f g varr in
		let t2 = typecheck_exp f g e in
		if (eq_type t1 t2) then Tprim(Tvoid) else error("assignvar")

	| Eassignloc (varn, myexp) ->
		let _ = print_endline "Eassignloc" in 
		let varr = Evar(varn) in
		let t1 = typecheck_exp f g varr in  
		(match t1 with
		| Taddr t2 ->
			let t3= typecheck_exp f g myexp in
			if (eq_type t2 t3) then Tprim(Tvoid) else error("Eassignloc")

		| _ -> error("Eassignloc")
		)

	| Eseq (exp1, exp2) ->
		let _ = print_endline "Eseq" in
		let _ = typecheck_exp f g exp1 in 
		typecheck_exp f g exp2 

	| Eblk (t,v,e) ->
		let _ = print_endline "Eblk" in
 		typecheck_exp f ((t,v)::g) e

 	| Eif (condexp, trueexp, falseexp ) ->
 		let _ = print_endline "Eif" in 
 		let t1 = typecheck_exp f g condexp in
 		let t2 = typecheck_exp f g trueexp in
 		let t3 = typecheck_exp f g falseexp in
 		
 		(match t1 with
 		| Tprim(Tbool) -> 
 			if eq_type t2 t3 then t1 else error("eif")
  		| _ -> error ("Eif"))

 	| Eplus (ex1, ex2) | Eminus (ex1, ex2) | Estar (ex1, ex2) | Ediv (ex1, ex2) ->
 		let t1 = typecheck_exp f g ex1 in
 		let t2 = typecheck_exp f g ex2 in
 		(match (t1, t2) with
 		| (Tprim(Tint), Tprim(Tint)) -> Tprim(Tint)
 		| _ -> error("Earithmetic"))

	| Eless (ex1, ex2) |  Eeq (ex1, ex2) | Egreater (ex1, ex2) | Eneq (ex1, ex2) ->
		let t1 = typecheck_exp f g ex1 in
 		let t2 = typecheck_exp f g ex2 in
 		(match (t1, t2) with
 		| (Tprim(Tint), Tprim(Tint)) -> Tprim(Tbool)
 		| _ -> error("Ecompare"))

	| Eand (ex1, ex2) | Eor (ex1, ex2) ->
		let t1 = typecheck_exp f g ex1 in
 		let t2 = typecheck_exp f g ex2 in
 		(match (t1, t2) with
 		| (Tprim(Tbool), Tprim(Tbool)) -> Tprim(Tbool)
 		| _ -> error("Eand"))

 	| Enot (ex) -> 
 		let t1 = typecheck_exp f g ex in
 		(match t1 with
 		| Tprim(Tbool) -> Tprim(Tbool)
 		| _ -> error("Enot"))

 	| Eprint myexp -> Tprim(Tvoid)

 	| Enew (mytype, varn) ->
 		let _ = print_endline "Enew" in
 		let varr = Evar(varn) in 
 		let t1 = typecheck_exp f g varr in
 		if eq_type mytype t1 then 
 			Taddr(mytype)
 		else
 			error("Enew")

 	| Efcall (name, args) ->  		
 		let (tr, fname, lParam, eBody) = findfunc f name in
 		let type_list = type_of_args2 args g in

 		if check_func_types type_list lParam then
 			let t = typecheck_exp f lParam eBody in
 			t
 		else
 			error("Efcall")

 	| Ewhile (cond, body) ->
 		let t1 = typecheck_exp f g cond in
 		let t2 = Tprim(Tbool) in
 		if eq_type t1 t2 then 
 			let _ = typecheck_exp f g body in
 			Tprim(Tvoid)
 		else
 			error("Ewhile")

 	| Edelete loc -> 
 		let varr = Evar(loc) in
		let t1 = typecheck_exp f g varr in
 		match t1 with
 		| Taddr _ -> Tprim(Tvoid)
 		| _ -> error("Edelete")



let typecheck_func (fd:funDecl) (p:program) : bool =
	let (tr,_,lParam,eBody) = fd in
	let tcalc= typecheck_exp p lParam eBody in
	eq_type tcalc tr

let rec typecheck_prg (p:program) (p1:program) : bool =
	match p with
	| [] -> true
	| [(tr,fname,lParam,eBody)] -> 
			fname = "main" && typecheck_func (tr,fname,lParam,eBody) p1
	| fd :: rest ->
 		typecheck_func fd p1 && typecheck_prg rest p1 
