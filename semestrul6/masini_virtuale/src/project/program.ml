open Ast

let program1 =
[
	(Tprim Tint, "f", [(Tprim(Tint), "x")],	
		Eseq( 
		Ewhile (Egreater (Evar "x", Eval (Vint 0)), 
  	            Eseq(Eprint (Evar "x"),
  	            	 Eassignvar ("x", Eminus (Evar "x",Eval (Vint 1))))
			),
		Eval(Vint 150)
		)
		
	);
	(Tprim(Tvoid) , "main", [],
		Eblk (Tprim(Tint), "x",
	 		Eseq (Eif( Eval(Vbool false),  Eassignvar ("x", Eval (Vint 20)), Eassignvar ("x", Eval (Vint 10))),
	 			  Eseq ( Eprint ( Efcall ("f", ["x"])) ,
 				  	Eseq ( Eprint( Evar("x")),
 				  	 Eblk ( Taddr(Tprim(Tint)), "y",
 				  	   Eseq( Eassignvar( "y",  Enew(Tprim(Tint), "x")),
 				  	   	  Eseq (
 				  	   	  	Eprint(Ederef("y")),
 				  	   	  	Eseq(Eassignloc("y", Eval(Vint 100)),
 				  	   	 		 Eprint(Ederef("y"))))
 				  	   	  ) 
 				  	   	  
 				  	    )
 				  	 
 				  )
 				  )
 				 )
 			 )
 	)
]


let program2 = 
[
	(Tprim Tint, "main", [],
		Eblk (Tprim(Tint), "x",
			Eseq (Eassignvar ("x", Eval (Vint 20)),
				   Eseq ( Eprint (Evar "x"),
				   		  Evar "x"
				   	)
				 )
		)
	)
]
