open Ast
open Program
open Typechecker

let rec evalE (s) =
	let aritmetic_value (s) = 
		let new_state = evalE s in 
		match new_state.e with
		| Eval realvalue -> realvalue
		| _ -> raise  (ProgramError "trebuie sa se reduca la Eval")
	in

	let rec operator_handler (s) (ex1:exp) (ex2:exp) (f) = 
		let state1 = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=ex1} in 
		let i1 = get_int_from_value state1.e in
		let state2 = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=ex2} in
		let i2 = get_int_from_value state2.e in
		{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval (f i1 i2))}
 	in

 	let rec get_bool_from_exp (s) (ex: exp) = 
 		let state1 = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=ex} in 
		get_bool_from_value state1.e
	in 

 	let rec bool_operator_handler (s) (ex1:exp) (ex2:exp) (f) = 
 		let b1 = get_bool_from_exp s ex1 in
		let b2 = get_bool_from_exp s ex2 in 
		{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval (f b1 b2))}
 	in 
	
	match s.e with
 	| Eval valuev ->
 		let _ = print_endline "Eval" in

		{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval valuev)}

 	| Evar varn -> 
 		let _ = print_endline "Evar" in 

 		let myvalue = findvarr_content s varn (fun (s) -> s.v ) in
 		{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval myvalue)}
 	
	| Ederef varp -> 
		let _ = print_endline "Ederef" in 

		let locvar = findvarr_content s varp (fun (s) -> s.v ) in
		(match locvar with
		 | Vaddr loc -> 
			let myvalue = findvarr_content s loc (fun (s) -> s.h) in
			{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval myvalue)}	
		 | _ -> raise (AdressError "This is not a valid adress")
		)
		
	| Eassignvar (varn, myexp) ->
 		let _ = print_endline "Eassignvar" in 

 		let typ_var = getVarType s.v varn in 
 		let new_state = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=myexp} in 
 		(match new_state.e with
		| Eval realvalue -> 
			(* let _ = print_endline "here" in  *)
			let typ_val = getValType realvalue new_state in
			if eq_type typ_var typ_val then
				let newvenv = update s.v varn realvalue in 
				{f=s.f;h=new_state.h;v=newvenv;o=new_state.o;e=Eval(Vnull)}
			else
				raise (TypeError "Variables and values must have the same type")

		| _ -> raise  (ProgramError "trebuie sa se reduca la Eval")
 		)
		
	| Eassignloc (varn, myexp) -> 
		let _ = print_endline "Eassignloc" in

		let locvar = findvarr_content s varn (fun (s) -> s.v ) in 
		(match locvar with
		 | Vaddr loc -> 
			let myvalue = findvarr_content s loc (fun (s) -> s.h) in
			let realvalue = aritmetic_value {f=s.f;h=s.h;v=s.v;o=s.o;e=myexp} in 
			let typ_var = getValType myvalue s in
			let typ_val = getValType realvalue s in
			
			if eq_type typ_var typ_val then
				let new_heap = update s.h loc realvalue in 
				(* actualizeaza heap-u *)
				{f=s.f;h=new_heap;v=s.v;o=s.o;e=Eval(Vnull)}	
			else
				raise (AdressError "Wrong adress")

		 | _ -> raise (AdressError "This is not a valid adress")
		)

	| Eseq (exp1, exp2) -> 
		let _ = print_endline "Eseq" in 
 		let ns = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=exp1} in
		evalE {f=ns.f;h=ns.h;v=ns.v;o=ns.o;e=exp2}

	| Eblk (mytype, varn, expr) ->
		let _ = print_endline "Eblk" in

		let new_venv = (varn, (mytype, init mytype)) :: s.v in 
		let new_state = evalE {f=s.f;h=s.h;v=new_venv;o=s.o;e=expr} in
		{f=s.f;h=new_state.h;v=s.v;o=new_state.o;e=new_state.e}

	| Eprint myexp ->
		let _ = print_endline "Eprint" in

		let new_state = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=myexp} in 
		(match new_state.e with
		| Eval realvalue -> 
			let real_value = aritmetic_value {f=s.f;h=s.h;v=s.v;o=s.o;e=myexp} in 
			let out = new_state.o @ [real_value] in 
			{f=s.f;h=new_state.h;v=s.v;o=out;e=Eval(Vnull)}

		| _ -> raise  (ProgramError "trebuie sa se reduca la Eval")
		)

	| Enew (mytype, varn) ->
		let _ = print_endline "Enew" in 

		let myvalue = findvarr_content s varn (fun (s) -> s.v ) in 
		let vartype = getVarType s.v varn in
		if eq_type mytype vartype then
			let loc = aloc_space s.h in 
			let new_heap = (loc, (mytype, myvalue))::s.h in
			{f=s.f;h=new_heap;v=s.v;o=s.o;e=Eval(Vaddr(loc))}
		else 
			raise (TypeError "wrong type")
	| Edelete loc ->
		let _ = print_endline "Edelete" in 

		let locvar = findvarr_content s loc (fun (s) -> s.v ) in
		(match locvar with
		 | Vaddr loc -> 
			let new_heap = deleteH loc s.h  in 
			{f=s.f;h=new_heap;v=s.v;o=s.o;e=Eval(Vnull)}

		 | _ -> raise (AdressError "Heap is only for adresses")
		)

	| Eif (condexp, trueexp, falseexp ) ->
		let _ = print_endline "Eif" in

		let new_state = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=condexp} in 
		let new_value = get_value new_state.e in 
		let new_bool = get_bool new_value in
		if new_bool then
			evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=trueexp}
		else
			evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=falseexp}

	| Eplus (ex1, ex2) ->
		let _ = print_endline "Ediv" in
		
		operator_handler s ex1 ex2 (fun x y -> Vint(x + y))
	| Eminus (ex1, ex2) ->
		let _ = print_endline "Eminus" in
		
		operator_handler s ex1 ex2 (fun x y -> Vint(x - y))
	| Estar (ex1, ex2) -> 
		let _ = print_endline "Estar" in
		
		operator_handler s ex1 ex2 (fun x y -> Vint(x * y)) 
	| Ediv (ex1, ex2) ->  
		let _ = print_endline "Ediv" in
		
		(* ar trebui aruncata eroare la division by 0  ? *)
		operator_handler s ex1 ex2 (fun x y -> Vint(x / y))
	
	| Eless (ex1, ex2) ->
		let _ = print_endline "Eless" in

		operator_handler s ex1 ex2 (fun x y -> Vbool(x < y))

	| Eeq (ex1, ex2) ->
		let _ = print_endline "Eeq" in

		operator_handler s ex1 ex2 (fun x y -> Vbool(x = y))
	| Egreater (ex1, ex2) -> 
		let _ = print_endline "Egreater" in

		operator_handler s ex1 ex2 (fun x y -> Vbool(x > y))
	| Eneq (ex1, ex2) ->
		let _ = print_endline "Eneq" in

		operator_handler s ex1 ex2 (fun x y -> Vbool(x != y))
	| Eand (ex1, ex2) ->
		let _ = print_endline "Eand" in

		bool_operator_handler s ex1 ex2 (fun x y -> Vbool(x && y))

	| Eor (ex1, ex2) ->
		let _ = print_endline "Eor" in
	 	
	 	bool_operator_handler s ex1 ex2 (fun x y -> Vbool(x || y))

	 | Enot (ex) -> 
	 	let _ = print_endline "Enot" in

	 	let b = get_bool_from_exp s ex in
	 	{f=s.f;h=s.h;v=s.v;o=s.o;e=(Eval(Vbool(not b)))}

	 | Ewhile (cond, body) -> 
	 	let _ = print_endline "Ewhile" in

	 	let new_state = evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=cond} in
	 	let b = get_bool_from_exp s new_state.e in 
	 	if b then
	 		evalE {f=s.f;h=s.h;v=s.v;o=s.o;e=Eseq(body, Ewhile(cond, body))}
	 	else
	 		{f=s.f;h=s.h;v=s.v;o=s.o;e=Eval(Vnull)}

	 | Efcall (name, args) ->
	 	let _ = print_endline "Efcall" in
	 	let myfunc = findfunc s.f name in 
	 	let (_, namef, params, body) = myfunc in 
	 	
	 	let type_list = type_of_args args s in
	 	if check_func_types type_list params then
	 		let vars = gen_vars args params s in
	 		let new_state = evalE {f=s.f;h=s.h;v=vars;o=s.o;e=body} in
	 		{f=s.f;h=new_state.h;v=s.v;o=new_state.o;e=new_state.e}
	 	else
	 		raise (CallError "Function call is incorect")

let evalP (p:program) =
	 let mainF = findfunc p "main" in
	 let (_,_,_,body)=mainF in
	 	let typeproof = typecheck_prg p p in
	 	if typeproof then
		 	let s = {f=p;h=[];v=[];o=[];e=body} in
			let final_s = evalE s in 

			let _ = print_endline "" in
			let _ = print_endline "" in 
			let _ = print_endline "" in 

			let _ = print_values final_s.o in 
			final_s
		else
			raise (ProgramError "Nu-i bun")

let _ = evalP program1
