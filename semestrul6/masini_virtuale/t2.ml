
let rec concatList (l1: int list) (l2: int list): int list =
	match l1 with
	| [] -> l2
	| h::t -> concatList t (h::l2)


let rec print_list (l1: int list): unit = 
	match l1 with
	| [] -> ()
	| h::t -> ignore(print_endline(string_of_int h));
				print_list(t)

let rec dup(l1: int list): int list =
	match l1 with
	| [] -> []
	| h::t -> h :: h :: dup(t)


let rec merge(l1: int list) (l2: int list): int list = 
	match (l1, l2) with
	| (h1::t1, h2::t2) ->  if h1 < h2 then 	h1 :: (merge t1 l2) else h2 :: (merge l1 t2)
	| ([], l) | (l, []) -> l

let rec merge_sort(l1: int list): int list =
	match l1 with
	| h::t -> let r = merge [h]   in
	| h::[] -> h::[]

let l3 = merge [4;9;10;16] [3;5;7;11;12;17]
let _ = print_list l3