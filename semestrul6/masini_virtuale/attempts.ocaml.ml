(* type point = float * float
let distance (p1:point) (p2:point) : float = 
	let square x = x *. x in
	let (x1, y1) = p1 in 
	let (x2, y2) = p2 in
	sqrt (square (x2 -. x1) +. square(y2 -. y1))

let pt1 = (2.0, 3.0)
let pt2 = (0.0, 1.0)
let dist12 = distance pt1 pt2
let _ = print_endline( string_of_float dist12 )

let slope (p1:point) (p2:point) : float option = 
	let (x1, y1) = p1 in 
	let (x2, y2) = p2 in
	let xd = x2 -. x1 in
	if xd != 0.0 then
		Some ((y2 -. y1) /. xd)
	else
		None

let slope12 = slope pt1 pt2

let _ = match slope12 with
| Some s -> print_endline( string_of_float s )
| None -> print_endline "mda"
 

let rec prods (xs: (int * int) list): int list = 
	match xs with
	| (x, y) :: t -> x * y :: prods t
	| [] -> []


let rec insert (l: int list) (el: int): int list = 
	match l with
	| h :: t -> 
		if h > el then
			el :: h :: t
		else
			h :: insert t el 
	| [] -> [el]

let rec insertion_sort (l: int list): int list = 
	let rec aux (sorted: int list) (unsorted: int list): int list = 
		match unsorted with
		| [] -> sorted
		| h :: t -> aux (insert sorted h) t
	in 
	aux [] l

let rec print_list (l: int list): int list = 
	match l with
	| [] -> []
	| h::t -> let _ = print_endline (string_of_int h) in
			  print_list t

let _ = print_list (insertion_sort [10;5;32;3;6;1])
 *)
type color = Blue | Yellow | Green | Red

type key = string
type value = int

type tree = 
	  Leaf
	| Node of key * value * tree * tree

let _ = a [("3", true); ("5", false)]

































